package com.datafoundry.llpdatamanager.controller;

import com.datafoundry.llpdatamanager.Mocks;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.FileMetaData;
import com.datafoundry.llpdatamanager.models.ListSolutionProcessIdResponse;
import com.datafoundry.llpdatamanager.models.SearchResponse;
import com.datafoundry.llpdatamanager.service.AsyncLLPDataManagerService;
import com.datafoundry.llpdatamanager.service.LLPDataManagerService;
import com.datafoundry.llpdatamanager.service.impl.CaptureLogImpl;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = LLPDataManagerController.class)
public class LLPDataManagerControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    LLPDataManagerService llpDataManagerService;

    @MockBean
    AsyncLLPDataManagerService asyncLLPDataManagerService;
    
    @MockBean
    CaptureLogImpl captureLog;

    ObjectMapper objectMapper = new ObjectMapper();
    private final String applicationJson = "application/json";

    @Test
    void whenValidInputForUpdateSolutionId_thenReturn200() throws Exception {
        mockMvc.perform(put(AppConstants.SOLUTION_ID_PATH + AppConstants.UPDATE_PATH)
                .contentType("application/json")
                .content(Mocks.UPDATE_SOLUTION_ID_REQUEST))
                .andExpect(status().isOk());
    }

    @Test
    void whenInvalidInputForUpdateSolutionId_thenReturn400() throws Exception {
        mockMvc.perform(put(AppConstants.SOLUTION_ID_PATH + AppConstants.UPDATE_PATH)
                .contentType("application/json")
                .content(Mocks.UPDATE_SOLUTION_ID_400_REQUEST))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidSolutionIdForUpdateSolutionId_thenReturn404() throws Exception {
        Mockito.when(llpDataManagerService.update(Mockito.any()))
                .thenThrow(new AppException(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND, ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND));
        mockMvc.perform(put(AppConstants.SOLUTION_ID_PATH + AppConstants.UPDATE_PATH)
                .contentType("application/json")
                .content(Mocks.UPDATE_SOLUTION_ID_REQUEST))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenSomethingWentWrongForUpdateSolutionId_thenReturn422() throws Exception {
        Mockito.when(llpDataManagerService.update(Mockito.any()))
                .thenThrow(new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SERVICE_UNAVAILABLE));
        mockMvc.perform(put(AppConstants.SOLUTION_ID_PATH + AppConstants.UPDATE_PATH)
                .contentType("application/json")
                .content(Mocks.UPDATE_SOLUTION_ID_REQUEST))
                .andExpect(status().isUnprocessableEntity());
    }

    @Tag("uploadDocuments")
	@Test
	void whenClientIdIsInValid_thenReturn400() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_CLIENT_ID_IS_EMPTY)).andExpect(status().isBadRequest());

	}

	@Tag("uploadDocuments")
	@Test
	void whenRequestingServiceIsInValid_thenReturn400() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_REQUESTING_SERVICE_IS_EMPTY))
				.andExpect(status().isBadRequest());

	}
	
  @Tag("uploadDocuments")
	@Test
	void whenResourceTypeIsInValid_thenReturn400() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_EMPTY)).andExpect(status().isBadRequest());

	}

	@Tag("uploadDocuments")
	@Test
	void whenDocIdIsInValid_thenReturn422() throws Exception {

		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_DOCID_IS_EMPTY)).andExpect(status().isBadRequest());

	}
	
    @Tag("uploadDocuments")
	@Test
	void whenRequiredRequestField_IsNull_thenReturn400() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_REQUIRED_FIELD_IS_NULL)).andExpect(status().isBadRequest());
	}

	@Tag("uploadDocuments")
	@Test
	void whenResourceTypeIsCase_lawFirmIDNotPassed_thenReturn400() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_CASE)).andExpect(status().isBadRequest());
	}

	@Tag("uploadDocuments")
	@Test
	void whenResourceTypeIsCase_caseTypeNotPassed_thenReturn400() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_CASE_CASE_TYPE_IS_NULL))
				.andExpect(status().isBadRequest());
	}

	@Tag("uploadDocuments")
	@Test
	void whenResourceTypeIsCase_caseNumberNotPassed_thenReturn400() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_CASE_CASE_NUMBER_IS_NULL))
				.andExpect(status().isBadRequest());
	}

	@Tag("uploadDocuments")
    @Test
	void whenResourceTypeIsActOrJudgement_lawFirmIdNotPassed_thenReturn200() throws Exception {
		mockMvc.perform(post(AppConstants.UPLOAD_DOCUMENT_PATH).contentType("application/json")
				.content(Mocks.UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_ACT_JUDGEMENT)).andExpect(status().isOk());
	}

    @Test
    void whenValidInputForCreateSolutionId_thenReturn201() throws Exception {
        mockMvc.perform(post(AppConstants.SOLUTION_ID_PATH + AppConstants.CREATE_PATH)
                .contentType("application/json")
                .content(Mocks.CREATE_SOLUTION_ID_REQUEST))
                .andExpect(status().isCreated());
    }

    @Test
    void whenInvalidInputForCreateSolutionId_thenReturn400() throws Exception {
        mockMvc.perform(post(AppConstants.SOLUTION_ID_PATH + AppConstants.CREATE_PATH)
                .contentType("application/json")
                .content(Mocks.CREATE_SOLUTION_ID_400_REQUEST))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenInvalidInputForCreateSolutionId_thenReturn422() throws Exception {
        Mockito.when(llpDataManagerService.create(Mockito.any()))
                .thenThrow(new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SERVICE_UNAVAILABLE));
        mockMvc.perform(post(AppConstants.SOLUTION_ID_PATH + AppConstants.CREATE_PATH)
                .contentType("application/json")
                .content(Mocks.CREATE_SOLUTION_ID_REQUEST))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void whenInvalidInputForCreateSolutionId_thenReturn500() throws Exception {
        Mockito.when(llpDataManagerService.create(Mockito.any()))
                .thenThrow(new AppException(ErrorConstants.INTERNAL_SERVER_ERROR, ErrorConstants.INTERNAL_SERVER_ERROR));
        mockMvc.perform(post(AppConstants.SOLUTION_ID_PATH + AppConstants.CREATE_PATH)
                .contentType("application/json")
                .content(Mocks.CREATE_SOLUTION_ID_REQUEST))
                .andExpect(status().isInternalServerError());
    }

    @Tag("searchResources")
    @Test
    void whenValidInputForSearchResource_thenReturn200() throws Exception {
        FileMetaData fm = new FileMetaData();
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setFileMetaData(fm);
        List<SearchResponse> result = new ArrayList<>();
        result.add(searchResponse);

        Mockito.when(llpDataManagerService.searchResources(any())).thenReturn(result);
        MvcResult mvcResult = mockMvc.perform(post(AppConstants.SEARCH_PATH)
                .contentType(applicationJson)
                .content(Mocks.SEARCH_RESOURCE_REQUEST)
                .header(AppConstants.RR_ID, Mocks.RR_ID))
                .andExpect(status().isOk()).andReturn();
        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(
                objectMapper.writeValueAsString(result));
    }

    @Tag("searchResources")
    @Test
    void whenServiceReturnsNull_thenReturn404() throws Exception {
        Mockito.when(llpDataManagerService.searchResources(any())).thenReturn(null);
        mockMvc.perform(post(AppConstants.SEARCH_PATH).contentType(applicationJson)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.SEARCH_RESOURCE_REQUEST)).andExpect(status().isNotFound());
    }

    @Tag("searchResources")
    @Test
    void whenServiceReturnsEmptyList_thenReturn404() throws Exception {
        Mockito.when(llpDataManagerService.searchResources(any())).thenReturn(new ArrayList<>());
        mockMvc.perform(post(AppConstants.SEARCH_PATH).contentType(applicationJson)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.SEARCH_RESOURCE_REQUEST)).andExpect(status().isNotFound());
    }

    @Tag("searchResources")
    @Test
    void whenNoLawFirmIdForResourceTypeCase_thenReturn422() throws Exception {
        mockMvc.perform(post(AppConstants.SEARCH_PATH).contentType(applicationJson)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.SEARCH_RESOURCE_REQUEST_WITHOUT_LAWFIRM_FOR_RESOURCE_CASE)).andExpect(status().isBadRequest());
    }

    @Tag("searchResources")
    @Test
    void whenNoLawFirmIdForResourceTypeAll_thenReturn404() throws Exception {
        mockMvc.perform(post(AppConstants.SEARCH_PATH).contentType(applicationJson)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.SEARCH_RESOURCE_REQUEST_WITHOUT_LAWFIRM_FOR_RESOURCE_ALL)).andExpect(status().isNotFound());
    }

    @Tag("searchResources")
    @Test
    void whenNoLawFirmIdForResourceTypePublic_thenReturn200() throws Exception {
        List<SearchResponse> result = new ArrayList<>();
        result.add(new SearchResponse());

        Mockito.when(llpDataManagerService.searchResources(any())).thenReturn(result);
        mockMvc.perform(post(AppConstants.SEARCH_PATH).contentType(applicationJson)
                .header(AppConstants.RR_ID, Mocks.RR_ID)
                .content(Mocks.SEARCH_RESOURCE_REQUEST_WITHOUT_LAWFIRM_FOR_RESOURCE_PUBLIC)).andExpect(status().isOk());
    }

    @Tag("searchResources")
    @Test
    void whenInvalidRequestForSearch_thenReturn422() throws Exception {
        Mockito.when(llpDataManagerService.searchResources(any()))
                .thenThrow(new AppException(ErrorConstants.CLIENT_ID_EMPTY_OR_BLANK, ErrorConstants.CLIENT_ID_EMPTY_OR_BLANK));
        mockMvc.perform(post(AppConstants.SEARCH_PATH)
                .contentType(applicationJson)
                .content(Mocks.SEARCH_RESOURCE_REQUEST))
                .andExpect(status().isUnprocessableEntity());
    }

    @Tag("searchResources")
    @Test
    void whenSomethingWentWrongForSearch_thenReturn422() throws Exception {
        Mockito.when(llpDataManagerService.searchResources(any()))
                .thenThrow(new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SERVICE_UNAVAILABLE));
        mockMvc.perform(post(AppConstants.SEARCH_PATH)
                .contentType(applicationJson)
                .content(Mocks.SEARCH_RESOURCE_REQUEST))
                .andExpect(status().isUnprocessableEntity());
    }

    @Tag("searchResources")
    @Test
    void whenInvalidResourceType_thenReturn422() throws Exception {
        String requestWithInvalidResourceType = "{\"clientId\":\"1234\",\"searchText\":\"old juvenile\",\"lawFirmId\":\"1111\",\"solutionName\":\"LLP\",\"resourceType\":\"abcd\",\"numberOfResults\":100,\"rrId\":\"9c99969e-adb7-4eb0-a74b-b66501f7488e\"  }";
        Mockito.when(llpDataManagerService.searchResources(any()))
                .thenThrow(new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SERVICE_UNAVAILABLE));
        mockMvc.perform(post(AppConstants.SEARCH_PATH)
                .contentType(applicationJson)
                .content(requestWithInvalidResourceType))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenValidInputForLoadNerExtractedEntities_thenReturn200() throws Exception {
        mockMvc.perform(post(AppConstants.LOAD_NER_ENTITIES_PATH)
                .contentType("application/json")
                .content(Mocks.LOAD_NER_DATA_REQUEST))
                .andExpect(status().isOk());
    }
    @Test
    void whenInValidInputForLoadNerExtractedEntities_thenReturn400() throws Exception {
        mockMvc.perform(post(AppConstants.LOAD_NER_ENTITIES_PATH)
                .contentType("application/json")
                .content(Mocks.LOAD_NER_DATA_400_REQUEST))
                .andExpect(status().isBadRequest());
    }
    @Test
    void whenNoNerDocumentIdForLoadNerExtractedEntities_thenReturn400() throws Exception {
        mockMvc.perform(post(AppConstants.LOAD_NER_ENTITIES_PATH)
                .contentType("application/json")
                .content(Mocks.LOAD_NER_DATA_422_REQUEST))
                .andExpect(status().isBadRequest());
    }

    @Tag("getDocumentSummary")
    @Test
    void whenDataIsValid_return_200() throws Exception {
        Mockito.when(llpDataManagerService.getDocumentSummary(any(), any()))
                .thenReturn(Mocks.TEMPLATE_SALEDEED_FLAT);
        mockMvc.perform(post(AppConstants.GET_DOCUMENT_SUMMARY_PATH).contentType("application/json")
                .content(Mocks.GET_SUMMARY_TEMPLATE_VALID_REQUEST_BODY)).andExpect(status().isOk());
    }

    @Tag("getDocumentSummary")
    @Test
    void whenDataIsMissing_return_400() throws Exception {
        mockMvc.perform(post(AppConstants.GET_DOCUMENT_SUMMARY_PATH).contentType("application/json")
                .content(Mocks.WHEN_FIELD_MISSING)).andExpect(status().isBadRequest());
    }

    @Tag("getDocumentSummary")
    @Test
    void whenDataIsEmptyOrBlank_return_400() throws Exception {
        mockMvc.perform(post(AppConstants.GET_DOCUMENT_SUMMARY_PATH).contentType("application/json")
                .content(Mocks.WHEN_FIELD_IS_EMPTY_BLANK)).andExpect(status().isBadRequest());
    }

    @Tag("getDocumentSummary")
    @Test
    void whenNoDataFound_return_204() throws Exception {
        Mockito.when(llpDataManagerService.getDocumentSummary(any(), any()))
                .thenReturn(null);
        mockMvc.perform(post(AppConstants.GET_DOCUMENT_SUMMARY_PATH).contentType("application/json")
                .content(Mocks.GET_SUMMARY_TEMPLATE_VALID_REQUEST_BODY)).andExpect(status().isNoContent());
    }

    @Tag("getListOfSolutionRefId")
    @Test
    void whenInputIsValid_return_200() throws Exception {
        List<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();
        ListSolutionProcessIdResponse response = new ListSolutionProcessIdResponse();
        res.add(new HashMap<String, String>() {{
                put("solutionProcessRefId", "60a295f14341773a2722ba0c");
                put("lawFirmId", "1111");
                put("status", "completed");
            }});
        response.setListOfSolutionProcessId(res);
        response.setTotalRecords("3");
        Mockito.when(llpDataManagerService.getListOfSolutionRefId(any()))
                .thenReturn(response);
        mockMvc.perform(post(AppConstants.LIST_SOLUTION_PROCESS_REF_ID_PATH).contentType("application/json")
                .content(Mocks.REQUEST_MODEL)).andExpect(status().isOk());
    }

    @Tag("getListOfSolutionRefId")
    @Test
    void whenInputIsInValid_return_400() throws Exception {
        mockMvc.perform(post(AppConstants.LIST_SOLUTION_PROCESS_REF_ID_PATH).contentType("application/json")
                .content(Mocks.REQUEST_MODEL_400)).andExpect(status().isBadRequest());
    }

    @Tag("getListOfSolutionRefId")
    @Test
    void whenInputIsInValid_return_204() throws Exception {
        Mockito.when(llpDataManagerService.getListOfSolutionRefId(any()))
                .thenThrow(new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND));
        mockMvc.perform(post(AppConstants.LIST_SOLUTION_PROCESS_REF_ID_PATH).contentType("application/json")
                .content(Mocks.REQUEST_MODEL)).andExpect(status().isNoContent());
    }
}
