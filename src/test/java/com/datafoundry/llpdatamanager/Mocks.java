package com.datafoundry.llpdatamanager;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.datafoundry.llpdatamanager.models.*;
import org.bson.types.ObjectId;

import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.models.ESSearchResponse;
import com.datafoundry.llpdatamanager.models.ListSolutionProcessIdRequest;
import com.datafoundry.llpdatamanager.models.LoadNerDataRequest;
import com.datafoundry.llpdatamanager.models.NerData;
import com.datafoundry.llpdatamanager.models.ResourceMetaData;
import com.datafoundry.llpdatamanager.models.SearchRequest;
import com.datafoundry.llpdatamanager.models.SignedUrlResponse;
import com.datafoundry.llpdatamanager.models.SolutionIdCreate;
import com.datafoundry.llpdatamanager.models.SolutionIdUpdate;
import com.datafoundry.llpdatamanager.models.SummaryRequest;
import com.datafoundry.llpdatamanager.models.TemplateResponse;
import com.datafoundry.llpdatamanager.models.UploadRequestModel;


public class Mocks {

    public static final String REQUESTING_SERVICE = "llp-dm-service";
    public static final String SOLUTION_NAME = "llp";
    public static final String CLIENT_ID = "1234";
    public static final String SOLUTION_PROCESS_REF_ID = "6038f997f9e8c22e782d14cd";
    public static final String STATUS = "update from scg";

    public static final String RR_ID = "9c99969e-adb7-4eb0-a74b-b66501f7488e";
    public static final SolutionIdUpdate SOLUTION_ID_UPDATE_ONLY_STATUS = new SolutionIdUpdate.SolutionIdUpdateBuilder(REQUESTING_SERVICE)
            .setSolutionName(SOLUTION_NAME)
            .setClientId(CLIENT_ID)
            .setSolutionProcessId(SOLUTION_PROCESS_REF_ID)
            .setStatus(STATUS)
            .setRrId(RR_ID)
            .build();
    
    public static final SolutionIdCreate CREATE_SOLUTION_ID = new SolutionIdCreate(REQUESTING_SERVICE, SOLUTION_NAME, CLIENT_ID, RR_ID, STATUS, "1111");
    public static final String UPDATE_SOLUTION_ID_REQUEST = "{\"lawFirmId\":\"1111\",\"requestingService\":\"scg\",\"solutionName\":\"llp\",\"clientId\":\"1234\",\"solutionProcessId\":\"6038f997f9e8c22e782d14cd\",\"rrId\":\"rrId\",\"data\":{\"key1\":\"value1\",\"key2\":{\"key2k1\":\"key2v1\"}}}";
    public static final String LOAD_NER_DATA_REQUEST = "{\"clientId\":\"LRM\",\"sourceFileName\":\"123.pdf\", \"sourceDocumentId\":\"60bdc2e202e83e33b31978f8\",\"documentId\":\"6038f997f9e8c22e782d14cd\",\"lawFirmId\":\"1234\",\"solutionName\":\"llp\",\"resourceType\":\"saleDeed\",\"documentType\":\"saleDeed\",\"requestingService\":\"scg\",\"fileName\":\"SD_2005.PDF\",\"solutionProcessId\":\"6038f997f9e8c22e782d14cd\",\"sourceDocumentId\":\"6038f997f9e8c22e782d14cd\", \"nerData\": [{\"field_name\" : \"district\", \"field_value\" : \"Warangal\"}] }";
    public static final String LOAD_NER_DATA_400_REQUEST = "{\"clientId\":\"LRM\",\"lawFirmId\":\"1234\",\"solutionName\":\"llp\",\"documentType\":\"saleDeed\",\"requestingService\":\"scg\",\"solutionProcessId\":\"6038f997f9e8c22e782d14cd\",\"sourceDocumentId\":\"6038f997f9e8c22e782d14cd\"}";
    public static final String LOAD_NER_DATA_422_REQUEST = "{\"clientId\":\"LRM\",\"lawFirmId\":\"1234\",\"solutionName\":\"llp\",\"resourceType\":\"saleDeed\",\"documentType\":\"saleDeed\",\"requestingService\":\"scg\",\"fileName\":\"SD_2005.PDF\",\"solutionProcessId\":\"6038f997f9e8c22e782d14cd\",\"sourceDocumentId\":\" \",\"nerData\": [{\"field_name\" : \"district\", \"field_value\" : \"Warangal\"}]}";
    public static final String UPDATE_SOLUTION_ID_400_REQUEST = "{\"requestingService\":\"scg\",\"solutionName\":\"llp\",\"solutionProcessId\":\"6038f997f9e8c22e782d14cd\",\"rrId\":\"rrId\",\"data\":{\"key1\":\"value1\",\"key2\":{\"key2k1\":\"key2v1\"}}}";
    public static final String SEARCH_RESOURCE_REQUEST = "{\"clientId\":\"LRM\",\"searchText\":\"old juvenile\",\"lawFirmId\":\"1111\",\"solutionName\":\"llp\", \"isSummeryRequired\":false, \"resourceType\":\"act\",\"numberOfResults\":100  }";
    public static final String SEARCH_RESOURCE_REQUEST_WITHOUT_LAWFIRM_FOR_RESOURCE_CASE = "{\"clientId\":\"LRM\",\"searchText\":\"old juvenile\",\"lawFirmId\":\"\",    \"solutionName\":\"llp\",\"isSummeryRequired\":false,\"resourceType\":\"case\",\"numberOfResults\":100}";
    public static final String SEARCH_RESOURCE_REQUEST_WITHOUT_LAWFIRM_FOR_RESOURCE_ALL = "{\"clientId\":\"LRM\",\"searchText\":\"old juvenile\",\"lawFirmId\":\"\",    \"solutionName\":\"llp\",\"isSummeryRequired\":false,\"resourceType\":\"all\",\"numberOfResults\":100}";
    public static final String SEARCH_RESOURCE_REQUEST_WITHOUT_LAWFIRM_FOR_RESOURCE_PUBLIC = "{\"clientId\":\"LRM\",\"searchText\":\"old juvenile\",\"lawFirmId\":\"\",    \"solutionName\":\"llp\",\"isSummeryRequired\":false,\"resourceType\":\"act\",\"numberOfResults\":100}";
    public static final String CREATE_SOLUTION_ID_REQUEST = "{\"lawFirmId\":\"1111\",\"requestingService\":\"scg\",\"solutionName\":\"llp\",\"clientId\":\"1234\",\"solutionProcessId\":\"6038f997f9e8c22e782d14cd\",\"data\":{\"key1\":\"value1\"},\"rrId\":\"rrId\"}";
    public static final String CREATE_SOLUTION_ID_400_REQUEST = "{\"requestingService\":\"scg\",\"solutionName\":\"llp\",\"solutionProcessId\":\"6038f997f9e8c22e782d14cd\",\"rrId\":\"rrId\"}";
    public static Map<String, Object> DATA = Map.of("a", "A", "b", "B", "c", new Object());
    public static final SolutionIdUpdate SOLUTION_ID_UPDATE_BOTH_STATUS_AND_DATA = new SolutionIdUpdate.SolutionIdUpdateBuilder(REQUESTING_SERVICE)
            .setSolutionName(SOLUTION_NAME)
            .setClientId(CLIENT_ID)
            .setSolutionProcessId(SOLUTION_PROCESS_REF_ID)
            .setStatus(STATUS)
            .setData(DATA)
            .setRrId(RR_ID)
            .build();
    public static final SolutionIdUpdate SOLUTION_ID_UPDATE_ONLY_DATA = new SolutionIdUpdate.SolutionIdUpdateBuilder(REQUESTING_SERVICE)
            .setSolutionName(SOLUTION_NAME)
            .setClientId(CLIENT_ID)
            .setSolutionProcessId(SOLUTION_PROCESS_REF_ID)
            .setData(DATA)
            .setRrId(RR_ID)
            .build();

    public static final String SEARCH_TEXT = "search phrase";
    public static final String RESOURCE_TYPE = "act";
    public static final String DOCUMENT_TYPE_SALEDEED = "saledeed";
    public static final Integer NUM_OF_RESULTS = 100;
    public static final String FILE_NAME = "SD_668_OF_2014.pdf";
    public static final String DOCX_URL_CONSTANT= "https://llp-swag.df-dev.net";
    public static final SearchRequest SEARCH_REQUEST = new SearchRequest(CLIENT_ID,SEARCH_TEXT, CLIENT_ID, SOLUTION_NAME, RESOURCE_TYPE, NUM_OF_RESULTS, RR_ID);

    public static final String EXISTING_DOCUMENT_ID = "6057a2fc436f537213c704ed";
    public static final String EXISTING_URL = "https://llp-api.df-dev.net/docx/document/signed/download?id=605cdb2cfd2d3d7332c8fbb3&fileName=Constitution of India, 1950.pdf";
    public static final SignedUrlResponse SignedUrlResponse_Object = new SignedUrlResponse(EXISTING_URL, EXISTING_DOCUMENT_ID, true);


    public static final String HIGHLIGHT_FIELD_KEY_EXAMPLE = "Constitution of India, 1950.content";
    public static final Integer HIGHLIGHT_FIELD_VALUE_EXAMPLE = 2;
    public static Map<String, Object> mapObject() {
        Map<String, Object> map=new HashMap();
        map.put(HIGHLIGHT_FIELD_KEY_EXAMPLE, HIGHLIGHT_FIELD_VALUE_EXAMPLE);
        return map;
    }
    public static ESSearchResponse esSearchResponse_Object() {
        ESSearchResponse esSearchResponse = new ESSearchResponse();
        esSearchResponse.setHighLight(Mocks.mapObject());
        esSearchResponse.setSourceDocumentId(EXISTING_DOCUMENT_ID);
        return esSearchResponse;
    }

	public static final String UPLOAD_DOCUMENT_WHEN_REQUESTING_SERVICE_IS_EMPTY = "{\"clientId\":\"1234\",\"resourceType\":\"case\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"case\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"\"}";
	public static final String UPLOAD_DOCUMENT_WHEN_DOCID_IS_EMPTY = "{\"clientId\":\"1234\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"case\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"\"},\"requestingService\":\"scg-service\"}";
	public static final String UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_EMPTY = "{\"clientId\":\"1234\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"scg-service\"}";
	public static final String UPLOAD_DOCUMENT_WHEN_REQUIRED_FIELD_IS_NULL = "{\"clientId\":\"1234\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"case\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"scg-service\"}";
	public static final String UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_CASE = "{\"clientId\":\"1234\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"case\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"scg-service\"}";
	public static final String UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_CASE_CASE_TYPE_IS_NULL = "{\"clientId\":\"1234\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"case\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"scg-service\"}";
	public static final String UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_IS_CASE_CASE_NUMBER_IS_NULL = "{\"clientId\":\"1234\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"case\",\"caseNumber\":\"\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"scg-service\"}";
	public static final String UPLOAD_DOCUMENT_WHEN_RESOURCE_TYPE_ACT_JUDGEMENT = "{\"clientId\":\"1234\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"act\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"scg-service\"}";
    public static final String UPLOAD_DOCUMENT_WHEN_CLIENT_ID_IS_EMPTY = "{\"clientId\":\"\",\"ack\":\"d3b97ad7-467c-4777-9b91-f94ab31f3f2e\",\"solutionName\":\"llp\",\"lawFirmId\":\"1234\",\"solutionProcessId\":\"60b7c3e30a7bb501a5b831db\",\"fileData\":{\"resourceType\":\"case\",\"caseNumber\":\"1\",\"internalUrl\":\"http://docx-service/document/internal/download?id=60b7c3f6da10fd5c9e13a4d9\",\"fileName\":\"scg_CC No 213 of 2020.json\",\"version\":\"1.0\",\"status\":\"CREATED\",\"documentType\":\"saledeed\",\"caseType\":\"WP1\",\"documentId\":\"60b7c3f6da10fd5c9e13a4d9\"},\"requestingService\":\"scg-service\"}";

	//These for testing LLP-58
    public static final String LAWFIRM_ID = "1111";
    public static final String RESOURCE_TYPE_case = "case";
    public static final String CASE_TYPE = "cvcm";
    public static final String CASE_NUMBER = "123456789";
    public static final String DOCUMENT_ID = "6038f997f9e8c22e782d14cd";
    public static final ArrayList<NerData> NER_DATA = new ArrayList<NerData>();
    public static final String SCG_DOC_ID = "6038f997f9e8c22e782d14cd";
    public static final UploadRequestModel uploadRequestModel = new UploadRequestModel(LAWFIRM_ID, CLIENT_ID, SOLUTION_NAME, REQUESTING_SERVICE, SOLUTION_PROCESS_REF_ID, new FileData());
    public static final UploadRequestModel uploadRequestModels = new UploadRequestModel(null, CLIENT_ID, SOLUTION_NAME,  REQUESTING_SERVICE,SOLUTION_PROCESS_REF_ID, new FileData());
    public static final LoadNerDataRequest LOAD_NER_DATA = new LoadNerDataRequest(CLIENT_ID, LAWFIRM_ID, SOLUTION_NAME, RESOURCE_TYPE, DOCUMENT_TYPE_SALEDEED, REQUESTING_SERVICE, SOLUTION_PROCESS_REF_ID, FILE_NAME, DOCUMENT_ID, NER_DATA);
    public static final String docxBaseUrl = "http://docx-swag.df-dev.net/document/internal/download?";
	public static final String x = "{\n" +
            "   \"content\":[\n" +
            "      {\n" +
            "         \"topic\":\"memorandum\",\n" +
            "         \"content\":\"How Supreme Court\",\n" +
            "         \"page_number\":[\n" +
            "            2,\n" +
            "            3\n" +
            "         ],\n" +
            "         \"sub_topics\":[\n" +
            "            {\n" +
            "               \"sub_topic\":\"Allahabad High Court\",\n" +
            "               \"content\":\"In such a situation\",\n" +
            "               \"page_number\":[\n" +
            "                  3\n" +
            "               ]\n" +
            "            },\n" +
            "            {\n" +
            "               \"sub_topic\":\"between\",\n" +
            "               \"content\":\"The stay\",\n" +
            "               \"page_number\":[\n" +
            "                  3\n" +
            "               ]\n" +
            "            }\n" +
            "         ]\n" +
            "      }\n" +
            "   ],\n" +
            "   \"metaData\":{\n" +
            "      \"internalUrl\":\"url\",\n" +
            "      \"caseNumber\":\"O.S.NO 100 of 2012\",\n" +
            "      \"caseType\":\"OS\",\n" +
            "      \"resourceType\":\"judgement\",\n" +
            "      \"documentId\":\"60477cbb7531687bcd50952e\",\n" +
            "      \"fileName\":\"judgement_scg_output1\",\n" +
            "      \"version\":\"\",\n" +
            "      \"status\":\"\"\n" +
            "   }\n" +
            "}";

	//Testing for LLP_150
    public static final SummaryRequest SUMMARY_REQUEST =new SummaryRequest("1234","saledeed", "1111","llp","case","llp","hdcd","flat");
    public static final String TEMPLATE_SALEDEED_FLAT = "Ramesh S/o. Suresh purchased Flat bearing No. 203 admeasuring 1200 Sq. ft in Sujatha Enclave situated in Jubilee Hills from Rajesh S/o.  Mahesh for a consideration of Rs. 10,00,000 /- under sale deed dated 30.12.2013 bearing Document No. 45/2013.";
    public static final ResourceMetaData RESOURCE_META_DATA = new ResourceMetaData("1111","60990d57da318157df33c104s",Map.of("name of the district", "Warangal","nameofthevillage" , "ghato"),"llp","llp","hdcd");
    public static final TemplateResponse TEMPLATE_RESPONSE = new TemplateResponse("3333","saledeed","flat",TEMPLATE_SALEDEED_FLAT, "1111", "case", "llp");

    public static final String GET_SUMMARY_TEMPLATE_VALID_REQUEST_BODY = "{\"clientId\": \"1234\",\"documentType\": \"saledeed\",\"lawFirmId\": \"1111\",\"requestingService\": \"llp\",\"resourceType\": \"case\",\"solutionName\": \"llp\",\"solutionProcessId\": \"6099318dd366483cbe7928a3\",\"documentSubType\": \"flat\"}\n}";
    public static final String WHEN_FIELD_MISSING = "{ \"clientId\": \"1234\",\"lawFirmId\": \"1111\",\"requestingService\": \"llp\",\"resourceType\": \"case\",\"solutionName\": \"llp\",\"solutionProcessId\": \"6099318dd366483cbe7928a3\",\"documentSubType\": \"flat\"}\n}";
    public static final String WHEN_FIELD_IS_EMPTY_BLANK = "{\"clientId\": \"1234\",\"documentType\": \"\",\"lawFirmId\": \"1111\",\"requestingService\": \"llp\",\"resourceType\": \"case\",\"solutionName\": \"llp\",\"solutionProcessId\": \"6099318dd366483cbe7928a3\",\"documentSubType\": \"flat\"}\n}";

    //Testing for LLP-167
    public static final SolutionProcessRefModel SOLUTION_REF_ID_RESPONSE_MODEL = new SolutionProcessRefModel(new ObjectId("60a295f14341773a2722ba0c"),"1234", "lrm",Map.of("a", "A", "b", "B"), "completed", "llp", null,"","1111");
    public static final ListSolutionProcessIdRequest LIST_SOLUTION_REF_ID_REQUEST_MODEL = new ListSolutionProcessIdRequest("1234","1111", "lrm","llp","",0,0);
    public static final String REQUEST_MODEL= "{ \"requestingService\":\"llp\",\"lawFirmId\": \"1111\",\"clientId\": \"1234\", \"solutionName\": \"de\", \"solutionProcessIds\": \"60a3fced002dee265be7acde\"}";
    public static final String REQUEST_MODEL_400 = "{ \"requestingService\":\"llp\", \"lawFirmId\": \"1111\",\"solutionName\": \"de\", \"solutionProcessIds\": \"60a3fced002dee265be7acde\"}";
}
