package com.datafoundry.llpdatamanager.async;

import com.datafoundry.llpdatamanager.Mocks;
import com.datafoundry.llpdatamanager.client.ElasticSearchClient;
import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.LoadNerDataRequest;
import com.datafoundry.llpdatamanager.models.NerData;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.service.AsyncLLPDataManagerService;
import com.datafoundry.llpdatamanager.service.impl.AsyncLLPDataManagerImpl;
import com.datafoundry.llpdatamanager.service.impl.CaptureLogImpl;
import com.datafoundry.llpdatamanager.service.impl.DocxServiceImpl;
import com.datafoundry.llpdatamanager.service.impl.RestService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AsyncLLPDataManagerImplTests {

    @Mock
    MongoRepository<SolutionProcessRefModel> mongoRepository;

    @Mock
	ElasticSearchClient elasticSearchClient;

    @Mock
    RestService restService;

    @Mock
    CaptureLogImpl captureLog;

    @InjectMocks
    DocxServiceImpl docxService = new DocxServiceImpl();	 

    @Spy
    @InjectMocks
    AsyncLLPDataManagerService asyncLLPDataManager = new AsyncLLPDataManagerImpl();
 

	@Test
	public void createDocumentsIntoElasticsearch_Success() throws IOException {
		Mockito.when(restService.getResponse(any(), any(), any(), any(), any(),any())).thenReturn(new ResponseEntity<>(Mocks.x, HttpStatus.OK));
		asyncLLPDataManager.createDocumentsInElasticSearch(Mocks.RR_ID, Mocks.uploadRequestModel);
		verify(asyncLLPDataManager, times(1)).createDocumentsInElasticSearch(Mocks.RR_ID, Mocks.uploadRequestModel);
		Assertions.assertEquals(
				docxService.downloadDocumentsFromDocx(Mocks.RR_ID, Mocks.uploadRequestModel),
				Mocks.x);
	}

    @Test
    public void insertIntoNerCollection_Success() {
        LoadNerDataRequest loadNerDataRequest = new LoadNerDataRequest();
        ArrayList<NerData> entitiesList = new ArrayList<NerData>();
        asyncLLPDataManager.insertIntoNerCollection(loadNerDataRequest);
        verify(asyncLLPDataManager, times(1)).insertIntoNerCollection(loadNerDataRequest);
    }

    @Test
    public void uploadDocuments_Success() throws AppException, IOException {
        Mockito.doNothing().when(asyncLLPDataManager).createDocumentsInElasticSearch(Mocks.RR_ID, Mocks.uploadRequestModel);
        asyncLLPDataManager.uploadDocuments(Mocks.RR_ID, Mocks.uploadRequestModel);
        verify(asyncLLPDataManager, times(1)).uploadDocuments(Mocks.RR_ID, Mocks.uploadRequestModel);
    }
}