package com.datafoundry.llpdatamanager.service;

import com.datafoundry.llpdatamanager.Mocks;
import com.datafoundry.llpdatamanager.client.ElasticSearchClient;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.ESSearchResponse;
import com.datafoundry.llpdatamanager.service.impl.LLPElasticSearchServiceImpl;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LLPElasticSearchServiceTests {

    @Mock
    ElasticSearchClient elasticSearchClient;

    @InjectMocks
    LLPElasticSearchService llpElasticSearchService = new LLPElasticSearchServiceImpl();

    @Test
    public void getIndex_Success() {
        String lawFirmLd = new String();
        String indexName = llpElasticSearchService.getIndex(Mocks.RESOURCE_TYPE, lawFirmLd);
        Assertions.assertEquals(indexName, Mocks.CLIENT_ID);
    }

    @Test
    public void searchResource_Success() {

        List<ESSearchResponse> searchResult = new ArrayList<>();
        searchResult.add(Mocks.esSearchResponse_Object());
        when(elasticSearchClient.searchResource(Mocks.SEARCH_REQUEST, Mocks.RESOURCE_TYPE, Mocks.CLIENT_ID)).thenReturn(searchResult);
        List<ESSearchResponse> x = llpElasticSearchService.searchResource(Mocks.SEARCH_REQUEST, Mocks.RESOURCE_TYPE, Mocks.CLIENT_ID);
        Assertions.assertEquals(x.size(), 1);

    }

    @Test
    public void searchResource_Failure() {

        when(llpElasticSearchService.searchResource(Mocks.SEARCH_REQUEST, Mocks.RESOURCE_TYPE, Mocks.CLIENT_ID)).thenThrow(new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND));
        Throwable exception = Assertions.assertThrows(AppException.class, () -> llpElasticSearchService.searchResource(Mocks.SEARCH_REQUEST, Mocks.RESOURCE_TYPE, Mocks.CLIENT_ID));
        Assertions.assertEquals(exception.getMessage(), ErrorConstants.NO_DATA);

    }

}
