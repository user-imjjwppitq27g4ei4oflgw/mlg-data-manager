package com.datafoundry.llpdatamanager.service;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.datafoundry.llpdatamanager.Mocks;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.SignedUrlResponse;
import com.datafoundry.llpdatamanager.service.impl.CaptureLogImpl;
import com.datafoundry.llpdatamanager.service.impl.DocxServiceImpl;
import com.datafoundry.llpdatamanager.service.impl.RestService;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;


@ExtendWith(MockitoExtension.class)
public class DocxServiceTests {

    @Mock
    RestService restService;

    @Spy
    @InjectMocks
    DocxServiceImpl docxService = new DocxServiceImpl();
    
    @Mock
    CaptureLogImpl captureLog;    
    
    @Test
    public void generateSignedURL_Success() {
        List<String> ids = new ArrayList<>();
        ids.add(Mocks.EXISTING_DOCUMENT_ID);
        SignedUrlResponse[] arraySignedUrlResponse = new SignedUrlResponse[1];
        arraySignedUrlResponse[0] = Mocks.SignedUrlResponse_Object;
        ArrayList<SignedUrlResponse> signedUrlResponses_Array = new ArrayList<SignedUrlResponse>();
        signedUrlResponses_Array.add(Mocks.SignedUrlResponse_Object);
        docxService.setDocxUrl(Mocks.DOCX_URL_CONSTANT);
        when(restService.getResponse(any(), any(), any(), any(), any(), any())).thenReturn(new ResponseEntity<>(arraySignedUrlResponse, HttpStatus.OK));
        List<SignedUrlResponse> x = docxService.generateSignedURL(ids, Mocks.RR_ID, Mocks.CLIENT_ID);
        Assertions.assertEquals(x.size(), 1);
    }

    @Test
    public void generateSignedURL_Failure() {
        List<String> ids = new ArrayList<>();
        docxService.setDocxUrl(Mocks.DOCX_URL_CONSTANT);
        when(restService.getResponse(any(), any(), any(), any(), any(), any())).
                thenThrow(new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG));
        Throwable exception = Assertions.assertThrows(AppException.class, () ->docxService.generateSignedURL(ids, Mocks.RR_ID, Mocks.CLIENT_ID));
        Assertions.assertEquals(exception.getMessage(), ErrorConstants.SERVICE_UNAVAILABLE);

    }

    @Test
	public void downloadDocumentsFromDocx_Success() throws IOException {
		Mockito.when(restService.getResponse(any(), any(), any(), any(), any(), any()))
				.thenReturn(new ResponseEntity<>(Mocks.x, HttpStatus.OK));
		docxService.downloadDocumentsFromDocx(Mocks.RR_ID, Mocks.uploadRequestModel);
		verify(docxService, times(1)).downloadDocumentsFromDocx(Mocks.RR_ID, Mocks.uploadRequestModel);
		Assertions.assertEquals(docxService.downloadDocumentsFromDocx(Mocks.RR_ID, Mocks.uploadRequestModel), Mocks.x);
	}    
}
