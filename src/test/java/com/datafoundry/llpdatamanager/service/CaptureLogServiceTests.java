package com.datafoundry.llpdatamanager.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.datafoundry.llpdatamanager.Mocks;
import com.datafoundry.llpdatamanager.entity.ProcessLog;
import com.datafoundry.llpdatamanager.entity.RequestLog;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.utils.AppConstants;

@ExtendWith(MockitoExtension.class)
public class CaptureLogServiceTests {

	@Mock
	MongoRepository<RequestLog> mongoRepository;

	@Mock
	MongoRepository<ProcessLog> mongoRepo;

	@Mock
	CaptureLogService captureLogService;

	@Test
	public void captureRequestLog_Success() throws AppException {
		captureLogService.captureRequestLogs(Mocks.RR_ID, Mocks.uploadRequestModel, AppConstants.UPLOAD_REQUEST,
				AppConstants.SUCCESS);
		verify(captureLogService, times(1)).captureRequestLogs(Mocks.RR_ID, Mocks.uploadRequestModel,
				AppConstants.UPLOAD_REQUEST, AppConstants.SUCCESS);
	}

	@Test
	public void captureProcessLog_Success() throws AppException {
		captureLogService.captureProcessLogs(Mocks.RR_ID, Mocks.uploadRequestModel,
				AppConstants.ARE_DOCUMENTS_DOWNLOADING_FROM_DOCX, AppConstants.SUCCESS);
		verify(captureLogService, times(1)).captureProcessLogs(Mocks.RR_ID, Mocks.uploadRequestModel,
				AppConstants.ARE_DOCUMENTS_DOWNLOADING_FROM_DOCX, AppConstants.SUCCESS);
	}

	@Test
	public void updateStatusAndRemarks_Success() throws AppException {
		captureLogService.updateStatusAndRemarks(Mocks.RR_ID, AppConstants.SUCCESS, AppConstants.IS_REQUEST_COMPLETED,
				Mocks.uploadRequestModel);
		verify(captureLogService, times(1)).updateStatusAndRemarks(Mocks.RR_ID, AppConstants.SUCCESS,
				AppConstants.IS_REQUEST_COMPLETED, Mocks.uploadRequestModel);
	}
}
