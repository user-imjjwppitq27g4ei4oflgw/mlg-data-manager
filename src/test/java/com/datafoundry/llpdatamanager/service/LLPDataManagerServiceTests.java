package com.datafoundry.llpdatamanager.service;


import com.datafoundry.llpdatamanager.Mocks;
import com.datafoundry.llpdatamanager.client.ElasticSearchClient;
import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.service.impl.LLPDataManagerServiceImpl;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class LLPDataManagerServiceTests {

    @Mock
    MongoRepository<SolutionProcessRefModel> mongoRepository;

    @Mock
    MongoRepository<TemplateResponse> mongoRepo;

    @Mock
    ElasticSearchClient elasticSearchClient;

    @Mock
    MongoRepository<ResourceMetaData> mongoRepositoryObj;

    @Spy
    @InjectMocks
    LLPDataManagerService llpDataManagerService = new LLPDataManagerServiceImpl();

    @Mock
    CaptureLogService captureLog;

    @Mock
    AsyncLLPDataManagerService asyncLLPdataManager;
    
    @Mock
    LLPElasticSearchService llpElasticSearchService;

    @Mock
    DocxService docxService;


    @Test
    public void updateSolutionId_BothDataAndStatus_Success() throws AppException {
        Long updatedCount = 1L;
        when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(),
                Mockito.any(), Mockito.eq(SolutionProcessRefModel.class)))
                .thenReturn(updatedCount);
        Status result = llpDataManagerService.update(Mocks.SOLUTION_ID_UPDATE_BOTH_STATUS_AND_DATA);
        Assertions.assertEquals(result.getSuccess(), true);
    }

    @Test
    public void updateSolutionId_OnlyData_Success() throws AppException {
        Long updatedCount = 1L;
        when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(),
                Mockito.any(), Mockito.eq(SolutionProcessRefModel.class)))
                .thenReturn(updatedCount);
        Status result = llpDataManagerService.update(Mocks.SOLUTION_ID_UPDATE_ONLY_DATA);
        Assertions.assertEquals(result.getSuccess(), true);
    }

    @Test
    public void updateSolutionId_OnlyStatus_Success() throws AppException {
        Long updatedCount = 1L;
        when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(),
                Mockito.any(), Mockito.eq(SolutionProcessRefModel.class)))
                .thenReturn(updatedCount);
        Status result = llpDataManagerService.update(Mocks.SOLUTION_ID_UPDATE_ONLY_STATUS);
        Assertions.assertEquals(result.getSuccess(), true);
    }

    @Test
    public void updateSolutionId_Failure() {
        Long updatedCount = 0L;
        when(mongoRepository.updateOne(Mockito.any(Bson.class), Mockito.any(Bson.class), Mockito.any(),
                Mockito.any(), Mockito.eq(SolutionProcessRefModel.class)))
                .thenReturn(updatedCount);
        Throwable exception = Assertions.assertThrows(AppException.class, () -> llpDataManagerService.update(Mocks.SOLUTION_ID_UPDATE_BOTH_STATUS_AND_DATA));

        Assertions.assertEquals(exception.getMessage(), ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND);
    }



    @Test
    public void createSolutionId_Success() throws AppException {
        when(mongoRepository.save(Mockito.any(), Mockito.any(),
                Mockito.any(), Mockito.eq(SolutionProcessRefModel.class)))
                .thenReturn(true);
        String solutionProcessRefId = llpDataManagerService.create(Mocks.CREATE_SOLUTION_ID);
        Assertions.assertEquals(solutionProcessRefId.length(), 24);
    }

    @Test
    public void createSolutionId_Failure() throws AppException {
        when(mongoRepository.save(Mockito.any(), Mockito.any(),
                Mockito.any(), Mockito.eq(SolutionProcessRefModel.class)))
                .thenReturn(false);
        Throwable exception = Assertions.assertThrows(AppException.class, () -> llpDataManagerService.create(Mocks.CREATE_SOLUTION_ID));
        Assertions.assertEquals(exception.getMessage(), ErrorConstants.SOMETHING_WENT_WRONG);

    }

    @Test
    public void searchResources_Success() throws IOException {

        List<SignedUrlResponse> LIST_OF_SIGNED_URL_RESPONSE = new ArrayList<>();
        LIST_OF_SIGNED_URL_RESPONSE.add(Mocks.SignedUrlResponse_Object);
        List<ESSearchResponse> searchResult = new ArrayList<>();
        searchResult.add(Mocks.esSearchResponse_Object());
        List<ResourceMetaData> entityList = new ArrayList<>();
        ResourceMetaData resourceMetaData = new ResourceMetaData();
        resourceMetaData.setDocumentId(Mocks.EXISTING_DOCUMENT_ID);
        entityList.add(resourceMetaData);

        when(llpElasticSearchService.getIndex(any(), any())).thenReturn(Mocks.CLIENT_ID);
        when(llpElasticSearchService.searchResource(any(), any(), any())).thenReturn(searchResult);
        when(docxService.generateSignedURL(any(), any(), any())).thenReturn(LIST_OF_SIGNED_URL_RESPONSE);

        List<SearchResponse> cc = llpDataManagerService.searchResources(Mocks.SEARCH_REQUEST);
        Assertions.assertEquals(cc.size(),1);
    }

    @Test
    public void searchResources_Failure() throws AppException {
        when(llpElasticSearchService.searchResource(any(), any(), any())).thenThrow(new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND));
        Throwable exception = Assertions.assertThrows(AppException.class, () -> llpDataManagerService.searchResources(Mocks.SEARCH_REQUEST));
        Assertions.assertEquals(exception.getMessage(), ErrorConstants.NO_DATA);

    }

    @Test
    public void documentSummary_Sucess() throws AppException {
        when(mongoRepositoryObj.findOne(any(), any(), any(),any())).thenReturn(Mocks.RESOURCE_META_DATA);
        Bson whereFilter = Filters.in(AppConstants.SOLUTION_PROCESS_REF_ID, Mocks.SOLUTION_PROCESS_REF_ID);
        ResourceMetaData extrectedEntitiesResult = mongoRepositoryObj.findOne(whereFilter, "1111", AppConstants.EXTRACTED_ENTITY_COLLECTION,
                ResourceMetaData.class);

        when(mongoRepo.findOne(any(), any(), any(), any())).thenReturn(Mocks.TEMPLATE_RESPONSE);
        Bson whereFilters = Filters.and(Filters.in(AppConstants.DOCUMENT_TYPE, "saledeed"),
                Filters.eq(AppConstants.DOCUMENT_SUB_TYPE,"flat"));
        TemplateResponse templateResult = mongoRepo.findOne(whereFilters, "1111", AppConstants.TEMPLATE_COLLECTION,
                TemplateResponse.class);

//        String result = llpDataManagerService.getDocumentSummary(Mocks.RR_ID,Mocks.SUMMARY_REQUEST);

        Assertions.assertEquals(Mocks.RESOURCE_META_DATA, extrectedEntitiesResult);
        Assertions.assertEquals(Mocks.TEMPLATE_RESPONSE, templateResult);
//        Assertions.assertEquals(Mocks.TEMPLATE_SALEDEED_FLAT, result);
    }

    @Test
    public void documentSummary_Failure_When_Enities_Null(){
        Mockito.doThrow(new AppException(ErrorConstants.NOT_FOUND, ErrorConstants.NO_DATA_FOUND))
                .when(llpDataManagerService).getDocumentSummary(Mocks.RR_ID, Mocks.SUMMARY_REQUEST);
        Throwable exception = Assertions.assertThrows(AppException.class, () -> llpDataManagerService.getDocumentSummary(Mocks.RR_ID, Mocks.SUMMARY_REQUEST));
        Assertions.assertEquals(exception.getMessage(), ErrorConstants.NOT_FOUND);
    }

    @Test
    public void documentSummary_Failure_When_Template_Null(){
        Mockito.doThrow(new AppException(ErrorConstants.NOT_FOUND, ErrorConstants.NO_DATA_FOUND))
                .when(llpDataManagerService).getDocumentSummary(Mocks.RR_ID, Mocks.SUMMARY_REQUEST);
        Throwable exception = Assertions.assertThrows(AppException.class, () -> llpDataManagerService.getDocumentSummary(Mocks.RR_ID, Mocks.SUMMARY_REQUEST));
        Assertions.assertEquals(exception.getMessage(), ErrorConstants.NOT_FOUND);
    }

    @Test
    public void getListOfSolutionProcessId_Success() {
        ArrayList<SolutionProcessRefModel> listRefId = new ArrayList<SolutionProcessRefModel>();
        listRefId.add(Mocks.SOLUTION_REF_ID_RESPONSE_MODEL);
        Mockito.when(mongoRepository.findAll(any(), any(), any(), any(), any(), any())).thenReturn(listRefId);
        Mockito.when(mongoRepository.getCount(any(),any(),any(),any())).thenReturn(3L);
        List<HashMap<String, String>> res = new ArrayList<>();
        ListSolutionProcessIdResponse response = new ListSolutionProcessIdResponse();
        res.add(new HashMap<String, String>() {{
            put("solutionProcessId", "60a295f14341773a2722ba0c");
            put("status", "completed");
        }});
        response.setListOfSolutionProcessId(res);
        response.setLawFirmId("1111");
        response.setTotalRecords("3");

        ListSolutionProcessIdResponse result = llpDataManagerService.getListOfSolutionRefId(Mocks.LIST_SOLUTION_REF_ID_REQUEST_MODEL);
        Assertions.assertEquals(result.getListOfSolutionProcessId(), response.getListOfSolutionProcessId());
    }

    @Test
    public void getListOfSolutionProcessId_Failure() {
        Mockito.doThrow(new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG))
                .when(llpDataManagerService).getListOfSolutionRefId(Mocks.LIST_SOLUTION_REF_ID_REQUEST_MODEL);
        Throwable exception = Assertions.assertThrows(AppException.class, () -> llpDataManagerService.getListOfSolutionRefId(Mocks.LIST_SOLUTION_REF_ID_REQUEST_MODEL));
        Assertions.assertEquals(exception.getMessage(), ErrorConstants.SERVICE_UNAVAILABLE);
    }
}

