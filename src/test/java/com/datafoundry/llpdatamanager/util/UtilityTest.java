package com.datafoundry.llpdatamanager.util;

import com.datafoundry.llpdatamanager.Mocks;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.utils.Utility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UtilityTest {

	@Test
    public void testGetExceptionMessage() throws AppException {
        String errorMsg = Utility.getExceptionMessage(new NullPointerException());
        Assertions.assertEquals(true, errorMsg.startsWith("java.lang.NullPointerException"));
    }

    @Test
    public void testGetRRId() throws AppException {
        Assertions.assertEquals(Mocks.RR_ID, Utility.getRRId(Mocks.RR_ID));
        String rrId = Utility.getRRId(Mocks.RR_ID);
        Assertions.assertEquals(36, rrId.length());
        rrId = Utility.getRRId(null);
        Assertions.assertEquals(36, rrId.length());
    }
}