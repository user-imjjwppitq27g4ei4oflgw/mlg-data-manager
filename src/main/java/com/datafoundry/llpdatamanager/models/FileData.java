package com.datafoundry.llpdatamanager.models;

import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileData implements Serializable {

    @ApiModelProperty(example = "case/act/judgement", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String resourceType;

    @ApiModelProperty(example = "'001'")
    private String caseNumber;

    @ApiModelProperty(example = "http://docx-service/document/internal/download?id=60c05dc2d107bd2c7e3740b4\n", required = true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String internalUrl;

    @ApiModelProperty(example = "SD_6046 OF 2013.pdf", required = true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String fileName;

    @ApiModelProperty(example = "'0.1'")
    private String version;

    @ApiModelProperty(example = "uploading in elasticsearch", required = true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String status;

    @ApiModelProperty(example = "petition")
    private String documentType;

    @ApiModelProperty(example = "'case001'")
    private String caseType;

    @ApiModelProperty(example = "'6076ae1e97d7430aea349ee2'" , required = true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String documentId;

    @ApiModelProperty(example = "'6054fa19436f537213c7042c'")
    private String sourceDocumentId;

    public void setResourceType(String resourceType){
        this.resourceType = resourceType;
    }
    public String getResourceType(){
        return this.resourceType;
    }
    public void setCaseNumber(String caseNumber){
        this.caseNumber = caseNumber;
    }
    public String getCaseNumber(){
        return this.caseNumber;
    }
    public void setInternalUrl(String internalUrl){
        this.internalUrl = internalUrl;
    }
    public String getInternalUrl(){
        return this.internalUrl;
    }
    public void setFileName(String fileName){
        this.fileName = fileName;
    }
    public String getFileName(){
        return this.fileName;
    }
    public void setVersion(String version){
        this.version = version;
    }
    public String getVersion(){
        return this.version;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public String getStatus(){
        return this.status;
    }
    public void setDocumentType(String documentType){
        this.documentType = documentType;
    }
    public String getDocumentType(){
        return this.documentType;
    }
    public void setCaseType(String caseType){
        this.caseType = caseType;
    }
    public String getCaseType(){
        return this.caseType;
    }
    public void setDocumentId(String documentId){
        this.documentId = documentId;
    }
    public String getDocumentId(){
        return this.documentId;
    }
    public void setSourceDocumentId(String sourceDocumentId){
        this.sourceDocumentId = sourceDocumentId;
    }
    public String getSourceDocumentId(){
        return this.sourceDocumentId;
    }
}

