package com.datafoundry.llpdatamanager.models;

import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadRequestModel implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(example = "'1111'")
	private String lawFirmId;

	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@ApiModelProperty(example = "'1234'",required=true)
	private String clientId;

	@ApiModelProperty(example = "llp",required=true)
	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String solutionName;

	@ApiModelProperty(example = "lrm",required=true)
	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String requestingService;

	@ApiModelProperty(example = "'6099318dd366483cbe7928a3'",required=true)
	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String solutionProcessId;

	@ApiModelProperty(example = "'6099318dd366483cbe7928a7'",required=true)
	private String ack;

	@ApiModelProperty(example = "{}",required=true)
	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@Valid
	private FileData fileData;

	public String getAck() {
		return ack;
	}

	public void setAck(String ack) {
		this.ack = ack;
	}

	public FileData getFileData() {
		return fileData;
	}

	public void setFileData(FileData fileData) {
		this.fileData = fileData;
	}
	
	public void setLawFirmId(String lawFirmId) {
		this.lawFirmId = lawFirmId;
	}

	public String getLawFirmId() {
		return this.lawFirmId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}

	public String getSolutionName() {
		return this.solutionName;
	}


	public void setRequestingService(String requestingService) {
		this.requestingService = requestingService;
	}

	public String getRequestingService() {
		return this.requestingService;
	}

	public void setSolutionProcessId(String solutionProcessId) {
		this.solutionProcessId = solutionProcessId;
	}

	public String getSolutionProcessId() {
		return this.solutionProcessId;
	}



	public UploadRequestModel(String lawFirmId, @NotNull String clientId, @NotNull String solutionName, @NotNull String requestingService, @NotNull String solutionProcessId, @NotNull FileData fileData) {
		this.lawFirmId = lawFirmId;
		this.clientId = clientId;
		this.solutionName = solutionName;
		this.requestingService = requestingService;
		this.solutionProcessId = solutionProcessId;
		this.fileData = fileData;
	}
}
