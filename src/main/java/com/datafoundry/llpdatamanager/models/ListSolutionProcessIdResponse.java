package com.datafoundry.llpdatamanager.models;

import java.util.HashMap;
import java.util.List;

public class ListSolutionProcessIdResponse {
    List<HashMap<String, String>> listOfSolutionProcessId;
    String totalRecords;
    String lawFirmId;

    public ListSolutionProcessIdResponse(){

    }

    public List<HashMap<String, String>> getListOfSolutionProcessId() {
        return listOfSolutionProcessId;
    }

    public void setListOfSolutionProcessId(List<HashMap<String, String>> listOfSolutionProcessId) {
        this.listOfSolutionProcessId = listOfSolutionProcessId;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }



}
