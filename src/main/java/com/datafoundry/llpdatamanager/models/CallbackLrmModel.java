package com.datafoundry.llpdatamanager.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CallbackLrmModel {
    private String caseUniqueId;
    private String documentId;
//    private String ocrDocumentId;
//    private String scgDocumentId;
    private String serviceDocumentId;
    private String message;
    private String status;
    private Boolean indexed;
    private String requestStatus;
}

/*
{
"caseUniqueId": "123XXXX",
"documentId": "7987887",
"message": "<concatenate the message. ${fileName} has successfully completed the OCR/topics are created/entity has been extracted.>",
"status": "SUCCESS", // Possible values: SUCCESS, ERROR. In case of error, please send the appropriate error message.
"indexed" : true/false,
"requestStatus" : "IN_PROGRESS"
}
 */