package com.datafoundry.llpdatamanager.models;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for ResourceMetaData
 */
public class ResourceMetaData implements Serializable {
    private static final long serialVersionUID = 1646948L;
    private String caseNumber;
    private String caseType;
    private String prayer;
    private String courtName;
    private String documentId;
    private String lawFirmId;
    private String judgementNumber;
    private String actNumber;
    private String resourceType;
    private String fileName;
    private String fileCreatedDate;
    private String fileCreatedBy;
    private Map<String, Object> entities;
    private String requestingService;
    private String solutionName;
    private String solutionProcessRefId;

    public ResourceMetaData() {
    }

    public ResourceMetaData(String lawFirmId, String nerDocumentId, Map<String, Object> entities, String requestingService, String solutionName, String solutionProcessRefId){
        this.lawFirmId = lawFirmId;
        this.entities = entities;
        this.requestingService = requestingService;
        this.solutionName = solutionName;
        this.solutionProcessRefId = solutionProcessRefId;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

    public String getJudgementNumber() {
        return judgementNumber;
    }

    public void setJudgementNumber(String judgementNumber) {
        this.judgementNumber = judgementNumber;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getActNumber() {
        return actNumber;
    }

    public void setActNumber(String actNumber) {
        this.actNumber = actNumber;
    }

    public String getPrayer() {
        return prayer;
    }

    public void setPrayer(String prayer) {
        this.prayer = prayer;
    }

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public String getFileCreatedDate() {
        return fileCreatedDate;
    }

    public void setFileCreatedDate(String fileCreatedDate) {
        this.fileCreatedDate = fileCreatedDate;
    }


    public String getFileCreatedBy() {
        return fileCreatedBy;
    }

    public void setFileCreatedBy(String fileCreatedBy) {
        this.fileCreatedBy = fileCreatedBy;
    }

    public void setEntities(Map<String, Object> entities) {
        this.entities = entities;
    }

    public Map<String, Object> getEntities() {
        return this.entities;
    }

    public void setRequestingService(String requestingService) {
        this.requestingService = requestingService;
    }

    public String getRequestingService() {
        return this.requestingService;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getSolutionName() {
        return this.solutionName;
    }

    public void setSolutionProcessRefId(String solutionProcessRefId) {
        this.solutionProcessRefId = solutionProcessRefId;
    }

    public String getSolutionProcessRefId() {
        return this.solutionProcessRefId;
    }
}