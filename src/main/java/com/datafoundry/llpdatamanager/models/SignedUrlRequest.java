package com.datafoundry.llpdatamanager.models;

import java.io.Serializable;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for SignedUrlRequest
 */
public class SignedUrlRequest implements Serializable {
    private static final long serialVersionUID = 365246147L;
    String documentId;
    Integer ttl;

    public SignedUrlRequest(String documentId, Integer ttl) {
        this.documentId = documentId;
        this.ttl = ttl;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Integer getTtl() {
        return ttl;
    }

    public void setTtl(Integer ttl) {
        this.ttl = ttl;
    }
}
