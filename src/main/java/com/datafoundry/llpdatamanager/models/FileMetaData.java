package com.datafoundry.llpdatamanager.models;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for FileMetaData
 */
public class FileMetaData {
    private String sourceDocumentId;
    private String fileUrl;
    private String sourceFileName;


    public FileMetaData() {
    }

    public FileMetaData(String sourceDocumentId, String fileUrl, String sourceFileName) {
        this.sourceDocumentId = sourceDocumentId;
        this.fileUrl = fileUrl;
        this.sourceFileName = sourceFileName;
    }

    public String getSourceDocumentId() {
        return sourceDocumentId;
    }

    public void setSourceDocumentId(String sourceDocumentId) {
        this.sourceDocumentId = sourceDocumentId;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }
}
