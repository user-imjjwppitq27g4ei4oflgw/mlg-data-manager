package com.datafoundry.llpdatamanager.models;

import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for SearchRequest
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchRequest {
    @ApiModelProperty(example = "1234",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String clientId;

    @ApiModelProperty(example = "high court of Delhi",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String searchText;

    @ApiModelProperty(example = "1111")
    private String lawFirmId;

    @ApiModelProperty(example = "llp",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String solutionName;

    @ApiModelProperty(example = "case/act/judgement/all")
    private String resourceType;

    @ApiModelProperty(example = "1")
    private Integer numberOfResults;
    @ApiModelProperty(hidden = true)
    private String rrId;

    public SearchRequest(String clientId, String searchText, String lawFirmId, String solutionName, String resourceType, Integer numberOfResults,  String rrId) {
        this.clientId = clientId;
        this.searchText = searchText;
        this.lawFirmId = lawFirmId;
        this.solutionName = solutionName;
        this.resourceType = resourceType;
        this.numberOfResults = numberOfResults;
        this.rrId = rrId;
    }

    public SearchRequest() {
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

    public String getSolutionName() {
        return solutionName;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Integer getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Integer numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    public String getRrId() {
        return rrId;
    }

    public void setRrId(String rrId) {
        this.rrId = rrId;
    }

}
