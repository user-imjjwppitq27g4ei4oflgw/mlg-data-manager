package com.datafoundry.llpdatamanager.models;

import java.io.Serializable;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for SignedUrlResponse
 */
public class SignedUrlResponse implements Serializable {
    private static final long serialVersionUID = 14668534L;

    String url;
    String documentId;
    Boolean success;

    public SignedUrlResponse(String url, String documentId, Boolean success) {
        this.url = url;
        this.documentId = documentId;
        this.success = success;
    }

    public SignedUrlResponse() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
