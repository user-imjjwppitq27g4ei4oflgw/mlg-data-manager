package com.datafoundry.llpdatamanager.models;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CallbackModel {

    private String lawFirmId;

    private String solutionProcessId;

    private String sourceDocumentId;

    private String serviceDocumentId;

    private String workflowMessage;

    private String docProgress;

//    static public enum DocProgress {
//
//        DATA_MANAGER, OCR, SCG, NER
//    }

}