package com.datafoundry.llpdatamanager.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for SearchResponse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchResponse implements Serializable {
    private static final long serialVersionUID = 1676945L;
    private String caseType;
    private String caseNumber;
    private String resourceType;
    private String lawFirmId;
    private String judgementNumber;
    private Map<String, Object> contentMatched;
    private String actNumber;
    private FileMetaData fileMetaData;
    private String createdAt;
    private String createdBy;
    private String documentType;
    private String solutionProcessId;

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getSolutionProcessId() {
        return solutionProcessId;
    }

    public void setSolutionProcessId(String solutionProcessId) {
        this.solutionProcessId = solutionProcessId;
    }

    public SearchResponse() {
    }

    public SearchResponse(String caseType, String caseNumber, String resourceType, String lawFirmId, String judgementNumber, Map<String, Object> contentMatched, String actNumber, FileMetaData fileMetaData, String documentType, String solutionProcessId) {
        this.caseType = caseType;
        this.caseNumber = caseNumber;
        this.resourceType = resourceType;
        this.lawFirmId = lawFirmId;
        this.judgementNumber = judgementNumber;
        this.contentMatched = contentMatched;
        this.actNumber = actNumber;
        this.fileMetaData = fileMetaData;
        this.solutionProcessId = solutionProcessId;
        this.documentType = documentType;
    }

   public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

    public FileMetaData getFileMetaData() {
        return fileMetaData;
    }

    public void setFileMetaData(FileMetaData fileMetaData) {
        this.fileMetaData = fileMetaData;
    }

    public String getJudgementNumber() {
        return judgementNumber;
    }

    public void setJudgementNumber(String judgementNumber) {
        this.judgementNumber = judgementNumber;
    }

    public Map<String, Object> getContentMatched() {
        return contentMatched;
    }

    public void setContentMatched(Map<String, Object> contentMatched) {
        this.contentMatched = contentMatched;
    }

    public String getActNumber() {
        return actNumber;
    }

    public void setActNumber(String actNumber) {
        this.actNumber = actNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
