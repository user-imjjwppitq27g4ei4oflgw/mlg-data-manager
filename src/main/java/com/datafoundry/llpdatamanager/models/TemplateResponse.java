package com.datafoundry.llpdatamanager.models;

public class TemplateResponse {
    private String templateId;

    private String documentType;

    private String documentSubType;

    private String templateFormat;

    private String lawFirmId;

    private String resourceType;

    private String solutionName;

    public TemplateResponse() {
    }

    public TemplateResponse(String templateId, String documentType, String documentSubType, String templateFormat, String lawFirmId, String resourceType, String solutionName) {
        this.templateId = templateId;
        this.documentType = documentType;
        this.documentSubType = documentSubType;
        this.templateFormat = templateFormat;
        this.lawFirmId = lawFirmId;
        this.resourceType = resourceType;
        this.solutionName = solutionName;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateId() {
        return this.templateId;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setdocumentSubType(String documentSubType) {
        this.documentSubType = documentSubType;
    }

    public String getdocumentSubType() {
        return this.documentSubType;
    }

    public void setTemplateFormat(String templateFormat) {
        this.templateFormat = templateFormat;
    }

    public String getTemplateFormat() {
        return this.templateFormat;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

    public String getLawFirmId() {
        return this.lawFirmId;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceType() {
        return this.resourceType;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getSolutionName() {
        return this.solutionName;
    }
}
