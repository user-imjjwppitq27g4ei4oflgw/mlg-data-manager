package com.datafoundry.llpdatamanager.models;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OCRRequestBody {
    private String clientId;

    private String lawFirmId;

    private String solutionName;

    private String requestingService;

    private String solutionProcessId;

    private String requestType;

//    private String targetDomain;

    private List<OCRDocumentsData> documentsData;
}
