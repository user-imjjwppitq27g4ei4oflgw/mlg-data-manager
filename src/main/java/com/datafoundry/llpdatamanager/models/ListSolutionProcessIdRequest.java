package com.datafoundry.llpdatamanager.models;

import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ListSolutionProcessIdRequest {

    @ApiModelProperty(example = "lrm",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String requestingService;

    @ApiModelProperty(example = "1234",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String clientId;

    @ApiModelProperty(example = "llp",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String solutionName;

    @ApiModelProperty(example = "6099318dd366483cbe7928a3")
    private String solutionProcessId;

    @ApiModelProperty(example = "1111",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String lawFirmId;

    @ApiModelProperty(hidden = true)
    private String rrId;

    private Integer pageNumber;
    private Integer pageSize;

    public ListSolutionProcessIdRequest(){
    }

    public ListSolutionProcessIdRequest(String clientId, String lawFirmId, String requestingService, String solutionName, String solutionProcessId, Integer pageNumber, Integer pageSize){
    super();
    this.clientId = clientId;
    this.lawFirmId = lawFirmId;
    this.requestingService = requestingService;
    this.solutionName= solutionName;
    this.solutionProcessId = solutionProcessId;
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    }

    public void setRequestingService(String requestingService) {
        this.requestingService = requestingService;
    }

    public String getRequestingService() {
        return this.requestingService;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getSolutionName() {
        return this.solutionName;
    }

    public void setSolutionProcessId(String solutionProcessId) {
        this.solutionProcessId = solutionProcessId;
    }

    public String getSolutionProcessId() {
        return this.solutionProcessId;
    }

    public String getRrId() {
        return rrId;
    }

    public void setRrId(String rrId) {
        this.rrId = rrId;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

}


