package com.datafoundry.llpdatamanager.models;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import java.util.Map;

public class SolutionIdCreate {

    @ApiModelProperty(example = "lrm",required=true)
    @NotNull
    private String requestingService;
    @ApiModelProperty(example = "llp",required=true)
    @NotNull
    private String solutionName;
    @ApiModelProperty(example = "'1234'",required=true)
    @NotNull
    private String clientId;
    @ApiModelProperty(example = "'1111'", required=true)
    @NotNull
    private String lawFirmId;
    @ApiModelProperty(example = "request for uploading documents into llp")
    private String status;
    @NotNull
    private Map<String, Object> data;
    @ApiModelProperty(hidden = true)
    private String rrId;

    public SolutionIdCreate(@NotNull String requestingService, @NotNull String solutionName, @NotNull String clientId, String rrId, String status,  @NotNull String lawFirmId ) {
        this.requestingService = requestingService;
        this.solutionName = solutionName;
        this.clientId = clientId;
        this.status = status;
        this.rrId = rrId;
        this.lawFirmId = lawFirmId;
    }

    public String getRequestingService() {
        return requestingService;
    }

    public void setRequestingService(String requestingService) {
        this.requestingService = requestingService;
    }

    public String getSolutionName() {
        return solutionName;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public String getRrId() {
        return rrId;
    }

    public void setRrId(String rrId) {
        this.rrId = rrId;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }
}
