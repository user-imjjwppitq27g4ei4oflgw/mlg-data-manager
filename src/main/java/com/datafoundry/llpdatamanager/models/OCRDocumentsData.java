package com.datafoundry.llpdatamanager.models;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OCRDocumentsData {

	private String internalUrl;

	private String caseNumber;

	private String caseType;

	private String resourceType;

	private String sourceDocumentId;

	private String sourceFileName;

	private String documentType;

	private String documentId;

//    private String mode;

	private String fileName;

	private String version;

	private String status;

	private String folderPath;

	private Boolean extraction;
}
