package com.datafoundry.llpdatamanager.models;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class NerData implements Serializable {

    private static final long serialVersionUID = 1646948L;
    @ApiModelProperty(example = "district", required = true)
    private String field_name;
    @ApiModelProperty(example = "RangaReddy", required = true)
    private Object field_value;

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public Object getField_value() {
        return field_value;
    }

    public void setField_value(Object field_value) {
        this.field_value = field_value;
    }
}
