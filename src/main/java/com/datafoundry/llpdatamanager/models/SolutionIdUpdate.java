package com.datafoundry.llpdatamanager.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SolutionIdUpdate implements Serializable {

    private static final long serialVersionUID = 1646945L;

    @ApiModelProperty(example = "lrm",required=true)
    @NotNull
    private String requestingService;
    @ApiModelProperty(example = "llp",required=true)
    @NotNull
    private String solutionName;
    @ApiModelProperty(example = "'1234'",required=true)
    @NotNull
    private String clientId;
    @ApiModelProperty(example = "'6099318dd366483cbe7928a3'",required=true)
    @NotNull
    private String solutionProcessId;
    @ApiModelProperty(example = "uploading of documents in llp completed")
    private String status;
    private Map<String, Object> data;
    @ApiModelProperty(hidden = true)
    private String rrId;
    @ApiModelProperty(example = "'1111'",required=true)
    @NotNull
    private String lawFirmId;
    @NotNull
    private String sourceDocumentId; //sourceDocumentId

    @NotNull
    private String serviceDocumentId; //serviceDocumentId

    public static class SolutionIdUpdateBuilder {
        private String requestingService;
        private String solutionName;
        private String clientId;
        private String solutionProcessId;
        private String status;
        private Map<String, Object> data;
        private String rrId;
        private String lawFirmId;

        public SolutionIdUpdateBuilder (String requestingService) {
            this.requestingService = requestingService;
        }

        public SolutionIdUpdateBuilder  setSolutionName(String solutionName){
            this.solutionName = solutionName;
            return this;
        }
        public SolutionIdUpdateBuilder  setClientId(String clientId){
            this.clientId = clientId;
            return this;
        }
        public SolutionIdUpdateBuilder  setSolutionProcessId(String solutionProcessId){
            this.solutionProcessId = solutionProcessId;
            return this;
        }
        public SolutionIdUpdateBuilder  setStatus(String status){
            this.status = status;
            return this;
        }
        public SolutionIdUpdateBuilder  setData(Map<String, Object> data){
            this.data = data;
            return this;
        }
        public SolutionIdUpdateBuilder  setRrId(String rrId){
            this.rrId = rrId;
            return this;
        }

        public SolutionIdUpdateBuilder  setLawFirmId(String lawFirmId){
            this.lawFirmId = lawFirmId;
            return this;
        }

        public SolutionIdUpdate build() {
            SolutionIdUpdate solutionIdUpdate = new SolutionIdUpdate();
            solutionIdUpdate.requestingService = this.requestingService;
            solutionIdUpdate.solutionName = this.solutionName;
            solutionIdUpdate.clientId = this.clientId;
            solutionIdUpdate.solutionProcessId = this.solutionProcessId;
            solutionIdUpdate.status = this.status;
            solutionIdUpdate.data = this.data;
            solutionIdUpdate.rrId = this.rrId;
            solutionIdUpdate.lawFirmId = this.lawFirmId;
            return solutionIdUpdate;
        }

    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRequestingService() {
        return requestingService;
    }

    public SolutionIdUpdate() {
        super();
    }

    public void setRequestingService(String requestingService) {
        this.requestingService = requestingService;
    }

    public String getSolutionName() {
        return solutionName;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getRrId() {
        return rrId;
    }

    public void setRrId(String rrId) {
        this.rrId = rrId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public String getSolutionProcessId() {
        return solutionProcessId;
    }

    public void setSolutionProcessId(String solutionProcessId) {
        this.solutionProcessId = solutionProcessId;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }
    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

    public String getSourceDocumentId() {
        return sourceDocumentId;
    }

    public void setSourceDocumentId(String sourceDocumentId) {
        this.sourceDocumentId = sourceDocumentId;
    }

    public String getServiceDocumentId() {
        return serviceDocumentId;
    }

    public void setServiceDocumentId(String serviceDocumentId) {
        this.serviceDocumentId = serviceDocumentId;
    }
}
