package com.datafoundry.llpdatamanager.models;


import java.util.Map;
import java.util.Optional;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for ESResponse
 */
public class ESSearchResponse {
    private String sectionName;
    private String sourceDocumentId;
    private String resourceType;
    private String sourceFileName;
    private String documentType;
    private String caseType;
    private String caseNumber;
    private String solutionProcessId;
    private String judgementNumber;
    private String actNumber;
    private String lawFirmId;
    private Map<String, Object> highLight;

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

    public String getJudgementNumber() {
        return judgementNumber;
    }

    public void setJudgementNumber(String judgementNumber) {
        this.judgementNumber = judgementNumber;
    }

    public String getActNumber() {
        return actNumber;
    }

    public void setActNumber(String actNumber) {
        this.actNumber = actNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getSolutionProcessId() {
        return solutionProcessId;
    }

    public void setSolutionProcessId(String solutionProcessId) {
        this.solutionProcessId = solutionProcessId;
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSourceDocumentId() {
        return sourceDocumentId;
    }

    public void setSourceDocumentId(String sourceDocumentId) {
        this.sourceDocumentId = sourceDocumentId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Map<String, Object> getHighLight() {
        return highLight;
    }

    public void setHighLight(Map<String, Object> highLight) {
        this.highLight = highLight;
    }

}
