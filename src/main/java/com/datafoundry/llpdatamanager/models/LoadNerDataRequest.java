package com.datafoundry.llpdatamanager.models;

import java.io.Serializable;
import java.util.ArrayList;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.SwaggerConstants;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModelProperty;

public class LoadNerDataRequest implements Serializable {
    private static final long serialVersionUID = 14668534L;
    @ApiModelProperty(example = "'1234'", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String clientId;
    @ApiModelProperty(example = "'4567'", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String lawFirmId;
    @ApiModelProperty(example = "llp", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String solutionName;
    @ApiModelProperty(example = "case", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String resourceType;
    @ApiModelProperty(example = "salesdeed", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String documentType;
    @ApiModelProperty(example = "ner", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String requestingService;
    @ApiModelProperty(example = "'6038f997f9e8c22e782d14cd'", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String solutionProcessId;
    @ApiModelProperty(example = "SD_668_OF_2014.pdf", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String fileName;
    @ApiModelProperty(example = "'6038f997f9e8c22e782d14cd'", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String documentId;

    @ApiModelProperty(example = "'6038f997f9e8c22e782d14cf'", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String sourceDocumentId;

    @ApiModelProperty(example = "case.pdf", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String sourceFileName;

    @JsonDeserialize
    @ApiModelProperty(example = SwaggerConstants.NER_DATA_SWAGGER_EXAMPLE, required = true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotEmpty(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private ArrayList<NerData> nerData;
    @ApiModelProperty(hidden = true)
    private String rrId;


    public String getSourceDocumentId() {
        return sourceDocumentId;
    }

    public void setSourceDocumentId(String sourceDocumentId) {
        this.sourceDocumentId = sourceDocumentId;
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFirmId) {
        this.lawFirmId = lawFirmId;
    }

    public String getSolutionName() {
        return solutionName;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getRequestingService() {
        return requestingService;
    }

    public void setRequestingService(String requestingService) {
        this.requestingService = requestingService;
    }

    public String getSolutionProcessId() {
        return solutionProcessId;
    }

    public void setSolutionProcessId(String solutionProcessId) {
        this.solutionProcessId = solutionProcessId;
    }

    public String getRrId() {
        return rrId;
    }

    public void setRrId(String rrId) {
        this.rrId = rrId;
    }

    public ArrayList<NerData> getNerData() {
        return nerData;
    }

    public void setNerData(ArrayList<NerData> nerData) {
        this.nerData = nerData;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public LoadNerDataRequest() {
    }

    public LoadNerDataRequest(@NotNull String clientId, @NotNull String lawFirmId, @NotNull String solutionName, @NotNull String resourceType, @NotNull String documentType, @NotNull String requestingService, @NotNull String solutionProcessId,
                              @NotNull String fileName, @NotNull String documentId,
                              @NotNull ArrayList<NerData> nerData) {
        this.clientId = clientId;
        this.lawFirmId = lawFirmId;
        this.solutionName = solutionName;
        this.resourceType = resourceType;
        this.documentType = documentType;
        this.requestingService = requestingService;
        this.solutionProcessId = solutionProcessId;
        this.fileName = fileName;
        this.documentId = documentId;
        this.nerData = nerData;
    }
}
