package com.datafoundry.llpdatamanager.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 26/02/2021
 * @description Class for Status
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Status implements Serializable {

    private static final long serialVersionUID = 1696875L;
    private String message;
    private Boolean success;
    private Integer statusCode;
    private String solutionProcessId;
    private String acknowledgementId;
    private Object data;
    private String documentSummary;
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getSolutionProcessId() {
        return solutionProcessId;
    }

    public void setSolutionProcessId(String solutionProcessId) {
        this.solutionProcessId = solutionProcessId;
    }

	public String getAcknowledgementId() {
		return acknowledgementId;
	}

	public void setAcknowledgementId(String acknowledgementId) {
		this.acknowledgementId = acknowledgementId;
	}

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    
    public String getDocumentSummary() {
        return documentSummary;
    }

    public void setDocumentSummary(String documentSummary) {
        this.documentSummary = documentSummary;
    }

    public static class StatusBuilder {
        private String message;
        private Boolean success;
        private Integer statusCode;
        private String solutionProcessId;
        private String acknowledgementId;
        private Object data;
        private String documenySummary;

        public StatusBuilder(String message) {
            this.message = message;
        }

        public StatusBuilder(String message, Boolean success, Integer statusCode, String solutionProcessId, String acknowledgementId, Object data) {
            this.message = message;
            this.success = success;
            this.statusCode = statusCode;
            this.solutionProcessId = solutionProcessId;
            this.acknowledgementId = acknowledgementId;
            this.data = data;
        }

        public StatusBuilder isSuccess(Boolean success) {
            this.success = success;
            return this;
        }

        public StatusBuilder withStatusCode(Integer statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public StatusBuilder withSolutionProcessId(String solutionProcessId) {
            this.solutionProcessId = solutionProcessId;
            return this;
        }

        public StatusBuilder withAcknowledgementId(String acknowledgementId) {
            this.acknowledgementId = acknowledgementId;
            return this;
        }

        public StatusBuilder withData(Object  data){
            this.data = data;
            return this;
        }

         public StatusBuilder withDocumentSummary(String documentSummary){
            this.documenySummary = documentSummary;
            return this;
        }

        public Status build() {
            Status status = new Status();
            status.message = this.message;
            status.statusCode = this.statusCode;
            status.success = this.success;
            status.solutionProcessId = this.solutionProcessId;
            status.acknowledgementId = this.acknowledgementId;
            status.data = this.data;
            status.documentSummary = this.documenySummary;
            return status;
        }
    }
}
