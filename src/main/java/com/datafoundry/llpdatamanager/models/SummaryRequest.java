package com.datafoundry.llpdatamanager.models;

import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SummaryRequest {

    @ApiModelProperty(example = "1111", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
        private String lawFirmId;

    @ApiModelProperty(example = "1234",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
        private String clientId;

    @ApiModelProperty(example = "llp",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
        private String solutionName;

    @ApiModelProperty(example = "case/act/judgement", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
        private String resourceType;

    @ApiModelProperty(example = "saledeed", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
        private String documentType;

    @ApiModelProperty(example = "6099318dd366483cbe7928a3",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String solutionProcessId;


    @ApiModelProperty(example = "plot/flat/house/land", required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    private String documentSubType;


    @ApiModelProperty(example = "lrm",required=true)
    @NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
    @NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
        private String requestingService;


    public SummaryRequest(String clientId, String documentType, String lawFirmId, String requestingService, String resourceType, String solutionName, String solutionProcessId, String documentSubType){
        this.clientId = clientId;
        this.documentType = documentType;
        this.lawFirmId = lawFirmId;
        this.requestingService = requestingService;
        this.resourceType = resourceType;
        this.solutionName = solutionName;
        this.solutionProcessId = solutionProcessId;
        this.documentSubType = documentSubType;
    }


        public void setLawFirmId(String lawFirmId){
        this.lawFirmId = lawFirmId;
    }
        public String getLawFirmId(){
        return this.lawFirmId;
    }
        public void setClientId(String clientId){
        this.clientId = clientId;
    }
        public String getClientId(){
        return this.clientId;
    }
        public void setSolutionName(String solutionName){
        this.solutionName = solutionName;
    }
        public String getSolutionName(){
        return this.solutionName;
    }
        public void setResourceType(String resourceType){
        this.resourceType = resourceType;
    }
        public String getResourceType(){
        return this.resourceType;
    }
        public void setDocumentType(String documentType){
        this.documentType = documentType;
    }
        public String getDocumentType(){
        return this.documentType;
    }
        public void setDocumentSubType(String documentSubType){
        this.documentSubType = documentSubType;
    }
        public String getDocumentSubType(){
        return this.documentSubType;
    }
        public void setRequestingService(String requestingService){
        this.requestingService = requestingService;
    }
        public String getRequestingService(){
        return this.requestingService;
    }
        public void setSolutionProcessId(String solutionProcessId){
        this.solutionProcessId = solutionProcessId;
    }
        public String getSolutionProcessId(){
        return this.solutionProcessId;
    }
    }


