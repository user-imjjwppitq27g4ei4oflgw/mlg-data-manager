package com.datafoundry.llpdatamanager.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.function.Predicate;

/**
 * @author Dalee B
 * @version number Llpdatamanager v1.0
 * @created on 26/02/2021
 * @description Class for creating Swagger Config
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${llp-dm-service.base-url}")
    private String llpDatamanagerBaseURL;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).host(llpDatamanagerBaseURL.replaceFirst("^(http[s]?://)", "")).select().apis(RequestHandlerSelectors.any())
                .paths(Predicate.not(PathSelectors.regex("/error"))).build().apiInfo(metadata())
                .useDefaultResponseMessages(false);

    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder().title("LLP Data Manager Service API")
                .description("This is an application for handling LLP Data Manager").version("1.0.0").build();
    }
}
