package com.datafoundry.llpdatamanager.config;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Dalee B
 * @version number Llpdatamanager v1.0
 * @created on 26/02/2021
 * @description Config Class for Elastic Search Rest low level client
 */

@Configuration
public class ElasticSearchRestLowLevelClientConfiguration extends AbstractFactoryBean<RestClient> {
    private static final RRAdapter logger = new RRAdapterImpl(ElasticSearchRestLowLevelClientConfiguration.class.getName());
    @Value("${spring.data.elasticsearch.uri}")
    private String elasticSearchUri;
    @Value("${spring.data.elasticsearch.port}")
    private Integer elasticsearchPort;
    @Value("${spring.data.elasticsearch.scheme}")
    private String elasticsearchScheme;
    @Value("${app.rr-id}")
    private String app_rr_id;
    private RestClient restClient;

    @Override
    public Class<RestClient> getObjectType() {
        return RestClient.class;
    }

    @Override
    protected RestClient createInstance() throws Exception {
        return buildClient();
    }

    private RestClient buildClient() {
        try {
            restClient = RestClient.builder(
                    new HttpHost(elasticSearchUri, elasticsearchPort, elasticsearchScheme),
                    new HttpHost(elasticSearchUri, elasticsearchPort, elasticsearchScheme)).build();
        } catch (Exception e) {
            logger.error(app_rr_id, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(e), null);
        }
        return restClient;
    }

    @Override
    public void destroy() {
        try {
            if (restClient != null) {
                restClient.close();
            }
        } catch (final Exception e) {
            logger.error(app_rr_id, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(e), null);
        }
    }
}
