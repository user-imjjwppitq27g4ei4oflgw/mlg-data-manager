package com.datafoundry.llpdatamanager.config;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Dalee B
 * @version number Llpdatamanager v1.0
 * @created on 26/02/2021
 * @description Config Class for Elastic Search Rest high level client
 */

@Configuration
public class ElasticSearchRestHighLevelClientConfiguration extends AbstractFactoryBean<RestHighLevelClient> {

    private static final RRAdapter logger = new RRAdapterImpl(ElasticSearchRestHighLevelClientConfiguration.class.getName());
    @Value("${spring.data.elasticsearch.uri}")
    private String elasticSearchUri;
    @Value("${spring.data.elasticsearch.port}")
    private Integer elasticsearchHostPort;
    @Value("${spring.data.elasticsearch.scheme}")
    private String elasticsearchScheme;
    @Value("${app.rr-id}")
    private String app_rr_id;

    private RestHighLevelClient restHighLevelClient;

    @Override
    public void destroy() {
        try {
            if (restHighLevelClient != null) {
                restHighLevelClient.close();
            }
        } catch (final Exception e) {
            logger.error(app_rr_id, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(e), null);
        }
    }

    @Override
    public Class<RestHighLevelClient> getObjectType() {
        return RestHighLevelClient.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    @Override
    public RestHighLevelClient createInstance() {
        return buildClient();
    }

    private RestHighLevelClient buildClient() {
        try {
            restHighLevelClient = new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost(elasticSearchUri, elasticsearchHostPort, elasticsearchScheme),
                            new HttpHost(elasticSearchUri, elasticsearchHostPort, elasticsearchScheme)));
        } catch (Exception e) {
            logger.error(app_rr_id, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(e), null);
        }
        return restHighLevelClient;
    }

}


