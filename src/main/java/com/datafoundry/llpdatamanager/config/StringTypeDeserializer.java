package com.datafoundry.llpdatamanager.config;

import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

public class StringTypeDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonToken t = p.getCurrentToken();
        if (t.isBoolean()) {
            throw new AppException(ErrorConstants.INVALID_DATA_TEXT, ErrorConstants.INVALID_DATA_TEXT);
        } else if (t.isNumeric()) {
            throw new AppException(ErrorConstants.INVALID_DATA_TEXT, ErrorConstants.INVALID_DATA_TEXT);
        } else if (t.equals(JsonToken.VALUE_STRING)) {
            return p.getValueAsString();
        }
        return null;
    }
}
