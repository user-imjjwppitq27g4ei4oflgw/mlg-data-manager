//package com.datafoundry.llpdatamanager.repositories;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.data.mongodb.repository.MongoRepository;
//
//import com.datafoundry.llpdatamanager.entity.LRMDocument;
//import com.datafoundry.llpdatamanager.entity.LRMDocument.RequestStatus;
//
//public interface LRMDocumentRepository extends MongoRepository<LRMDocument, String> {
//
//    Optional<LRMDocument> findByDocumentIdAndRequestId(String documentId, String id);
//
//    List<LRMDocument> findByStatusNe(RequestStatus completed);
//
//}