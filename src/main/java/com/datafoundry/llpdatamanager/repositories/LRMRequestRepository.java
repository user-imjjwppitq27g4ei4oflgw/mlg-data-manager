//package com.datafoundry.llpdatamanager.repositories;
//
//import java.util.Optional;
//
//import org.springframework.data.mongodb.repository.MongoRepository;
//
//import com.datafoundry.llpdatamanager.entity.LrmRequest;
//
//public interface LRMRequestRepository extends MongoRepository<LrmRequest, String> {
//
//    Optional<LrmRequest> findBySolutionProcessId(String solutionProcessId);
//
//}