package com.datafoundry.llpdatamanager.repositories;

import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.exclude;
import static com.mongodb.client.model.Projections.excludeId;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dalee B
 * @version number Llpdatamanager v1.0
 * @created on 26/02/2021
 * @description Class for MongoDB connection
 */

@Repository
public class MongoRepository<T> {

    @Autowired
    MongoClient mongoClient;

    public MongoCollection<T> getCollection(String databaseName, String collection, Class<T> type) {
        return mongoClient.getDatabase(getDatabaseName(databaseName)).getCollection(collection, type);
    }

    public Long updateOne(Bson whereQuery, Bson updateObject, String databaseName, String collection, Class<T> type) {
        return (getCollection(databaseName, collection, type).updateOne(whereQuery, updateObject).getModifiedCount());
    }

    public Boolean save(T obj, String databaseName, String collection, Class<T> type) {
        return getCollection(databaseName, collection, type).insertOne(obj).wasAcknowledged();
    }

    public String getDatabaseName(String databaseName) {
        return databaseName.trim();
    }

    public List<T> findAll(Bson filter, Integer skip, Integer limit, String databaseName, String collection, Class<T> type) {
        return getCollection(databaseName, collection, type).find(filter).skip(skip).sort(new Document(AppConstants._ID, 1))
                .limit(limit).into(new ArrayList<>());
    }

    public List<T> findAllNoLimit(Bson filter, String databaseName, String collection, Class<T> type) {
        return getCollection(databaseName, collection, type).find(filter).into(new ArrayList<>());
    }

    public T findOne(Bson filter, String databaseName, String collection, Class<T> type) {
       return  getCollection(databaseName, collection, type).find(filter).first();
    }

    public Long getCount(Bson filter,String databaseName, String collection, Class<T> type){
       return getCollection(databaseName, collection, type).countDocuments(filter);
    }


    public List<T> findAllNoLimitWithProjection(Bson filter, String databaseName, String collection, Class<T> type) {
        return getCollection(databaseName, collection, type).find(filter)
                .projection(fields(exclude("clientId", "lawFirmId", "fileName", "requestingService", "createdAt", "createdBy",
                        "solutionName", "documentId"), excludeId()))
                .into(new ArrayList<>());

    }

    public Long update(Bson whereQuery, Bson updateObject, String databaseName, String collection, Class<T> type) {
        return (getCollection(databaseName, collection, type).updateMany(whereQuery, updateObject).getModifiedCount());
    }
}
