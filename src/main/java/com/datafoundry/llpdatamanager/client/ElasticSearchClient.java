package com.datafoundry.llpdatamanager.client;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.ESSearchResponse;
import com.datafoundry.llpdatamanager.models.ResourceMetaData;
import com.datafoundry.llpdatamanager.models.SearchRequest;
import com.datafoundry.llpdatamanager.models.UploadRequestModel;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.ResponseException;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for connecting to ElasticSearch
 */
@Component
public class ElasticSearchClient {
    RRAdapter logger = new RRAdapterImpl(ElasticSearchClient.class.getName());

    @Autowired
    RestClient restClient;
    
    @Value("${elasticsearch.config.stopwords.path}")
    private String esConfigPath;   
    

    public List<ESSearchResponse> searchResource(SearchRequest searchRequest, String resourceType, String index) {
        logger.info(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, "performing search to Elastic Server", null);
        List<ESSearchResponse> searchResult = new ArrayList<>();
        Request request = generateElasticSearchRequest(searchRequest, resourceType, index);
        try {
            Response response = restClient.performRequest(request);
            if (response.getStatusLine().getStatusCode() == org.apache.http.HttpStatus.SC_OK) {
                String responseBody = EntityUtils.toString(response.getEntity());
                searchResult = parseResponse(responseBody);
            }if(searchResult == null || searchResult.size() ==0) {
                throw new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND);
            }
        } catch (ResponseException ex) {
            if (ex.getResponse().getStatusLine().getStatusCode() == HttpStatus.NOT_FOUND.value()) {
                logger.error(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
                throw new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND);
            } else {
                logger.error(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
                throw new AppException(ErrorConstants.SOMETHING_WENT_WRONG, ErrorConstants.SOMETHING_WENT_WRONG);
            }
        } catch (IOException e) {
            logger.error(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(e), null);
            throw new AppException(ErrorConstants.SOMETHING_WENT_WRONG, ErrorConstants.SOMETHING_WENT_WRONG);
        }
        logger.info(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, "Fetching data from Elastic Server completed", null);
        return searchResult;
    }

    private List<ESSearchResponse> parseResponse(String responseBody) throws JSONException {
        JSONObject jsonObject = new JSONObject(responseBody.trim());
        List<ESSearchResponse> result = new ArrayList<>();
        if (jsonObject.get("hits") != null && jsonObject.get("hits") instanceof JSONObject) {
            JSONObject jsonObject1 = (JSONObject) jsonObject.get("hits");
            if (jsonObject1.get("hits") != null && jsonObject1.get("hits") instanceof JSONArray) {
                JSONArray jsonList = (JSONArray) jsonObject1.get("hits");
                if (jsonList.length() > 0) {
                    for (int i = 0; i < jsonList.length(); i++) {
                        JSONObject tempObject = jsonList.getJSONObject(i);
                        ESSearchResponse esSearchResponse = new ESSearchResponse();
                        JSONObject metaObject = tempObject.getJSONObject("_source").getJSONObject("metaData");
                        esSearchResponse.setHighLight(tempObject.getJSONObject("highlight").toMap());
                        esSearchResponse.setLawFirmId(tempObject.optString("_index"));
                        esSearchResponse.setSourceDocumentId(metaObject.optString(AppConstants.SOURCE_DOCUMENT_ID).toString());
                        esSearchResponse.setCaseType(metaObject.optString(AppConstants.CASE_TYPE, ""));
                        esSearchResponse.setCaseNumber(metaObject.optString(AppConstants.CASE_NUMBER, ""));
                        esSearchResponse.setDocumentType(metaObject.optString(AppConstants.DOCUMENT_TYPE, ""));
                        esSearchResponse.setSourceFileName(metaObject.optString(AppConstants.SOURCE_FILE_NAME, ""));
                        esSearchResponse.setSolutionProcessId(metaObject.optString(AppConstants.SOLUTION_PROCESS_ID, ""));
                        esSearchResponse.setResourceType(metaObject.optString(AppConstants.RESOURCE_TYPE, ""));
                        esSearchResponse.setActNumber(metaObject.optString(AppConstants.ACT_NUMBER, ""));
                        esSearchResponse.setJudgementNumber(metaObject.optString(AppConstants.JUDGEMENT_NUMBER, ""));
                        result.add(esSearchResponse);
                    }
                }
            }
        }
        return result;
    }

    private Request generateElasticSearchRequest(SearchRequest searchRequest, String resourceType, String index) {
        String query;
        String endpoint = null;
        if (searchRequest.getNumberOfResults() == null || searchRequest.getNumberOfResults() == 0) {
            searchRequest.setNumberOfResults(AppConstants.LIMIT);
        }
        if (AppConstants.Resource_Type_ALL.equalsIgnoreCase(resourceType)) {
            try {
                Boolean isPrivateIndexAvailable = false;
                query = String.format("{\"query\": {\"multi_match\" : {\"query\": \"%s\", \"analyzer\": \"search_analyzer\"}}," +
                        "\"highlight\": {\"fields\":{\"*.*\" : {}}," +
                        "\"type\":\"plain\","+
                        "\"pre_tags\" : [\"<b>\"]," +
                        "\"post_tags\" : [\"</b>\"]," +
                        "\"number_of_fragments\" : %d," +
                        "\"fragment_size\": %d }," +
                        "\"size\": %d " +
                        "}", searchRequest.getSearchText(), AppConstants.NUMBER_OF_FRAGMENT, AppConstants.FRAGMENT_SIZE, searchRequest.getNumberOfResults());


                if (index!=null && !index.equalsIgnoreCase(AppConstants.PUBLIC_INDEX))
                    isPrivateIndexAvailable = isIndexAvailable(searchRequest.getRrId(), index);

                Boolean isPublicIndexAvailable = isIndexAvailable(searchRequest.getRrId(), AppConstants.PUBLIC_INDEX);

                if (isPublicIndexAvailable && isPrivateIndexAvailable)
                    endpoint = String.format("/%s,%s/_search", index, AppConstants.PUBLIC_INDEX);
                else if (isPrivateIndexAvailable)
                    endpoint = String.format("%s/_search", index);
                else if (isPublicIndexAvailable)
                    endpoint = String.format("%s/_search", AppConstants.PUBLIC_INDEX);
            } catch (Exception e) {
                logger.error(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(e), null);
                throw new AppException(ErrorConstants.SOMETHING_WENT_WRONG, ErrorConstants.SOMETHING_WENT_WRONG);
            }
        } else {
            query = String.format(" { \"query\": {\"multi_match\": { \"query\":\"%s\" ,\"analyzer\": \"search_analyzer\" }}, " +
                    " \"highlight\": {\"fields\":{\"*.*\" : {}}," +
                    "\"type\":\"plain\","+
                    "\"pre_tags\" : [\"<b>\"]," +
                    "\"post_tags\" : [\"</b>\"]," +
                    "\"number_of_fragments\" : %d," +
                    "\"fragment_size\": %d},\"size\": %d, " +
                    "\"post_filter\": { \"term\": { \"metaData.resourceType\":\"%s\" } } }", searchRequest.getSearchText(), AppConstants.NUMBER_OF_FRAGMENT, AppConstants.FRAGMENT_SIZE, searchRequest.getNumberOfResults(), searchRequest.getResourceType().toLowerCase());

            endpoint = String.format("/%s/_search", (index));
        }
        Request request = new Request("GET", endpoint);
        request.setJsonEntity(query);
        return request;
    }
	
	/* checks if index is available or not */
	public Boolean isIndexAvailable(String rrId, String index) {
		try {
			logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Checking if a index is available in ElasticSearch or not",
					null);
			Request request = new Request("HEAD", "/" + index);
			Response indexExists = restClient.performRequest(request);
			return indexExists.getStatusLine().getStatusCode() == 200;

		}  catch (Exception ex) {
			logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ex.getMessage());
		}
	}

	/* create index */
	public Boolean createIndex(String rrId, UploadRequestModel uploadRequestModel) {
		try {
			logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Creating index for the given lawFirmId in ElasticSearch",
					null);
			String indexSettings = String.format(
					"{\"settings\": {\"analysis\": {\"analyzer\": {\"search_analyzer\": {\"type\": \"stop\",\"stopwords_path\":\"%s\",\"ignore_case\":\"true\"}}}}}",
					esConfigPath);
			Request request = new Request("PUT", "/" + uploadRequestModel.getLawFirmId());
			request.setJsonEntity(indexSettings);
			Response isIndexCreated = restClient.performRequest(request);
			return isIndexCreated.getStatusLine().getStatusCode() == 200;
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ex.getMessage());
		}
	}

	/* insert single document into Elasticsearch */
	public boolean insertInElasticSearch(String rrId, UploadRequestModel uploadRequestModel, String scgOutput) {
		try {
			logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Indexing documents in ElasticSearch for the given index",
					null);
			Request request = new Request("POST", String.format("%s/_doc", uploadRequestModel.getLawFirmId()));
			request.setJsonEntity(scgOutput);
			Response response = restClient.performRequest(request);
			return response.getStatusLine().getStatusCode() == 201;
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ex.getMessage());
		}
	}
}
