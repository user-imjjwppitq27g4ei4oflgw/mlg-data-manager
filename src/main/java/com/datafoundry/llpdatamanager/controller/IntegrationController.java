package com.datafoundry.llpdatamanager.controller;

import com.datafoundry.llpdatamanager.entity.ExtractedEntitiesModel;
import com.datafoundry.llpdatamanager.entity.LrmRequest;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.CallbackModel;
import com.datafoundry.llpdatamanager.models.Status;
import com.datafoundry.llpdatamanager.service.IntegrationService;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.SwaggerConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1")
public class IntegrationController {

    @Autowired
    IntegrationService integrationService;

    //this job is to trigger the workflow
    @ApiOperation(value = "To trigger workflow for extracting entities for uploaded document")
    @PostMapping(path = "/document/extract/entities")
    public ResponseEntity<Object> documentExtractEntities(@Valid @RequestBody LrmRequest lrmRequest, BindingResult bindingResult) throws AppException, IOException {

        if (bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, lrmRequest.getRrId()));
        }
        String solutionProcessId = integrationService.processLRMRequest(lrmRequest);
        Status status = new Status.StatusBuilder(SwaggerConstants.DATA_RECEIVED)
                .isSuccess(true)
                .withStatusCode(HttpStatus.OK.value()).withSolutionProcessId(solutionProcessId)
                .build();
        return new ResponseEntity<>(status, HttpStatus.OK);

    }

    @ApiOperation(value = "Call back status update in side the work flow.")
    @PutMapping(path = "/callbackApi")
    public ResponseEntity<Object> callback(@RequestBody CallbackModel model, BindingResult bindingResult) {

        String rrId = Utility.getRRId("");
        if (bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, "rrId"));
        }
        return integrationService.callback(model, rrId);
    }

    //GetExtractedEntities API
    @ApiOperation(value = SwaggerConstants.GET_EXTRACTED_ENTITIES, response = ExtractedEntitiesModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = AppConstants.ENTITIES_FOUND, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.GET_EXTRACTED_ENTITIES_RESULT, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = ErrorConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.BAD_REQUEST_RESPONSE_400, mediaType = "*/*"))),
            @ApiResponse(code = 404, message = ErrorConstants.NOT_FOUND, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SOLUTION_ID_404_RESPONSE, mediaType = "*/*"))),})
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE),
            @ApiImplicitParam(name = SwaggerConstants.SOLUTION_PROCESS_ID, value = SwaggerConstants.SOLUTION_PROCESS_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.SOLUTION_PROCESS_EXAMPLE)})
    @PostMapping(AppConstants.GET_EXTRACTED_ENTITIES_PATH)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ExtractedEntitiesModel>> getExtractedEntities(@RequestParam(name = SwaggerConstants.SOLUTION_PROCESS_ID, required = true) String solutionProcessId,
                                                                             @RequestParam(name = SwaggerConstants.LAW_FIRM_ID, required = true) String lawFirmId,
                                                                             @RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader) throws AppException {
        String rrId = Utility.getRRId(rrIdHeader);
        Utility.validateGetExtractionEntities(solutionProcessId, lawFirmId);
        List<ExtractedEntitiesModel> result = integrationService.getExtractedEntities(rrId, solutionProcessId, lawFirmId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


}