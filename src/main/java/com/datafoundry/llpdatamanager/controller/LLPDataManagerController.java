package com.datafoundry.llpdatamanager.controller;


import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import com.datafoundry.llpdatamanager.service.AsyncLLPDataManagerService;
import com.datafoundry.llpdatamanager.service.CaptureLogService;
import com.datafoundry.llpdatamanager.service.LLPDataManagerService;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.SwaggerConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/")
public class LLPDataManagerController {
    @Autowired
    LLPDataManagerService llpDataManagerService;

    @Autowired
    AsyncLLPDataManagerService asyncLLPDataManagerService;
    
    @Autowired
    CaptureLogService captureLog;

    @ApiOperation(value = SwaggerConstants.UPDATE_SOLUTION_ID_TITLE, response = SolutionProcessRefModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = SwaggerConstants.UPDATED_SOLUTION_ID_SUCCESS_MESSAGE, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPDATE_SOLUTION_ID_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SOLUTION_ID_400_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 404, message = SwaggerConstants.NOT_FOUND, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SEARCH_404_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SOLUTION_ID_422_RESPONSE, mediaType = "*/*"))) })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = SwaggerConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE)
    })
    @PutMapping(AppConstants.SOLUTION_ID_PATH + AppConstants.UPDATE_PATH)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Status> updateSolutionId(
            @RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader,
            @RequestBody @Valid SolutionIdUpdate solutionIdUpdate,
            BindingResult bindingResult
    ) throws AppException {
        solutionIdUpdate.setRrId(Utility.getRRId(rrIdHeader));
        if (bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, solutionIdUpdate.getRrId()));
        }
//        Utility.validateUpdateSolutionIdModel(solutionIdUpdate);
        llpDataManagerService.update(solutionIdUpdate);
        Status status = new Status.StatusBuilder(SwaggerConstants.UPDATED_SOLUTION_ID_SUCCESS_MESSAGE)
                .isSuccess(true)
                .withStatusCode(HttpStatus.OK.value())
                .build();
        return new ResponseEntity<>(status, HttpStatus.OK);

    }

    @ApiOperation(value = SwaggerConstants.CREATE_SOLUTION_ID_TITLE, response = SolutionProcessRefModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = SwaggerConstants.CREATED_SOLUTION_ID_SUCCESS_MESSAGE, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.CREATE_SOLUTION_ID_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SOLUTION_ID_400_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 422, message = SwaggerConstants.UNPROCESSABLE_ENTITY, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SOLUTION_ID_422_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 500, message = SwaggerConstants.INTERNAL_SERVER_ERROR, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SOLUTION_ID_500_RESPONSE, mediaType = "*/*")))})
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = SwaggerConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE)
    })
    @PostMapping(AppConstants.SOLUTION_ID_PATH + AppConstants.CREATE_PATH)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<Status> createSolutionId(
            @RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader,
            @RequestBody @Valid SolutionIdCreate solutionIdCreate,
            BindingResult bindingResult
    ) throws AppException {
        solutionIdCreate.setRrId(Utility.getRRId(rrIdHeader));
        if (bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, solutionIdCreate.getRrId()));
        }
        Utility.validateCreateSolutionIdModel(solutionIdCreate);
        String solutionProcessId = llpDataManagerService.create(solutionIdCreate);
        Status status = new Status.StatusBuilder(SwaggerConstants.CREATED_SOLUTION_ID_SUCCESS_MESSAGE)
                .isSuccess(true)
                .withStatusCode(HttpStatus.CREATED.value())
                .withSolutionProcessId(solutionProcessId)
                .build();
        return new ResponseEntity<>(status, HttpStatus.CREATED);

    }

    @ApiOperation(value = SwaggerConstants.SEARCH_DOCUMENT_TITLE, response = SolutionProcessRefModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = SwaggerConstants.SEARCH_SUCCESS_MESSAGE, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SEARCH_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 204, message = SwaggerConstants.NO_CONTENT, response = Status.class, examples = @Example(value = @ExampleProperty(value = "", mediaType = "*/*"))),
            @ApiResponse(code = 404, message = ErrorConstants.NOT_FOUND, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NO_DATA_FOUND, mediaType = "*/*"))),
    @ApiResponse(code = 400, message = SwaggerConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SEARCH_400_RESPONSE, mediaType = "*/*"))),})
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = SwaggerConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE)
})
    @PostMapping(AppConstants.SEARCH_PATH)

    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<List<SearchResponse>> searchResource(
            @RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader,
            @RequestBody @Valid SearchRequest searchRequest,
            BindingResult bindingResult
    ) throws AppException, IOException {

        searchRequest.setRrId(Utility.getRRId(rrIdHeader));
        if (bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, searchRequest.getRrId()));
        }
        Utility.setResourceType(searchRequest);
        Utility.validateSearchRequest(searchRequest);
        List<SearchResponse> result = llpDataManagerService.searchResources(searchRequest);
        if (result == null || result.size() == 0)
            throw new AppException(ErrorConstants.NOT_FOUND, ErrorConstants.NO_DATA_FOUND);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @ApiOperation(value = SwaggerConstants.UPLOAD_DOCUMENTS_TITLE, response = UploadRequestModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = AppConstants.DATA_RECEIVED, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPLOAD_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = ErrorConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.BAD_REQUEST_RESPONSE_WITH_ID, mediaType = "*/*"))),})
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE)})
    @PostMapping(AppConstants.UPLOAD_DOCUMENT_PATH)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Status> uploadDocuments(
            @RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader,
            @RequestBody @Valid UploadRequestModel uploadRequestModel, BindingResult bindingResult)
            throws AppException {
        String rrId = Utility.getRRId(rrIdHeader);
        try {
            if (bindingResult.hasErrors()) {
                throw new AppException(ErrorConstants.INVALID_DATA,
                        Utility.getFirstErrorInformation(bindingResult, rrId), rrId);
            }

            Utility.validateUploadDocumentsModel(uploadRequestModel, rrId, AppConstants.VALID_RESOURCE_TYPE);
            captureLog.captureRequestLogs(rrId, uploadRequestModel, AppConstants.UPLOAD_REQUEST, AppConstants.LOADING);

            asyncLLPDataManagerService.uploadDocuments(rrId, uploadRequestModel);
            Status status = new Status.StatusBuilder(AppConstants.DATA_RECEIVED).withAcknowledgementId(rrId)
                    .isSuccess(true).withStatusCode(HttpStatus.OK.value()).build();

            return new ResponseEntity<>(status, HttpStatus.OK);
        } catch (AppException ex) {
            captureLog.captureRequestLogs(rrId, uploadRequestModel, ex.getErrorMessage(), ErrorConstants.FAILURE);
            throw ex;
        }
    }
	
    @ApiOperation(value = SwaggerConstants.LOAD_NER_ENTITIES_TITLE, response = LoadNerDataRequest.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = AppConstants.DATA_RECEIVED, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.UPLOAD_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = ErrorConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.BAD_REQUEST_RESPONSE, mediaType = "*/*"))), })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = SwaggerConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE)
    })
    @PostMapping(AppConstants.LOAD_NER_ENTITIES_PATH)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Status> loadNerExtractedEntities(
            @RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader,
            @RequestBody @Valid LoadNerDataRequest loadNerDataRequest,
            BindingResult bindingResult
    ) throws AppException {
        loadNerDataRequest.setRrId(Utility.getRRId(rrIdHeader));
        if(bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, loadNerDataRequest.getRrId()));
        }
        Utility.validateNerInputRequestModel(loadNerDataRequest);
        captureLog.captureRequestLogs(loadNerDataRequest.getRrId(), loadNerDataRequest, AppConstants.NER_REQUEST_TO_LLPDM, AppConstants.LOADING);
        asyncLLPDataManagerService.insertIntoNerCollection(loadNerDataRequest);
        Status status = new Status.StatusBuilder(SwaggerConstants.DATA_RECEIVED)
                .isSuccess(true)
                .withStatusCode(HttpStatus.OK.value())
                .withAcknowledgementId(loadNerDataRequest.getRrId())
                .build();
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    
     @ApiOperation(value = SwaggerConstants.LIST_SOLUTION_REFID_TITLE, response = UploadRequestModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = AppConstants.DATA_RECEIVED, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.LIST_SOLUTION_REF_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 204, message = SwaggerConstants.NO_CONTENT, response = Status.class, examples = @Example(value = @ExampleProperty(value = "", mediaType = "*/*"))),
            @ApiResponse(code = 400, message = ErrorConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.BAD_REQUEST_RESPONSE_400, mediaType = "*/*"))),
            @ApiResponse(code = 404, message = ErrorConstants.NOT_FOUND, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.NO_DATA_FOUND, mediaType = "*/*"))),})
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE) })
    @PostMapping(AppConstants.LIST_SOLUTION_PROCESS_REF_ID_PATH)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Status> getListOfSolutionRefId(
             @RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader,
             @RequestBody @Valid ListSolutionProcessIdRequest listSolutionProcessIdRequest, BindingResult bindingResult)
            throws AppException {
        listSolutionProcessIdRequest.setRrId(Utility.getRRId(rrIdHeader));
        if (bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, listSolutionProcessIdRequest.getRrId()));
        }

        ListSolutionProcessIdResponse listOfSol = llpDataManagerService.getListOfSolutionRefId(listSolutionProcessIdRequest);

        Status status = new Status.StatusBuilder(AppConstants.DATA_RECEIVED).withData(listOfSol)
                .isSuccess(true).withStatusCode(HttpStatus.OK.value()).build();
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @ApiOperation(value = SwaggerConstants.DOCUMENT_SUMMARY_TITLE, response = SummaryRequest.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = AppConstants.DATA_RECEIVED, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.SUMMARY_SUCCESS_RESPONSE, mediaType = "*/*"))),
            @ApiResponse(code = 400, message = ErrorConstants.BAD_REQUEST, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.BAD_REQUEST_RESPONSE_400, mediaType = "*/*"))),
            @ApiResponse(code = 404, message = ErrorConstants.NOT_FOUND, response = Status.class, examples = @Example(value = @ExampleProperty(value = SwaggerConstants.TEMPLATE_NOT_FOUND_RESPONSE, mediaType = "*/*"))),})
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = SwaggerConstants.RR_ID, value = SwaggerConstants.RR_ID, paramType = AppConstants.HEADER, example = SwaggerConstants.RR_ID_EXAMPLE)})
    @PostMapping(AppConstants.GET_DOCUMENT_SUMMARY_PATH)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Status> documentSummary(@RequestHeader(name = SwaggerConstants.RR_ID, required = false) String rrIdHeader,
                                                  @RequestBody @Valid SummaryRequest summaryRequest, BindingResult bindingResult) throws AppException {
        String rrId = Utility.getRRId(rrIdHeader);
        if (bindingResult.hasErrors()) {
            throw new AppException(ErrorConstants.INVALID_DATA,
                    Utility.getFirstErrorInformation(bindingResult, rrId));
        }
        Utility.validateResourceType(AppConstants.VALID_RESOURCE_TYPE, summaryRequest.getResourceType(), rrId);
        String result = llpDataManagerService.getDocumentSummary(rrId, summaryRequest);

        if (result == null || result.length() == 0)
            throw new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND);

        Status status = new Status.StatusBuilder(AppConstants.SUMMARY_CREATED)
                .isSuccess(true).withStatusCode(HttpStatus.OK.value()).withDocumentSummary(result).build();

        return new ResponseEntity<>(status, HttpStatus.OK);
    }
}