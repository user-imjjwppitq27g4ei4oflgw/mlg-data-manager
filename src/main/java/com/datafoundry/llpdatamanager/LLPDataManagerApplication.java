package com.datafoundry.llpdatamanager;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import com.datafoundry.llpdatamanager.config.StringTypeDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;


@SpringBootApplication
@EnableAsync
public class LLPDataManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LLPDataManagerApplication.class, args);
    }

    @Bean
    public SimpleModule injectDeser() {
        return new SimpleModule().addDeserializer(String.class, new StringTypeDeserializer());
    }
        
    /* using this to handle 301 error*/
    @Bean
    public HttpComponentsClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        final HttpClient httpClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy())
			.build();
        clientHttpRequestFactory.setHttpClient(httpClient);
        return clientHttpRequestFactory;
    }    
    
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
        return restTemplate;
    }
}
