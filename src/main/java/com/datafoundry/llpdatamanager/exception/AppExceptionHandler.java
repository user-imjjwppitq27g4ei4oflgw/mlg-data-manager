package com.datafoundry.llpdatamanager.exception;

import com.datafoundry.llpdatamanager.models.Status;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Dalee B
 * @version number Llpdatamanager v1.0
 * @created on 26/02/2021
 * @description Class for generic exception Handler
 */

@Configuration
@ControllerAdvice(annotations = RestController.class)
public class AppExceptionHandler {

    @ExceptionHandler(value = {HttpMessageNotReadableException.class})
    public ResponseEntity<Status> handleHttpMessageNotReadable(HttpMessageNotReadableException ex) {
        return new ResponseEntity<Status>(new Status.StatusBuilder(ex.getMessage()).isSuccess(false)
                .withStatusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public ResponseEntity<Status> handleMissingParams(MissingServletRequestParameterException ex) {
        String exMessage = ex.getParameterName() + " is missing in the params";
        return new ResponseEntity<Status>(new Status.StatusBuilder(exMessage).isSuccess(false)
                .withStatusCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {AppException.class})
    public ResponseEntity<Status> handleCustomException(AppException ex) {

        Status.StatusBuilder statusBuilder = new Status.StatusBuilder(ex.getErrorMessage()).isSuccess(false);
        if (ex.getErrorCode().equals(ErrorConstants.INVALID_DATA) || ex.getErrorCode().equals(ErrorConstants.STATUS_DATA_BOTH_NULL))
            return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.BAD_REQUEST.value()).withAcknowledgementId(ex.getRequestRefId()).build(),
                    HttpStatus.BAD_REQUEST);
        else if (ex.getErrorCode().equals(ErrorConstants.NO_DATA))
            return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.NO_CONTENT.value()).build(),
                    HttpStatus.NO_CONTENT);
        if (ex.getErrorCode().equals(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND) || ex.getErrorCode().equals(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND) || ex.getErrorCode().equals(ErrorConstants.NOT_FOUND))
            return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.NOT_FOUND.value()).withAcknowledgementId(ex.getRequestRefId()).build(),
                    HttpStatus.NOT_FOUND);
        if (ex.getErrorCode().equals(ErrorConstants.NO_DATA))
            return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.NO_CONTENT.value()).withAcknowledgementId(ex.getRequestRefId()).build(),
                    HttpStatus.NO_CONTENT);
        if (ex.getErrorCode().equals(ErrorConstants.INTERNAL_SERVER_ERROR))
            return new ResponseEntity<Status>(statusBuilder.withStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).withAcknowledgementId(ex.getRequestRefId()).build(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<Status>(
                    statusBuilder.withStatusCode(HttpStatus.UNPROCESSABLE_ENTITY.value()).withAcknowledgementId(ex.getRequestRefId()).build(),
                    HttpStatus.UNPROCESSABLE_ENTITY);

    }

}
