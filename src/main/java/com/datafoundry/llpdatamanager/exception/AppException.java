package com.datafoundry.llpdatamanager.exception;

/**
 * @author Dalee B
 * @version number Llpdatamanager v1.0
 * @created on 26/02/2021
 * @description Class for Generic App level Exception
 */

public class AppException extends RuntimeException {

    private static final long serialVersionUID = 123234L;

    private String errorCode;

    private String errorMessage;

    private String requestRefId;

    public AppException(String errorCode, String errorMessage, String requestRefId) {
        super(errorCode);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.requestRefId = requestRefId;
    }

    public String getRequestRefId() {
        return requestRefId;
    }

    public void setRequestRefId(String requestRefId) {
        this.requestRefId = requestRefId;
    }

    public AppException(String errorCode, String errorMessage) {
        super(errorCode);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public AppException() {
        super("");
    }

    public AppException(String errorCode, Throwable cause) {
        super(errorCode, cause);
        this.errorCode = errorCode;
        this.errorMessage = cause.getMessage();

    }

    public AppException(String errorCode, Throwable cause, String requestRefId) {
        super(errorCode, cause);
        this.errorCode = errorCode;
        this.errorMessage = cause.getMessage();
        this.requestRefId = requestRefId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
