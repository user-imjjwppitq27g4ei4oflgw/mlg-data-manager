package com.datafoundry.llpdatamanager.utils;

import java.util.Arrays;
import java.util.List;

public class AppConstants {

    //Service names
    public static final String LLP_DATAMANAGER = "llpdatamanager";

    //Api endpoint paths
    public static final String SOLUTION_ID_PATH = "/solutionId";
    public static final String UPDATE_PATH = "/update";
    public static final String SEARCH_PATH = "/searchResource";
    public static final String GENERATE_SIGNED_URL_PATH = "/document/internal/generateSignedUrl";

    public static final String CREATE_PATH = "/create";
    public static final String UPLOAD_DOCUMENT_PATH = "/uploadDocument";
    public static final String LOAD_NER_ENTITIES_PATH = "/loadNerEntities";
    public static final String LIST_SOLUTION_PROCESS_REF_ID_PATH ="/getListOfSolutionId";
    public static final String GET_DOCUMENT_SUMMARY_PATH = "/getDocumentSummary";
    public static final String GET_EXTRACTED_ENTITIES_PATH = "/getExtractedEntities";

    //Collection names
    public static final String SOLUTION_PROCESS_REF_COLLECTION = "solutionProcessRef";
    public static final String EXTRACTED_ENTITY_COLLECTION = "extractedEntities";
    public static final String LRM_REQUEST = "lrmRequest";
    public static final String LRM_DOCUMENTS = "lrmDocuments";
    public static final String REQUEST_LOG_COLLECTION = "requestLog";
    public static final String PROCESS_LOG_COLLECTION = "processLog";
    public static final String TEMPLATE_COLLECTION = "template";

    //Constants and Fields names
    public static final String _ID = "_id";
    public static final String STATUS = "status";
    public static final String UPDATED_BY = "updatedBy";
    public static final String UPDATED_AT = "updatedAt";
    public static final String DATA = "data";
    public static final String CLIENT_Id = "clientId";
    public static final String LAW_FIRM_ID = "lawFirmId";
    public static final String USER_Id = "userId";
    public static final String SOLUTION_NAME = "solutionName";
    public static final String REQUESTING_SERVICE = "requestingService";
    public static final String SOLUTION_PROCESS_ID = "solutionProcessId";
    public static final Integer FRAGMENT_SIZE = 200;
    public static final Integer NUMBER_OF_FRAGMENT = 1;
    public static final Integer LIMIT = 100;
    public static final String DOCUMENT_ID = "documentId";
    public static final String USER_ID_VALUE = "5678";
    public static final String CONTENT = "content";
    public static final String CLIENT_NAME = "Law World";
    public static final String PUBLIC_INDEX = "1234";
    public static final String DOCUMENT_IdS = "documentIds";
    public static final Integer TTL_VALUE = 60;
    public static final String RR_ID = "rrId";
    public static final String SOLUTION_PROCESS_REF_ID = "solutionProcessRefId";
    public static final String REQUEST_STATUS = "requestStatus";

    public static final String ACT = "act";
    public static final String JUDGEMENT = "judgement";
    public static final String CASE = "case";
    public static final String REMARKS = "remarks";
    public static final String REQUEST_REFERENCE_ID  = "requestReferenceId";
    public static final String UPLOAD_REQUEST = "Request to upload documents";
    public static final String NER_REQUEST_TO_LLPDM = "Request to insert ner entities";
    public static final String LOADING = "Data is loading";
    public static final String DOCUMENT_TYPE = "documentType";
    public static final String DOCUMENT_SUB_TYPE = "documentSubType";
    public static final String RESOURCE_TYPE = "resourceType";
    public static final String CASE_NUMBER = "caseNumber";
    public static final String CASE_TYPE = "caseType";
    public static final String SOURCE_DOCUMENT_ID = "sourceDocumentId";
    public static final String SOURCE_FILE_NAME = "sourceFileName";
    public static final String ACT_NUMBER = "actNumber";
    public static final String JUDGEMENT_NUMBER = "judgementNumber";

    //Success Responses
    public static final String UPDATED_SUCCESSFULLY = "Data updated successfully";
    public static final String DATA_RECEIVED = "Data received!";
    public static final String SUCCESS = "success";
    public static final String SUMMARY_CREATED = "Summary created!";
    public static final String ENTITIES_FOUND = "Entities found!";

   

    //Programming constants
    public static final String ARE_DOCUMENTS_DOWNLOADING_FROM_DOCX  = "Are documents downloaded from Docx::";
    public static final String IS_INDEX_AVAILABLE = "Is index available::";
    public static final String IS_INDEX_CREATED = "Is index created::";
    public static final String ARE_DOCUMENTS_INDEXED = "Are documents indexed::";
    public static final String IS_REQUEST_COMPLETED = "Is documents upload completed::";
    public static final String INSERTED_NER_ENTITIES = "NER processing completed";
    public static final String NER_PROCESS_SUCCESS_AND_UPDATE_FAILED = "NER processing completed, failed to update solutionId";
    public static final String UPDATED_SOLUTION_ID_SUCCESS = "Updated given solutionProcessId successfully";
    public static final String INSERTING_NER_ENTITIES = "Started loading NER entities";
    public static final String NER_PROCESS_COMPLETED = "Processing NER request completed successfully";
    public static final String SCG_PROCESS_COMPLETED = "Processing SCG request completed successfully";
    public static final String UPDATED_SOLUTION_ID_FAILED = "Document uploaded successfully in Elasticsearch, failed to update solutionProcessId";
    public static final String COMPLETED = "COMPLETED";
    public static final String IN_PROGRESS = "IN PROGRESS";
    public static final String LLPDM_LOAD_ENTITIES = "llpDmLoadEntities";
    public static final String DOC_PROGRESS = "docProgress";
    public static final String STATE = "state";
    public static final String WORKFLOW_MESSAGE = "workflowMessage";

    public static final String UNKNOWN = "UNKNOWN";


    //Header description
    public static final String HEADER = "header";
    public static final String RR_Id = "RR_ID";

    // ResourceType/caseType/documentType
    public static final String Resource_Type_CASE = "case";
    public static final String Resource_Type_ALL = "all";
    public static final String SALEDEED = "saledeed";


    public static final List<String> VALID_RESOURCE_TYPE = Arrays.asList("act", "judgement","case");
    public static final List<String> VALID_RESOURCE_TYPE_SEARCH = Arrays.asList("act", "judgement","case", "all");
    public static final List<String> VALID_CASE_RESOURCE_TYPE = Arrays.asList("case");
}


