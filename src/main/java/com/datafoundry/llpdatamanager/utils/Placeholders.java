package com.datafoundry.llpdatamanager.utils;

public class Placeholders {
    //saledeed
    public static final String SELLER = "Seller";
    public static final String PURCHASER = "Purchaser";
    public static final String MANDAL = "Mandal";
    public static final String FLAT_NUMBER = "Flat Number";
    public static final String VILLAGE = "Village";
    public static final String PLOT_NUMBER = "Plot Number";
    public static final String CONSIDERATION_AMOUNT = "Consideration Amount";
    public static final String DISTRICT = "District";
    public static final String DATE_OF_DOCUMENT = "Date of document";
    public static final String AREA_SY_NUM = "Area_Sy_num";
    public static final String DOCUMENT_NO = "Document No";
    public static final String YEAR = "year";
    public static final String NAME_OF_APARTMENT = "Name of the apartment, if available";
    public static final String NAME_OF_AREA = "Name of the Area";
}

