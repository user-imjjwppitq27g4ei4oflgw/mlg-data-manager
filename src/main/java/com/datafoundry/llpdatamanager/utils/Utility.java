package com.datafoundry.llpdatamanager.utils;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import com.datafoundry.llpdatamanager.service.impl.LLPDataManagerServiceImpl;
import org.bson.types.ObjectId;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Utility {

    private static RRAdapter logger = new RRAdapterImpl(LLPDataManagerServiceImpl.class.getName());

    public static String getFirstErrorInformation(BindingResult bindingResult, String rrId) {
        String fieldName = null;
        logger.debug(rrId, AppConstants.LLP_DATAMANAGER, "++----Utility Methods--getErrorInformation()---++", null);
        StringBuilder sb = new StringBuilder();
        for (Object object : bindingResult.getAllErrors()) {
            if (object instanceof FieldError) {
                FieldError fieldError = (FieldError) object;
                sb.append(fieldError.getDefaultMessage().trim());
                fieldName = fieldError.getField();
                logger.debug(rrId, AppConstants.LLP_DATAMANAGER, "FIELD ERROR_" + fieldError.getField(), null);
                break;
            }

            if (object instanceof ObjectError) {
                ObjectError objectError = (ObjectError) object;
                sb.append(objectError.getObjectName());
                logger.debug(rrId, AppConstants.LLP_DATAMANAGER, "OBJECT_ERROR_" + objectError.getObjectName(), null);
                break;
            }

        }

        return fieldName + " " + sb.toString();
    }

    public static String getRRId(String rrId) {
        if (rrId == null || rrId.isEmpty())
            return UUID.randomUUID().toString();
        else
            return rrId;
    }

    public static String getExceptionMessage(Exception ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    public static void validateUpdateSolutionIdModel(SolutionIdUpdate solutionIdUpdate) {
        if (solutionIdUpdate.getData() == null && solutionIdUpdate.getStatus() == null){
            throw new AppException(ErrorConstants.STATUS_DATA_BOTH_NULL, ErrorConstants.STATUS_DATA_BOTH_NULL);
        } else if((solutionIdUpdate.getStatus() != null && (solutionIdUpdate.getStatus().isEmpty() || solutionIdUpdate.getStatus().isBlank())) &
                solutionIdUpdate.getData() != null && solutionIdUpdate.getData().isEmpty()) {
            throw new AppException(ErrorConstants.STATUS_OR_DATA_EMPTY, ErrorConstants.STATUS_OR_DATA_EMPTY);
        }else if(solutionIdUpdate.getStatus() == null && solutionIdUpdate.getData().isEmpty()){
            throw new AppException(ErrorConstants.STATUS_OR_DATA_EMPTY, ErrorConstants.STATUS_OR_DATA_EMPTY);
        } else if(solutionIdUpdate.getData() == null && (solutionIdUpdate.getStatus().isEmpty() || solutionIdUpdate.getStatus().isBlank())){
            throw new AppException(ErrorConstants.STATUS_OR_DATA_EMPTY, ErrorConstants.STATUS_OR_DATA_EMPTY);
        }

        else if(solutionIdUpdate.getClientId().isEmpty() || solutionIdUpdate.getClientId().isBlank() ){
            throw new AppException(ErrorConstants.CLIENT_ID_EMPTY_OR_BLANK, ErrorConstants.CLIENT_ID_EMPTY_OR_BLANK);
        } else if(solutionIdUpdate.getSolutionName().isEmpty() || solutionIdUpdate.getSolutionName().isBlank() ){
            throw new AppException(ErrorConstants.SOLUTION_NAME_EMPTY_OR_BLANK, ErrorConstants.SOLUTION_NAME_EMPTY_OR_BLANK);
        } else if(solutionIdUpdate.getRequestingService().isEmpty() || solutionIdUpdate.getRequestingService().isBlank() ){
            throw new AppException(ErrorConstants.REQUESTING_SERVICE_EMPTY_OR_BLANK, ErrorConstants.REQUESTING_SERVICE_EMPTY_OR_BLANK);
        } else if(solutionIdUpdate.getSolutionProcessId().isEmpty() || solutionIdUpdate.getSolutionProcessId().isBlank() ){
            throw new AppException(ErrorConstants.SOLUTION_PROCESS_ID_EMPTY_OR_BLANK, ErrorConstants.SOLUTION_PROCESS_ID_EMPTY_OR_BLANK);
        }if(!solutionIdUpdate.getSolutionProcessId().isEmpty() && !solutionIdUpdate.getSolutionProcessId().isBlank() ){
            try {
                ObjectId objectId = new ObjectId(solutionIdUpdate.getSolutionProcessId()); // to check if given solutionId can be converted to mongoDb ObjectId type
            } catch (Exception ex) {
                throw new AppException(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND, ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND);
            }
        }
    }

    public static void validateUploadDocumentsModel(UploadRequestModel uploadRequestModel, String ackId, List<String> listOfResorceType) {
        if (uploadRequestModel.getFileData().getResourceType().equalsIgnoreCase(AppConstants.CASE)) {
            if (isStringEmpty(uploadRequestModel.getLawFirmId()))
                throw new AppException(ErrorConstants.INVALID_DATA, AppConstants.LAW_FIRM_ID + ErrorConstants.SHOULD_NOT_NULL_BLANK, ackId);
            if (AppConstants.PUBLIC_INDEX.equalsIgnoreCase(uploadRequestModel.getLawFirmId()))
                throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.LAWFIRM_ID_SHOULD_NOT_BE_PUBLIC_FOR_RESOURCE_TYPE_CASE);
            if (isStringEmpty(uploadRequestModel.getFileData().getCaseType()))
                throw new AppException(ErrorConstants.INVALID_DATA, AppConstants.CASE_TYPE + ErrorConstants.SHOULD_NOT_NULL_BLANK, ackId);
            if (isStringEmpty(uploadRequestModel.getFileData().getCaseNumber()))
                throw new AppException(ErrorConstants.INVALID_DATA, AppConstants.CASE_NUMBER + ErrorConstants.SHOULD_NOT_NULL_BLANK, ackId);
            if (isStringEmpty(uploadRequestModel.getFileData().getDocumentType()))
                throw new AppException(ErrorConstants.INVALID_DATA, AppConstants.DOCUMENT_TYPE + ErrorConstants.SHOULD_NOT_NULL_BLANK, ackId);
        } else if (!isStringEmpty(uploadRequestModel.getFileData().getResourceType())) {
            if (!listOfResorceType.contains(uploadRequestModel.getFileData().getResourceType().toLowerCase()))
                throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.RESOURCE_TYPE_INVALID, ackId);
            if (uploadRequestModel.getFileData().getResourceType().equalsIgnoreCase(AppConstants.ACT) || uploadRequestModel.getFileData().getResourceType().equalsIgnoreCase(AppConstants.JUDGEMENT))
                uploadRequestModel.setLawFirmId("1234");
        }
    }

	public static void validateResourceType(List<String>listOfResorceType, String resourceType, String ack) {
		if (resourceType!=null) {
			if (!listOfResorceType.contains(resourceType)) {
				throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.RESOURCE_TYPE_INVALID, ack);
			}
		}
	}

    public static void validateCreateSolutionIdModel(SolutionIdCreate solutionIdCreate) {
        if(solutionIdCreate.getClientId().isEmpty() || solutionIdCreate.getClientId().isBlank() ){
            throw new AppException(ErrorConstants.CLIENT_ID_EMPTY_OR_BLANK, ErrorConstants.CLIENT_ID_EMPTY_OR_BLANK);
        } else if(solutionIdCreate.getSolutionName().isEmpty() || solutionIdCreate.getSolutionName().isBlank() ){
            throw new AppException(ErrorConstants.SOLUTION_NAME_EMPTY_OR_BLANK, ErrorConstants.SOLUTION_NAME_EMPTY_OR_BLANK);
        } else if(solutionIdCreate.getRequestingService().isEmpty() || solutionIdCreate.getRequestingService().isBlank() ){
            throw new AppException(ErrorConstants.REQUESTING_SERVICE_EMPTY_OR_BLANK, ErrorConstants.REQUESTING_SERVICE_EMPTY_OR_BLANK);
        }else if(solutionIdCreate.getStatus() != null && (solutionIdCreate.getStatus().isEmpty() || solutionIdCreate.getStatus().isBlank())) {
            throw new AppException(ErrorConstants.STATUS_EMPTY, ErrorConstants.STATUS_EMPTY);
        }else if(solutionIdCreate.getData() != null && solutionIdCreate.getData().isEmpty() ) {
            throw new AppException(ErrorConstants.DATA_EMPTY, ErrorConstants.DATA_EMPTY);
        }else if (solutionIdCreate.getLawFirmId() == null || solutionIdCreate.getLawFirmId().isEmpty() || solutionIdCreate.getLawFirmId().isBlank()) {
            throw new AppException(ErrorConstants.LAWFIRM_ID_EMPTY_OR_BLANK,ErrorConstants.LAWFIRM_ID_EMPTY_OR_BLANK);
        }
    }

    public static SolutionProcessRefModel createSolutionProcessRefDBModel(SolutionIdCreate solutionIdCreate) {
        ObjectId Id = new ObjectId();
        return new SolutionProcessRefModel(Id,
                solutionIdCreate.getClientId(), solutionIdCreate.getRequestingService(),
                solutionIdCreate.getData(), solutionIdCreate.getStatus(),solutionIdCreate.getSolutionName(),
                LocalDateTime.now(ZoneOffset.UTC), solutionIdCreate.getRequestingService(), solutionIdCreate.getLawFirmId()
            );
    }

    public static void setResourceType(SearchRequest searchRequest) {
        if (isStringEmpty(searchRequest.getResourceType()))
            searchRequest.setResourceType(AppConstants.Resource_Type_ALL);
        else searchRequest.setResourceType(searchRequest.getResourceType());
    }

    public static void validateSearchRequest(SearchRequest searchRequest) {
        if (!AppConstants.VALID_RESOURCE_TYPE_SEARCH.contains(searchRequest.getResourceType().toLowerCase())) {
            throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.INVALID_RESOURCE_TYPE);
        } else if ((AppConstants.VALID_CASE_RESOURCE_TYPE.contains(searchRequest.getResourceType().toLowerCase())) && isStringEmpty(searchRequest.getLawFirmId()))
            throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.LAWFIRM_ID_MISSING_FOR_CASETYPE_RESOURCE);
        else if (AppConstants.PUBLIC_INDEX.equalsIgnoreCase(searchRequest.getLawFirmId()) && (AppConstants.Resource_Type_CASE.equalsIgnoreCase(searchRequest.getResourceType()) || AppConstants.Resource_Type_ALL.equalsIgnoreCase(searchRequest.getResourceType()))) {
            throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.LAWFIRM_ID_SHOULD_NOT_BE_PUBLIC_FOR_CASETYPE);
        }
    }

    public static void validateNerInputRequestModel(LoadNerDataRequest loadNerDataRequest){
        ArrayList<NerData> objectArrayList = loadNerDataRequest.getNerData();
        for (int i = 0; i <= objectArrayList.size() - 1; i++) {
            if(objectArrayList.get(i).getField_name() == null || objectArrayList.get(i).getField_value() == null ) {
                throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.FIELD_NAME_NULL);
            }
            if(objectArrayList.get(i).getField_name().isBlank() || objectArrayList.get(i).getField_name().isEmpty()) {
                throw new AppException(ErrorConstants.INVALID_DATA, ErrorConstants.FIELD_NAME_EMPTY_OR_BLANK);
            }
        }

    }

    public static boolean isStringEmpty(String str){
		return str== null || str.isEmpty() || str.isBlank();
	}
	public static String isStringBlank(String str){return str== null || str.isEmpty() || str.isBlank()?null:str;}

    public static String getClientId(Map<String,Object> inputRequest, String defaultDB) {
        String clientIds = inputRequest.getOrDefault(AppConstants.CLIENT_Id, "").toString();
        if (clientIds != null && !isStringEmpty(clientIds))
            return inputRequest.get(AppConstants.CLIENT_Id).toString();
        else
            return defaultDB;
    }

    public static Object getValueFromMap(Map<String, Object> map, String key) {
        Object value = map.getOrDefault(key, "").toString();
        if (!Utility.isStringEmpty(value.toString())) {
            return value;
        } else {
            return AppConstants.UNKNOWN;
        }
    }

    public static Integer findOffSet(ListSolutionProcessIdRequest paginationRequest) {
        Integer skips = 0;
        if (paginationRequest.getPageSize() > 0 && paginationRequest.getPageNumber() > 0)
            skips = paginationRequest.getPageSize() * (paginationRequest.getPageNumber() - 1);
        return skips;
    }

    public static void getPageNumber(ListSolutionProcessIdRequest req) {
        if (req.getPageNumber()==null || req.getPageNumber()<1 )
            req.setPageNumber(1);
    }

    public static void getPageSize(ListSolutionProcessIdRequest req) {
        if (req.getPageSize()==null || req.getPageSize() < 1)
            req.setPageSize(10);
    }

    public static Boolean getNextPage(Integer totalCount, int pageSize, int pageNumber){
         return pageSize * pageNumber < totalCount ;
    }

    public static String getSolutionProcessId() {
        ObjectId Id = new ObjectId();
        return Id.toHexString();
    }

    @Async
    public static void logInfo(String rrId, String message) {
        logger.info(rrId, AppConstants.LLP_DATAMANAGER, message, null);

    }

    @Async
    public static void logError(String rrId, Exception message) {
        logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(message), null);

    }

    @Async
    public static void logWarn(String rrId, String message) {
        logger.warn(rrId, AppConstants.LLP_DATAMANAGER, message, null);

    }

    public static void validateGetExtractionEntities(String solutionProcessId, String lawFirmId) {
        if(solutionProcessId.isEmpty() || solutionProcessId.isBlank() || lawFirmId.isBlank() || lawFirmId.isEmpty()){
            throw new AppException(ErrorConstants.SOLUTION_PROCESS_ID_EMPTY_OR_BLANK, ErrorConstants.SOLUTION_PROCESS_ID_EMPTY_OR_BLANK);
        } else if(lawFirmId.isBlank() || lawFirmId.isEmpty()){
            throw new AppException(ErrorConstants.LAWFIRM_ID_EMPTY_OR_BLANK, ErrorConstants.LAWFIRM_ID_EMPTY_OR_BLANK);
        }
    }

}
