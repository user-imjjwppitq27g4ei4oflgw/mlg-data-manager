package com.datafoundry.llpdatamanager.utils;

public class SwaggerConstants {

    //RR_ID description
    public static final String RR_ID_EXAMPLE = "d300458c-946a-48a9-ba6c-04147b9b1a75";
    public static final String RR_ID = "rrId";
    public static final String HEADER = "header";

    //Api endpoint title
    public static final String UPDATE_SOLUTION_ID_TITLE = "To Update SolutionProcessRefId";
    public static final String CREATE_SOLUTION_ID_TITLE = "To Create SolutionProcessRefId";
    public static final String SEARCH_DOCUMENT_TITLE = "To Search Documents";
    public static final String UPLOAD_DOCUMENTS_TITLE = "To Upload Documents";
    public static final String LOAD_NER_ENTITIES_TITLE = "To Load Ner Entities";
    public static final String LIST_SOLUTION_REFID_TITLE = "To Get the List of SolutionProcessRefId";
    public static final String DOCUMENT_SUMMARY_TITLE = "To Get Summary of the Document";
    public static final String LAW_FIRM_ID = "lawFirmId";

    //Responses description
    public static final String UPDATED_SOLUTION_ID_SUCCESS_MESSAGE = "Data updated successfully";
    public static final String SEARCH_SUCCESS_MESSAGE = "Success";
    public static final String NO_CONTENT = "No Content";
    public static final String CREATED_SOLUTION_ID_SUCCESS_MESSAGE = "Created solutionId successfully";
    public static final String GET_EXTRACTED_ENTITIES = "To Get Extracted Entities";
    public static final String DATA_RECEIVED = "Data Received!";
    public static final String SOLUTION_PROCESS_ID = "solutionProcessId";
    public static final String SOLUTION_PROCESS_EXAMPLE = "6038f997f9e8c22e782d14c";

    //Responses
    public static final String UPDATE_SOLUTION_ID_SUCCESS_RESPONSE = "{\"message\":\"Data updated successfully\",\"success\":true,\"statusCode\":200}";
    public static final String SOLUTION_ID_400_RESPONSE = "{\"message\":\"clientId must not be null\",\"success\":false,\"statusCode\":400}";
    public static final String UPDATE_SOLUTION_ID_404_RESPONSE = "{\"message\":\"solutionProcessId not found\",\"success\":false,\"statusCode\":404}";
    public static final String SOLUTION_ID_422_RESPONSE = "{\"message\":\"clientId cannot be empty or blank\",\"success\":false,\"statusCode\":422}";
    public static final String SOLUTION_ID_500_RESPONSE = "{\"message\":\"Internal Server Error\",\"success\":false,\"statusCode\":500}";
    public static final String CREATE_SOLUTION_ID_SUCCESS_RESPONSE = "{\"message\":\"Created solutionId successfully\",\"success\":true,\"statusCode\":201, \"solutionProcessId\": \"6038f997f9e8c22e782d14cd\"}";

    //Errors messages
    public static final String BAD_REQUEST = "Bad Request!!";
    public static final String NOT_FOUND = "Not Found";
    public static final String UNPROCESSABLE_ENTITY = "Unprocessable Entity";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";

    public static final String SEARCH_204_RESPONSE = "{\"message\":\"No data found for the Lawfirm\",\"success\":false,\"statusCode\":204}";
    public static final String SEARCH_400_RESPONSE = "{\"message\": \"clientId should not be null or blank\", \"success\": false,\"statusCode\": 400}";
    public static final String SEARCH_404_RESPONSE = "{\n" +
            "    \"message\": \"No data found!\",\n" +
            "    \"success\": false,\n" +
            "    \"statusCode\": 404\n" +
            "}";

    public static final String SEARCH_SUCCESS_RESPONSE = "[\n" +
            "  {\n" +
            "    \"caseType\": \"AS\",\n" +
            "    \"caseNumber\": \"1\",\n" +
            "    \"resourceType\": \"case\",\n" +
            "    \"lawFirmId\": \"1111\",\n" +
            "    \"contentMatched\": {\n" +
            "      \"content.advocateDetails\": [\n" +
            "        \" for Defendant\\nno.4, Sri P.Shiv Kumar, Counsel for the Defendant no.6 and Counsel for\\n_ the Defendant Nos.2, 3 & 5 are set exparte and upon perusing the material\\n~-  on record, this <b>court</b> delivered the following:\\n\"\n" +
            "      ],\n" +
            "      \"content.courtName\": [\n" +
            "        \"IN THE <b>COURT</b> OF THE U1 ADDITIONAL SENIOR CIVIL JUDGE(F.T.C.),\\nRANGAREDDY DISTRICT AT L.B.NAGAR.\\nPresent:-Smt.D.Sarala Kumari, M.A., LL.M.,\\nVII Addl.Senior Civil Judge,\\nFAC III Addl.Senior Civil Judge(F.T.C.),\\nOn this the 9* day of September, 2011 .\\nO.5.No. 218 of 2003\"\n" +
            "      ],\n" +
            "      \"content.appendixOfEvidence\": [\n" +
            "        \"\\nAPPENDIX OF EVIDENCE\\nFAC 111 ADDL.SR.CIVIL JUDGE (F.T.C.)\\nRANGAREDDY DISTRICT\\n<b>COURT</b> CF -3Â¢ 109 TRiCT & | \\\"\\nSESETL P LS\\nRiâ\u0080\u0098;r.â\u0080\u009Cvâ\u0080\u0098:ï¬\u0081 3 'HÃ©;.-._.-',i\\\".,â\u0080\u0098 paeict 2 :\\nCALO IPL Y g Â©F 201\"\n" +
            "      ],\n" +
            "      \"content.judgment\": [\n" +
            "        \" to pay the said advance amount in 25 installments in his letter\\ndated 7-12-2002, and defendant no.3 filed a false suit in 0.5.N0.213/2003,\\nbefore this <b>Court</b> against the defendant nos.1,2,4,5 and 6\"\n" +
            "      ]\n" +
            "    },\n" +
            "    \"fileMetaData\": {\n" +
            "      \"sourceDocumentId\": \"60b74f37da10fd5c9e13a46b\",\n" +
            "      \"fileUrl\": \"https://llp-api.df-dev.net/docx/document/signed/download?id=60cf23d4f638f36954d19dc5&fileName=SD_10325  OF  2014.json\",\n" +
            "      \"sourceFileName\": \"decree.pdf\"\n" +
            "    },\n" +
            "    \"documentType\": \"judgement\",\n" +
            "    \"solutionProcessId\": \"60cf23d4f638f36954d19dc5\"\n" +
            "  }\n" +
            "]";

    public static final String UPLOAD_SUCCESS_RESPONSE = "{\r\n"
   		+ "  \"message\": \"Data Received!\",\r\n"
   		+ "  \"success\": true,\r\n"
   		+ "  \"statusCode\": 200,\r\n"
   		+ "  \"acknowledgementId\": \"d300458c-946a-48a9-ba6c-04147b9b1a75\"\r\n"
   		+ "}";
    
    
    public static final String BAD_REQUEST_RESPONSE = "{\r\n"
    		+ "  \"message\": \"requestingService must not be null\",\r\n"
    		+ "  \"success\": false,\r\n"
    		+ "  \"statusCode\": 400\r\n"
    		+ "}";
    
    public static final String UNPROCESSABLE_ENTITY_RESPONSE = "{\r\n"
    		+ "  \"message\": \"solutionName must not be blank or empty\",\r\n"
    		+ "  \"success\": false,\r\n"
    		+ "  \"statusCode\": 422\r\n"
    		+ "}";
    public static final String NER_DATA_SWAGGER_EXAMPLE = "[{\r\n"
                    + "  \"field_name\": \"district\",\r\n"
                    + "  \"field_value\": \"Warangal\"\r\n"
                    + "}]";


    public static final String LIST_SOLUTION_REF_RESPONSE = "{\n" +
            "    \"message\": \"Data received!\",\n" +
            "    \"success\": true,\n" +
            "    \"statusCode\": 200,\n" +
            "    \"data\": {\n" +
            "        \"listOfSolutionProcessId\": [\n" +
            "            {\n" +
            "                \"solutionProcessId\": \"60a295f14341773a2722ba09\",\n" +
            "                \"status\": \"Processing SCG request completed successfully\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"totalRecords\": \"1\",\n" +
            "        \"lawFirmId\":\"1111\"\n" +
            "    }\n" +
            "}";

       public static final String SUMMARY_SUCCESS_RESPONSE  ="{\n" +
               "    \"message\": \"Summary created!\",\n" +
               "    \"success\": true,\n" +
               "    \"statusCode\": 200,\n" +
               "    \"documentSummary\": \"ARUNA VANAM W/o Dr.Srinivasa  and 1 others purchased land admeasuring Sy.No 250 (Part), Ac.0-13 gts, Ac.2-09 gts at Gopanpally Village,Serilingampally Mandal,Ranga Reddy District from L.V.V.PRASAD for a consideration of Rs.Rs.50,00,000 under Sale deed dated this the 21st day of november 2013 bearing Document No.UNKNOWN/UNKNOWN\"\n" +
               "}";

    public static final String TEMPLATE_NOT_FOUND_RESPONSE = "{\n" +
            "    \"message\": \"template not found\",\n" +
            "    \"success\": false,\n" +
            "    \"statusCode\": 404\n" +
            "}";

    public static final String BAD_REQUEST_RESPONSE_400 = "{\r\n"
            + "  \"message\": \"requestingService must not be null or blank\",\r\n"
            + "  \"success\": false,\r\n"
            + "  \"statusCode\": 400\r\n"
            + "}";

    public static final String BAD_REQUEST_RESPONSE_WITH_ID ="{\n" +
            "    \"message\": \"requestingService must not be null or blank\",\n" +
            "    \"success\": false,\n" +
            "    \"statusCode\": 400,\n" +
            "    \"acknowledgementId\": \"3feb9984-85b1-43ef-9d5f-5ed5322c1e0a\"\n" +
            "}";

    public static final String NO_DATA_FOUND = "{\n" +
            "    \"message\": \"No data found!\",\n" +
            "    \"success\": false,\n" +
            "    \"statusCode\": 404\n" +
            "}";

    public static final String GET_EXTRACTED_ENTITIES_RESULT = "[\n" +
            "   {\n" +
            "       \"solutionProcessId\": \"6038f997f9e8c22e782d14c\",\n" +
            "       \"entities\":" +
            "        {\n" +
            "          \"purchaserName\": \"John smith\",\n" +
            "          \"sellerName\": \"Brown\"\n" +
            "        }\n" +
            "        \"sourceDocumentId\": \"60a295f14341773a2722ba09\",\n" +
            "        \"sourceFileName\": \"SD_6051 OF 2013.pdf\"\n" +
            "    }\n" +
            "   {\n" +
            "       \"solutionProcessId\": \"6038f997f9e8c22e782d14c\",\n" +
            "       \"entities\":" +
            "        {\n" +
            "          \"district\": \"RangaReddy\",\n" +
            "          \"village\": \"Sherlingampally\"\n" +
            "        }\n" +
            "        \"sourceDocumentId\": \"60a295f14341773a2722ba09\",\n" +
            "        \"sourceFileName\": \"SD_6053 OF 2013.pdf\"\n" +
            "    }\n" +

            "]";

    public static final String SOLUTION_ID_404_RESPONSE = "{\n" +
            "   \"message\":\"solutionProcessId not found\",\n" +
            "   \"success\":false,\n" +
            "   \"statusCode\":404\n" +
            "`}";
}
