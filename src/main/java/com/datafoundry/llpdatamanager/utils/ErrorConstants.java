package com.datafoundry.llpdatamanager.utils;

public class ErrorConstants {
    // Exceptions
    public static final String INVALID_DATA = "INVALID_DATA";
    public static final String SERVICE_UNAVAILABLE = "SERVICE_UNAVAILABLE";
    public static final String NO_DATA = "NO_DATA";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
    public static final String SOLUTION_PROCESS_REF_ID_NOT_FOUND = "solutionProcessId not found";
    public static final String BAD_REQUEST = "Bad Request!!";
    public static final String NOT_FOUND = "Not Found";
    public static final String UNPROCESSABLE_ENTITY = "Unprocessable Entity";
    public static final String FAILURE = "failed";

    
    //     Response Error
    public static final String SOMETHING_WENT_WRONG = "Something went wrong!!, Please try again later";
    public static final String INVALID_DATA_TEXT = "Invalid data passed in request";
    public static final String NO_DATA_FOUND = "No data found!";
    public static final String STATUS_DATA_BOTH_NULL = "status/data is required for update";
    public static final String STATUS_OR_DATA_EMPTY = "status/data should not be empty or blank";
    public static final String CLIENT_ID_EMPTY_OR_BLANK = "clientId should not be empty or blank";
    public static final String REQUESTING_SERVICE_EMPTY_OR_BLANK = "requestingService should not be empty or blank";
    public static final String SOLUTION_NAME_EMPTY_OR_BLANK = "solutionName should not be empty or blank";
    public static final String SOLUTION_PROCESS_ID_EMPTY_OR_BLANK = "solutionProcessId must not be null or blank";
    public static final String LAWFIRM_ID_MISSING_FOR_CASETYPE_RESOURCE = "lawFirmId should not be blank or empty for resourceType case";
    public static final String LAWFIRM_ID_SHOULD_NOT_BE_PUBLIC_FOR_CASETYPE = "lawFirmId should not be public lawFirmId for resourceType case/all";
    public static final String LAWFIRM_ID_SHOULD_NOT_BE_PUBLIC_FOR_RESOURCE_TYPE_CASE = "lawFirmId should not be public lawFirmId for resourceType case";
    public static final String INVALID_RESOURCE_TYPE ="Please provide a valid resource type (case/act/judgement/all)";
    public static final String STATUS_EMPTY = "status cannot be empty or blank";
    public static final String DATA_EMPTY = "data cannot be empty or blank";
    public static final String LAWFIRM_ID_EMPTY_OR_BLANK = "lawFirmId should not be empty or blank";
    public static final String FIELD_NAME_EMPTY_OR_BLANK = "field_name cannot be empty or blank";
    public static final String FIELD_NAME_NULL = "field_name or field_value cannot be null";
    public static final String RESOURCE_TYPE_INVALID = "Please provide a valid resourceType (case/act/judgement)";
    public static final String SHOULD_NOT_NULL_BLANK = " must not be null or blank";
    public static final String TEMPLATE_NOT_FOUND = "template not found";
    public static final String NO_REQUEST_FOUND = "No request found for ";

}
