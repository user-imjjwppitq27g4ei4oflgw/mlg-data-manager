package com.datafoundry.llpdatamanager.utils;

public enum RequestType {
    GET_DOCX_IDS,
    REQUEST_WITH_BODY
}