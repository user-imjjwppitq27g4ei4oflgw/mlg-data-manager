package com.datafoundry.llpdatamanager.entity;

import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import nonapi.io.github.classgraph.json.Id;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LrmRequest {

	private ObjectId id;

	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String clientId;

	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String lawFirmId;

	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String resourceType;

	private String courtType;

	private String state;

	private String district;

	private String court;

	private String caseNumber;

	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String caseUniqueId;

	private String year;

	private String caseType;

	@NotNull(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	@NotBlank(message = ErrorConstants.SHOULD_NOT_NULL_BLANK)
	private String callbackUrl;

	private String solutionProcessId;

	@ApiModelProperty(hidden = true)
	private String rrId;

	private String solutionName;
	
	private String requestingService;
	
	@Builder.Default
	private List<LRMDocument> documents = new ArrayList<>();

	@Builder.Default
	private String status = "IN_PROGRESS";
}
