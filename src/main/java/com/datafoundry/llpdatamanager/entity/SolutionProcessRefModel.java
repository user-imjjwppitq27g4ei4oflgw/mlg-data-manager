package com.datafoundry.llpdatamanager.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SolutionProcessRefModel implements Serializable {

    private static final long serialVersionUID = 65465461L;

    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    private String clientId;
    private String lawFirmId;
    private String requestingService;
    private String solutionName;
    private Map<String, Object> data;
    private String status;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String updatedBy;

    public SolutionProcessRefModel() {
    }

    public SolutionProcessRefModel(ObjectId id, String clientId, String requestingService, Map<String, Object> data, String status, String solutionName, LocalDateTime createdAt, String createdBy, String lawFirmId) {
        this.id = id;
        this.clientId = clientId;
        this.requestingService = requestingService;
        this.data = data;
        this.status = status;
        this.solutionName = solutionName;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.lawFirmId = lawFirmId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getLawFirmId() {
        return lawFirmId;
    }

    public void setLawFirmId(String lawFormId) {
        this.lawFirmId = lawFormId;
    }

    public String getRequestingService() {
        return requestingService;
    }

    public void setRequestingService(String requestingService) {
        this.requestingService = requestingService;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getSolutionName() {
        return solutionName;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }
}

