package com.datafoundry.llpdatamanager.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestLog implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String _id;

	private String solutionName;

	private String requestReferenceId ;

	private String solutionProcessId;

	private String requestingService;

	private Map<String,Object> reqBody;

	private String remarks;

	private String status;

	private LocalDateTime reqTime;
	
	
	public RequestLog( String solutionName, String requestReferenceId , String solutionProcessId,
			String requestingService, Map<String,Object> reqBody, String remarks,String status, LocalDateTime reqTime) {
		this.solutionName = solutionName;
		this.requestReferenceId  = requestReferenceId ;
		this.solutionProcessId = solutionProcessId;
		this.requestingService = requestingService;
		this.reqBody = reqBody;
		this.remarks = remarks;
		this.status = status;
		this.reqTime = reqTime;

	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_id() {
		return this._id;
	}

	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}

	public String getSolutionName() {
		return this.solutionName;
	}

	public void setRequestReferenceId (String requestReferenceId ) {
		this.requestReferenceId  = requestReferenceId ;
	}

	public String getRequestReferenceId () {
		return this.requestReferenceId ;
	}

	public void setSolutionProcessId(String solutionProcessId) {
		this.solutionProcessId = solutionProcessId;
	}

	public String getSolutionProcessId() {
		return this.solutionProcessId;
	}

	public void setRequestingService(String requestingService) {
		this.requestingService = requestingService;
	}

	public String getRequestingService() {
		return this.requestingService;
	}

	public void setReqBody(Map<String,Object> reqBody) {
		this.reqBody = reqBody;
	}

	public Map<String,Object> getReqBody() {
		return this.reqBody;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	public void setReqTime(LocalDateTime reqTime) {
		this.reqTime = reqTime;
	}

	public LocalDateTime getReqTime() {
		return this.reqTime;
	}
}
