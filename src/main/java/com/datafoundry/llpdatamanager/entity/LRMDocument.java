package com.datafoundry.llpdatamanager.entity;

//import com.datafoundry.llpdatamanager.models.CallbackModel.DocProgress;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.ObjectId;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LRMDocument {

//    @Id
	private ObjectId id;

	private String solutionProcessId;

	private String lawFirmId;

	private String clientId;

	/**
	 * extraction = false, just index the document.
	 */
	private Boolean extraction;

	/**
	 * folder path to update out files generated as part of the work flow.
	 */
	private String folderPath;

	private String url;

	private String documentType;

	private String version;

	private String documentId;

	private String fileName;

//	private String ocrDocumentId;

//	private String scgDocumentId;

//	private String serviceDocumentId;

	private String status; // docx status like "CREATED"

	private String state; // document state

	@Builder.Default
	private String docProgress = "LLP_DATA_MANAGER";

	private String workflowMessage;

	@Builder.Default
	private String RequestStatus = "IN_PROGRESS";

//    static public enum RequestStatus {
//
//        IN_PROGRESS, COMPLETED, ERROR
//    }

}