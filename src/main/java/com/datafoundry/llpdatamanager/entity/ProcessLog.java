package com.datafoundry.llpdatamanager.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String _id;

	private String requestReferenceId ;

	private String solutionProcessId;

	private String remarks;

	private String status;

	private LocalDateTime processTime;

	public ProcessLog(String requestReferenceId , String solutionProcessId, String remarks, String status, LocalDateTime processTime) {
		this.requestReferenceId = requestReferenceId ;
		this.solutionProcessId = solutionProcessId;
		this.remarks = remarks;
		this.status = status;
		this.processTime = processTime;
	}

	public String getRequestReferenceId () {
		return requestReferenceId ;
	}

	public void setRequestReferenceId (String requestReferenceId ) {
		this.requestReferenceId = requestReferenceId ;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getProcessTime() {
		return processTime;
	}

	public void setProcessTime(LocalDateTime processTime) {
		this.processTime = processTime;
	}

	public String getSolutionProcessId() {
		return solutionProcessId;
	}

	public void setSolutionProcessId(String solutionProcessId) {
		this.solutionProcessId = solutionProcessId;
	}
}
