package com.datafoundry.llpdatamanager.service;

import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface LLPDataManagerService {
    Status update(SolutionIdUpdate solutionIdUpdate) throws AppException;

    String create(SolutionIdCreate solutionIdCreate) throws AppException;

    List<SearchResponse> searchResources(SearchRequest searchRequest) throws AppException, IOException;
    ListSolutionProcessIdResponse getListOfSolutionRefId(ListSolutionProcessIdRequest listSolutionProcessIdRequest);
    String getDocumentSummary(String rrId, SummaryRequest summaryRequest) throws AppException;
    TemplateResponse getSummaryTemplate(String rrId, String lawFirmId, String documentType, String documentSubType, String resourceType) throws AppException;

}
