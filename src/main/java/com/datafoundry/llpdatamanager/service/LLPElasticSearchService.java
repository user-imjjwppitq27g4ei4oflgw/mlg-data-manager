package com.datafoundry.llpdatamanager.service;

import com.datafoundry.llpdatamanager.models.ESSearchResponse;
import com.datafoundry.llpdatamanager.models.SearchRequest;
import com.datafoundry.llpdatamanager.models.UploadRequestModel;

import java.util.List;

public interface LLPElasticSearchService {
    String getIndex(String resourceType, String lawFirmLd);
    List<ESSearchResponse> searchResource(SearchRequest searchRequest, String resourceType, String index);
	void saveDocumentsToElasticsearch(String rrId, UploadRequestModel uploadRequestModel, String SCGOutput);
}
