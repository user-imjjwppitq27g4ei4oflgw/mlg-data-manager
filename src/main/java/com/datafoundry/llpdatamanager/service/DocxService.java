package com.datafoundry.llpdatamanager.service;

import com.datafoundry.llpdatamanager.models.SignedUrlResponse;
import com.datafoundry.llpdatamanager.models.UploadRequestModel;

import java.util.List;

public interface DocxService {
    List<SignedUrlResponse> generateSignedURL(List<String> ids, String rrId, String clientID);
    String downloadDocumentsFromDocx(String rrId, UploadRequestModel uploadRequestModel);
}