package com.datafoundry.llpdatamanager.service;

import com.datafoundry.llpdatamanager.models.LoadNerDataRequest;
import com.datafoundry.llpdatamanager.models.UploadRequestModel;

public interface AsyncLLPDataManagerService {

	void createDocumentsInElasticSearch(String rrId, UploadRequestModel uploadRequestModel);
	void insertIntoNerCollection(LoadNerDataRequest loadNerDataRequest);
	void uploadDocuments(String rrId, UploadRequestModel uploadRequestModel);

}
