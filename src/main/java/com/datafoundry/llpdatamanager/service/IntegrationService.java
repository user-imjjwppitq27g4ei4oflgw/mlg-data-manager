package com.datafoundry.llpdatamanager.service;

import com.datafoundry.llpdatamanager.entity.ExtractedEntitiesModel;
import com.datafoundry.llpdatamanager.entity.LrmRequest;
import com.datafoundry.llpdatamanager.models.CallbackModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IntegrationService {

    String processLRMRequest(LrmRequest lrmRequest);

    ResponseEntity<Object> callback(CallbackModel model, String rrId);

    List<ExtractedEntitiesModel> getExtractedEntities(String rrId, String solutionProcessId, String lawFirmId);
}


