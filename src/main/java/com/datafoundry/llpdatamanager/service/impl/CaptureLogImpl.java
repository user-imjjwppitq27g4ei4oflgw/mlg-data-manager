package com.datafoundry.llpdatamanager.service.impl;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.entity.ProcessLog;
import com.datafoundry.llpdatamanager.entity.RequestLog;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.service.CaptureLogService;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

@Component
public class CaptureLogImpl implements CaptureLogService {
	RRAdapter logger = new RRAdapterImpl(CaptureLogImpl.class.getName());
	
	@Value("${llp.default.db}")
    private String defaultDB;

	@Autowired
	MongoRepository<RequestLog> mongoRepository;

	@Autowired
	MongoRepository<ProcessLog> mongoRepo;

	public void captureRequestLogs(String rrId, Object inputRequest, String remarks, String status) {
		try {
			logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Logging in Request Log", null);
			JSONObject jsonObj = new JSONObject(inputRequest);
			Map<String,Object> result = new ObjectMapper().readValue(jsonObj.toString(), HashMap.class);

			RequestLog requestLog = new RequestLog(result.getOrDefault(AppConstants.SOLUTION_NAME, "").toString(), rrId,
					result.getOrDefault(AppConstants.SOLUTION_PROCESS_ID, "").toString(), result.getOrDefault(AppConstants.REQUESTING_SERVICE, "").toString(),
					result, remarks, status, LocalDateTime.now(ZoneOffset.UTC));

			mongoRepository.save(requestLog, Utility.getClientId(result,defaultDB), AppConstants.REQUEST_LOG_COLLECTION,
					RequestLog.class);
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.LLP_DATAMANAGER, ex.getMessage(), null);
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
		}
	}

	public void captureProcessLogs(String rrId, Object inputRequestModel, String remarks, String status) {
		try {
			logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Logging in Process Log", null);
			JSONObject jsonObj = new JSONObject(inputRequestModel);
			ProcessLog object = new ProcessLog(rrId, jsonObj.getString(AppConstants.SOLUTION_PROCESS_ID), remarks, status, LocalDateTime.now(ZoneOffset.UTC));
			mongoRepo.save(object, jsonObj.getString(AppConstants.CLIENT_Id), AppConstants.PROCESS_LOG_COLLECTION,
					ProcessLog.class);
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
		}
	}

	public void updateStatusAndRemarks(String rrId, String status, String remarks,
			Object inputRequestModel) {
		JSONObject jsonObj = new JSONObject(inputRequestModel);
		try {
			logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Updating the remarks and status for a given referenceId",
					null);
			List<Bson> updateObject = new ArrayList<>();
			Bson whereQuery = Filters.eq(AppConstants.REQUEST_REFERENCE_ID , rrId);

			if (status != null && remarks != null) {
				updateObject.add(set(AppConstants.STATUS, status));
				updateObject.add(set(AppConstants.REMARKS, remarks));
			}
			updateObject.add(set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)));
			mongoRepository.updateOne(whereQuery, combine(updateObject), jsonObj.getString(AppConstants.CLIENT_Id),
					AppConstants.REQUEST_LOG_COLLECTION, RequestLog.class);
		} catch (Exception ex) {
			logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
		}
	}

}
