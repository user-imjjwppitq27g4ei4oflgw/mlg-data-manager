package com.datafoundry.llpdatamanager.service.impl;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.service.*;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.Placeholders;
import com.datafoundry.llpdatamanager.utils.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.model.Filters;
import org.apache.commons.text.StringSubstitutor;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

@Component
public class LLPDataManagerServiceImpl implements LLPDataManagerService {

    RRAdapter logger = new RRAdapterImpl(LLPDataManagerServiceImpl.class.getName());


    @Autowired
    MongoRepository<SolutionProcessRefModel> mongoRepository;

    @Autowired
    MongoRepository<ResourceMetaData> mongoRepositoryObj;

    @Autowired
    MongoRepository<TemplateResponse> mongoRepo;

    @Autowired
    LLPElasticSearchService llPElasticSearchService;

    @Autowired
    DocxService docxService;

    @Autowired
    CaptureLogService captureLog;

    @Autowired
    IntegrationService service;

    @Override
    public Status update(SolutionIdUpdate solutionIdUpdate) throws AppException {
        try {
            logger.info(solutionIdUpdate.getRrId(), AppConstants.LLP_DATAMANAGER, "Updating the data/status for a given solutionId", null);

            //update LrmDocuments collection, will update the status of that documentId and solutionProcessId
            // @formatter:off
                    CallbackModel model = CallbackModel.builder()
                                .lawFirmId(solutionIdUpdate.getLawFirmId())
                                .solutionProcessId(solutionIdUpdate.getSolutionProcessId())
                                .sourceDocumentId(solutionIdUpdate.getSourceDocumentId())
                                .workflowMessage(solutionIdUpdate.getStatus())
                                .docProgress(solutionIdUpdate.getRequestingService())
                                .serviceDocumentId(solutionIdUpdate.getServiceDocumentId())
                            .build();
            // @formatter:on
            service.callback(model, solutionIdUpdate.getRrId());

            List<Bson> updateObject = new ArrayList<>();
            Bson whereQuery = Filters.eq(AppConstants._ID, new ObjectId(solutionIdUpdate.getSolutionProcessId()));
            Long updatedCount = 0L;
            if (solutionIdUpdate.getData() != null) {
                if(!solutionIdUpdate.getData().isEmpty()){
                Set<String> data_keys = solutionIdUpdate.getData().keySet();
                for (String key : data_keys) {
                    updateObject.add(set(AppConstants.DATA + "." + key, solutionIdUpdate.getData().get(key)));
                }
                }
            }
            if (solutionIdUpdate.getStatus() != null) {
                if(!solutionIdUpdate.getStatus().isEmpty() & !solutionIdUpdate.getStatus().isBlank()) {
                    updateObject.add(set(AppConstants.STATUS, solutionIdUpdate.getStatus()));
                }
            }
            updateObject.add(set(AppConstants.UPDATED_AT, LocalDateTime.now(ZoneOffset.UTC)));
            updateObject.add(set(AppConstants.UPDATED_BY, solutionIdUpdate.getRequestingService()));
            var updatedResult = mongoRepository.updateOne(whereQuery, combine(updateObject), solutionIdUpdate.getClientId(),
                    AppConstants.SOLUTION_PROCESS_REF_COLLECTION, SolutionProcessRefModel.class);
            updatedCount = updatedCount + updatedResult;
            if (updatedCount == 0) {
                throw new AppException(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND, ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND);
            }

            return new Status.StatusBuilder(AppConstants.UPDATED_SUCCESSFULLY).isSuccess(true)
                    .withStatusCode(HttpStatus.OK.value()).build();

        } catch (AppException ae) {
            logger.error(solutionIdUpdate.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ae), null);
            throw new AppException(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND, ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND);
        } catch (Exception ex) {
            logger.error(solutionIdUpdate.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw new AppException(ErrorConstants.INTERNAL_SERVER_ERROR, ErrorConstants.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<SearchResponse> searchResources(SearchRequest searchRequest) throws AppException {
        logger.info(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, "Performing search in elastic server", null);
        String resourceType = searchRequest.getResourceType();
        String index = llPElasticSearchService.getIndex(resourceType, searchRequest.getLawFirmId());
        List<ESSearchResponse> resourceList = llPElasticSearchService.searchResource(searchRequest, resourceType, index);
        List<String> ids = resourceList.stream().map(i -> i.getSourceDocumentId()).distinct().collect(Collectors.toList());
        List<SignedUrlResponse> signedUrlResponseList = docxService.generateSignedURL(ids, searchRequest.getRrId(), searchRequest.getClientId());
        return generateUserResponse(resourceList, signedUrlResponseList);
    }

    @Override
    public String create(SolutionIdCreate solutionIdCreate) throws AppException {
        try {
            logger.info(solutionIdCreate.getRrId(), AppConstants.LLP_DATAMANAGER, "Creating solutionProcessId ", null);
            SolutionProcessRefModel toWrite = Utility.createSolutionProcessRefDBModel(solutionIdCreate);
            Boolean isInserted = mongoRepository.save(toWrite, toWrite.getClientId(), AppConstants.SOLUTION_PROCESS_REF_COLLECTION,
                    SolutionProcessRefModel.class);
            if (isInserted) {
                return toWrite.getId().toHexString();
            } else {
                throw new AppException(ErrorConstants.SOMETHING_WENT_WRONG, ErrorConstants.SOMETHING_WENT_WRONG);
            }
        } catch (AppException ae) {
            logger.error(solutionIdCreate.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ae), null);
            throw new AppException(ErrorConstants.SOMETHING_WENT_WRONG, ErrorConstants.SOMETHING_WENT_WRONG);
        } catch (Exception ex) {
            logger.error(solutionIdCreate.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw new AppException(ErrorConstants.INTERNAL_SERVER_ERROR, ErrorConstants.INTERNAL_SERVER_ERROR);
        }
    }


    private List<SearchResponse> generateUserResponse(List<ESSearchResponse> documentList, List<SignedUrlResponse> signedUrlResponseList) {
        Map<String, SignedUrlResponse> signedUrlMap = new HashMap<>(signedUrlResponseList.size());
        for (SignedUrlResponse signedUrlObj : signedUrlResponseList)
            signedUrlMap.put(signedUrlObj.getDocumentId(), signedUrlObj);

        List<SearchResponse> resourceList = new ArrayList<>();

        for (ESSearchResponse data : documentList) {
            Map<String, Object> highLight = data.getHighLight();

            SignedUrlResponse urlObj = signedUrlMap.get(data.getSourceDocumentId());
            FileMetaData fm = new FileMetaData(data.getSourceDocumentId(), urlObj != null ? urlObj.getUrl() : null, data.getSourceFileName());
            SearchResponse searchResponse = new SearchResponse(Utility.isStringBlank(data.getCaseType()), Utility.isStringBlank(data.getCaseNumber()), data.getResourceType(), data.getLawFirmId(),
                    Utility.isStringBlank(data.getJudgementNumber()), highLight, Utility.isStringBlank(data.getActNumber()), fm, Utility.isStringBlank(data.getDocumentType()), Utility.isStringBlank(data.getSolutionProcessId()));
            resourceList.add(searchResponse);
        }
        return resourceList;
    }


    @Override
    public ListSolutionProcessIdResponse getListOfSolutionRefId(ListSolutionProcessIdRequest listSolutionProcessIdRequest) {
        try {
            logger.info(listSolutionProcessIdRequest.getRrId(), AppConstants.LLP_DATAMANAGER, "Get the list of solutionRefId and its status", null);
            Bson filter = new Document();
            ListSolutionProcessIdResponse finalResult = new ListSolutionProcessIdResponse();
            if (!Utility.isStringEmpty(listSolutionProcessIdRequest.getSolutionProcessId()))
                filter = Filters.and(Filters.eq(AppConstants._ID, new ObjectId(listSolutionProcessIdRequest.getSolutionProcessId())),
                        Filters.eq(AppConstants.LAW_FIRM_ID, listSolutionProcessIdRequest.getLawFirmId()),
                        Filters.ne(AppConstants.STATUS, null), Filters.ne(AppConstants.STATUS, ""));
            else
                filter = Filters.and(Filters.eq(AppConstants.LAW_FIRM_ID, listSolutionProcessIdRequest.getLawFirmId()),
                        Filters.ne(AppConstants.STATUS, null), Filters.ne(AppConstants.STATUS, ""));

            Utility.getPageNumber(listSolutionProcessIdRequest);
            Utility.getPageSize(listSolutionProcessIdRequest);
            Integer skip = Utility.findOffSet(listSolutionProcessIdRequest);
            Long totalCount = mongoRepository.getCount(filter, listSolutionProcessIdRequest.getClientId(), AppConstants.SOLUTION_PROCESS_REF_COLLECTION, SolutionProcessRefModel.class);
            if (totalCount > 0) {
                List<SolutionProcessRefModel> result = mongoRepository.findAll(filter, skip, listSolutionProcessIdRequest.getPageSize(), listSolutionProcessIdRequest.getClientId(), AppConstants.SOLUTION_PROCESS_REF_COLLECTION, SolutionProcessRefModel.class);
                if (result.size() < 1)
                    throw new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND);
                else {
                    List<HashMap<String, String>> listOfSol = result.stream().map(refModel ->
                            new HashMap<String, String>() {{
                                put(AppConstants.SOLUTION_PROCESS_ID, refModel.getId().toString());
                                put(AppConstants.STATUS, refModel.getStatus());
                            }}

                    ).collect(Collectors.toList());
                    finalResult.setListOfSolutionProcessId(listOfSol);
                    finalResult.setTotalRecords(totalCount.toString());
                    finalResult.setLawFirmId(listSolutionProcessIdRequest.getLawFirmId());
                }
            }else
                throw new AppException(ErrorConstants.NOT_FOUND, ErrorConstants.NO_DATA_FOUND);

            return finalResult;
        } catch (Exception ex) {
            if (ex.getMessage().contains("invalid hexadecimal representation of an ObjectId")) {
                throw new AppException(ErrorConstants.NOT_FOUND, ErrorConstants.NO_DATA_FOUND);
            }
            logger.error(listSolutionProcessIdRequest.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw ex;
        }
    }

    @Override
    public String getDocumentSummary(String rrId, SummaryRequest summaryRequest) {
        try {
            logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Summary for the provided document type", null);
            // get the template
            TemplateResponse saleDeedTemplate = getSummaryTemplate(rrId, summaryRequest.getClientId(), summaryRequest.getDocumentType(), summaryRequest.getDocumentSubType(), summaryRequest.getResourceType());
            if (saleDeedTemplate != null) {
                Bson whereFilter = Filters.eq(AppConstants.SOLUTION_PROCESS_REF_ID, summaryRequest.getSolutionProcessId());
                // get the extracted NER output
                ResourceMetaData result = mongoRepositoryObj.findOne(whereFilter, summaryRequest.getClientId(), AppConstants.EXTRACTED_ENTITY_COLLECTION,
                        ResourceMetaData.class);
                if (result != null) {
                    Map<String, Object> extrectedEntities = validateEntities(rrId, summaryRequest.getDocumentType(), result);
                    //replace the placeholders with the entities
                    StringSubstitutor sub = new StringSubstitutor(extrectedEntities);
                    return sub.replace(saleDeedTemplate.getTemplateFormat());
                } else
                    throw new AppException(ErrorConstants.NOT_FOUND, String.format("No data found for solutionProcessId %s", summaryRequest.getSolutionProcessId()));
            } else
                throw new AppException(ErrorConstants.NOT_FOUND, ErrorConstants.TEMPLATE_NOT_FOUND);
        } catch (AppException ae) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ae), null);
            throw new AppException(ErrorConstants.NOT_FOUND, ae.getErrorMessage());
        } catch (Exception ex) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
        }
    }

    @Override
    public TemplateResponse getSummaryTemplate(String rrId, String clientId, String documentType, String documentSubType, String resourceType) {
        try {
            logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Get the template for the provided document type and document sub type", null);
            Bson whereFilter = Filters.and(Filters.eq(AppConstants.DOCUMENT_TYPE, documentType),Filters.eq(AppConstants.RESOURCE_TYPE, resourceType),
                    Filters.eq(AppConstants.DOCUMENT_SUB_TYPE, documentSubType));
            return mongoRepo.findOne(whereFilter, clientId, AppConstants.TEMPLATE_COLLECTION,
                    TemplateResponse.class);
        } catch (Exception ex) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
        }
    }

    private Map<String, Object> validateEntities(String rrId, String documentType, Object inputRequest) {
        try {
            logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Validating extracted entities from NER ", null);

            Map<String, Object> finalData = new HashMap<>();
            JSONObject jsonObj = new JSONObject(inputRequest);
            Map<String, Object> results = new ObjectMapper().readValue(jsonObj.get("entities").toString(), HashMap.class);

            if (documentType.equals(AppConstants.SALEDEED)) {
                finalData.put(Placeholders.DOCUMENT_NO, Utility.getValueFromMap(results, Placeholders.DOCUMENT_NO));
                finalData.put(Placeholders.CONSIDERATION_AMOUNT,
                        Utility.getValueFromMap(results, Placeholders.CONSIDERATION_AMOUNT));
                finalData.put(Placeholders.DISTRICT, Utility.getValueFromMap(results, Placeholders.DISTRICT));
                finalData.put(Placeholders.VILLAGE, Utility.getValueFromMap(results, Placeholders.VILLAGE));
                finalData.put(Placeholders.FLAT_NUMBER, Utility.getValueFromMap(results, Placeholders.FLAT_NUMBER));
                finalData.put(Placeholders.MANDAL, Utility.getValueFromMap(results, Placeholders.MANDAL));
                finalData.put(Placeholders.NAME_OF_APARTMENT,
                        Utility.getValueFromMap(results, Placeholders.NAME_OF_APARTMENT));
                finalData.put(Placeholders.NAME_OF_AREA,
                        Utility.getValueFromMap(results, Placeholders.NAME_OF_AREA));
                finalData.put(Placeholders.PLOT_NUMBER, Utility.getValueFromMap(results, Placeholders.PLOT_NUMBER));
                finalData.put(Placeholders.PURCHASER, Utility.getValueFromMap(results, Placeholders.PURCHASER));
                finalData.put(Placeholders.SELLER, Utility.getValueFromMap(results, Placeholders.SELLER));
                finalData.put(Placeholders.YEAR, Utility.getValueFromMap(results, Placeholders.YEAR));
                finalData.put(Placeholders.AREA_SY_NUM, Utility.getValueFromMap(results, Placeholders.AREA_SY_NUM).toString().replaceAll("\\[|\\]", ""));
                finalData.put(Placeholders.DATE_OF_DOCUMENT,
                        Utility.getValueFromMap(results, Placeholders.DATE_OF_DOCUMENT).toString().replaceAll("\\[|\\]", ""));
            }
            return finalData;
        } catch (Exception ex) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
        }
    }
}


