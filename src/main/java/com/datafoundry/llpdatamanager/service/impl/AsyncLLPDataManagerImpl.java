package com.datafoundry.llpdatamanager.service.impl;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.client.ElasticSearchClient;
import com.datafoundry.llpdatamanager.entity.ExtractedEntitiesModel;
import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.service.*;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
public class AsyncLLPDataManagerImpl implements AsyncLLPDataManagerService {
    RRAdapter logger = new RRAdapterImpl(AsyncLLPDataManagerImpl.class.getName());

    @Autowired
    DocxService docxService;

    @Autowired
    LLPElasticSearchService esService;

    @Autowired
    MongoRepository<ExtractedEntitiesModel> mongoRepository;

    @Autowired
    MongoRepository<SolutionProcessRefModel> mongoRepositorySolutionIdModel;

    @Autowired
    CaptureLogService captureLog;

    @Autowired
    LLPDataManagerService llpDataManagerService;

    @Autowired
    ElasticSearchClient elasticSearchClient;


    @Override
    public void createDocumentsInElasticSearch(String rrId, UploadRequestModel uploadRequestModel) {
        try {
            logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Asynchronously indexing documents into Elasticsearch",
                    null);
            String downloadedSCGOutput = docxService.downloadDocumentsFromDocx(rrId, uploadRequestModel);
            if (downloadedSCGOutput != null) {
                esService.saveDocumentsToElasticsearch(rrId, uploadRequestModel, downloadedSCGOutput);
            }
        } catch (Exception ex) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
        }
    }

    @Override
    @Async
    public void insertIntoNerCollection(LoadNerDataRequest loadNerDataRequest) {
        try {
            logger.info(loadNerDataRequest.getRrId(), AppConstants.LLP_DATAMANAGER, "Asynchronously inserting ner entities into LLPDM ", null);
            captureLog.captureProcessLogs(loadNerDataRequest.getRrId(), loadNerDataRequest, AppConstants.INSERTING_NER_ENTITIES, AppConstants.SUCCESS);
            ExtractedEntitiesModel extractedEntitiesModel = new ExtractedEntitiesModel();
            extractedEntitiesModel.setClientId(loadNerDataRequest.getClientId());
            extractedEntitiesModel.setLawFirmId(loadNerDataRequest.getLawFirmId());
            extractedEntitiesModel.setSolutionName(loadNerDataRequest.getSolutionName());
            extractedEntitiesModel.setRequestingService(loadNerDataRequest.getRequestingService());
            extractedEntitiesModel.setSolutionProcessId(loadNerDataRequest.getSolutionProcessId());
            extractedEntitiesModel.setFileName(loadNerDataRequest.getFileName());
            extractedEntitiesModel.setDocumentId(loadNerDataRequest.getDocumentId());
            extractedEntitiesModel.setSourceDocumentId(loadNerDataRequest.getSourceDocumentId());
            extractedEntitiesModel.setSourceFileName(loadNerDataRequest.getSourceFileName());
            ArrayList<NerData> objectArrayList = loadNerDataRequest.getNerData();
            Map<String, Object> data = new HashMap<>();
                for (int i = 0; i <= objectArrayList.size() - 1; i++) {
                    data.put((objectArrayList.get(i)).getField_name(), (objectArrayList.get(i)).getField_value());
                }
                extractedEntitiesModel.setEntities(data);
                extractedEntitiesModel.setCreatedAt(LocalDateTime.now(ZoneOffset.UTC));
                extractedEntitiesModel.setCreatedBy(loadNerDataRequest.getRequestingService());
                mongoRepository.save(extractedEntitiesModel, loadNerDataRequest.getLawFirmId(), AppConstants.EXTRACTED_ENTITY_COLLECTION, ExtractedEntitiesModel.class);
                captureLog.captureProcessLogs(loadNerDataRequest.getRrId(), loadNerDataRequest, AppConstants.INSERTED_NER_ENTITIES, AppConstants.SUCCESS);
                captureLog.updateStatusAndRemarks(loadNerDataRequest.getRrId(), AppConstants.SUCCESS,
                        AppConstants.INSERTED_NER_ENTITIES , loadNerDataRequest);
                //call callback api's method

            //UpdateSolutionProcessId here
            SolutionIdUpdate solutionIdUpdate = new SolutionIdUpdate();
            solutionIdUpdate.setLawFirmId(loadNerDataRequest.getLawFirmId());
            solutionIdUpdate.setClientId(loadNerDataRequest.getClientId());
            solutionIdUpdate.setSolutionName(loadNerDataRequest.getSolutionName());
            solutionIdUpdate.setRequestingService("llpDmLoadEntities");
            solutionIdUpdate.setSolutionProcessId(loadNerDataRequest.getSolutionProcessId());
            solutionIdUpdate.setSourceDocumentId(loadNerDataRequest.getSourceDocumentId());
            solutionIdUpdate.setStatus(AppConstants.NER_PROCESS_COMPLETED);
            Utility.validateUpdateSolutionIdModel(solutionIdUpdate);
            llpDataManagerService.update(solutionIdUpdate);
            captureLog.captureProcessLogs(loadNerDataRequest.getRrId(), loadNerDataRequest,  AppConstants.UPDATED_SOLUTION_ID_SUCCESS, AppConstants.SUCCESS);
        }
        catch (AppException ae) {
            captureLog.captureProcessLogs(loadNerDataRequest.getRrId(), loadNerDataRequest, ae.getMessage(),
                    ErrorConstants.FAILURE);
            if (ae.getMessage() == ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND) {
                captureLog.updateStatusAndRemarks(loadNerDataRequest.getRrId(), AppConstants.SUCCESS, AppConstants.NER_PROCESS_SUCCESS_AND_UPDATE_FAILED,
                        loadNerDataRequest);
            } else {
                captureLog.updateStatusAndRemarks(loadNerDataRequest.getRrId(), ErrorConstants.FAILURE, ae.getMessage(),
                        loadNerDataRequest);
            }
            logger.error(loadNerDataRequest.getRrId(), AppConstants.LLP_DATAMANAGER, ae.getMessage(), null);
        }
        catch (Exception ex) {
            captureLog.captureProcessLogs(loadNerDataRequest.getRrId(), loadNerDataRequest, ex.getMessage(),
                    ErrorConstants.FAILURE);
            captureLog.updateStatusAndRemarks(loadNerDataRequest.getRrId(), ErrorConstants.FAILURE, ex.getMessage(),
                    loadNerDataRequest);
            logger.error(loadNerDataRequest.getRrId(), AppConstants.LLP_DATAMANAGER, ex.getMessage(), null);
        }
    }

    /*
     * If index is available then insert in ElasticSearch or else create the index
     * then insert
     */
    @Override
    @Async
    public void uploadDocuments(String rrId, UploadRequestModel uploadRequestModel) {
        try {
            logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Upload Documents in ElasticSearch", null);
            Boolean isIndexAvailable = elasticSearchClient.isIndexAvailable(rrId, uploadRequestModel.getLawFirmId());
            captureLog.captureProcessLogs(rrId, uploadRequestModel, AppConstants.IS_INDEX_AVAILABLE + isIndexAvailable,
                    AppConstants.SUCCESS);
            if (!isIndexAvailable) {
                Boolean isIndexCreated = elasticSearchClient.createIndex(rrId, uploadRequestModel);
                captureLog.captureProcessLogs(rrId, uploadRequestModel, AppConstants.IS_INDEX_CREATED + isIndexCreated,
                        AppConstants.SUCCESS);
            }
            createDocumentsInElasticSearch(rrId, uploadRequestModel);
        } catch (AppException ae) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ae), null);
            captureLog.captureProcessLogs(rrId, uploadRequestModel, ae.getErrorMessage(),
                    ErrorConstants.FAILURE);
            captureLog.updateStatusAndRemarks(rrId, ErrorConstants.FAILURE, ae.getErrorMessage(),
                    uploadRequestModel);
        }
    }
}
