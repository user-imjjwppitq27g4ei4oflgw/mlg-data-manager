package com.datafoundry.llpdatamanager.service.impl;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.client.ElasticSearchClient;
import com.datafoundry.llpdatamanager.entity.SolutionProcessRefModel;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.service.CaptureLogService;
import com.datafoundry.llpdatamanager.service.LLPDataManagerService;
import com.datafoundry.llpdatamanager.service.LLPElasticSearchService;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for Elastic Search Service
 */
@Component
public class LLPElasticSearchServiceImpl implements LLPElasticSearchService {
    RRAdapter logger = new RRAdapterImpl(LLPElasticSearchServiceImpl.class.getName());
    
    @Autowired
    ElasticSearchClient elasticSearchClient;
    
    @Autowired
	CaptureLogService captureLog;

    @Autowired
    MongoRepository<SolutionProcessRefModel> mongoRepositorySolutionIdModel;

    @Autowired
    LLPDataManagerService llpDataManagerService;


    public List<ESSearchResponse> searchResource(SearchRequest searchRequest, String resourceType, String index) {
        try {
            return elasticSearchClient.searchResource(searchRequest, resourceType, index);
        } catch (AppException appEx) {
            logger.error(searchRequest.getRrId(), AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(appEx), null);
            throw new AppException(ErrorConstants.NO_DATA, ErrorConstants.NO_DATA_FOUND);
        }
    }

    @Override
    public String getIndex(String resourceType, String lawFirmLd) {
        if (AppConstants.Resource_Type_CASE.equalsIgnoreCase(resourceType)  || AppConstants.Resource_Type_ALL.equalsIgnoreCase(resourceType)) {
            if (Utility.isStringEmpty(lawFirmLd))
                return null;
            else return lawFirmLd;
        } else return AppConstants.PUBLIC_INDEX;
    }
   
    @Override
	public void saveDocumentsToElasticsearch(String rrId, UploadRequestModel uploadRequestModel, String SCGOutput) {
		try {
			Boolean isInserterted = elasticSearchClient.insertInElasticSearch(rrId, uploadRequestModel, SCGOutput);
			captureLog.captureProcessLogs(rrId, uploadRequestModel, AppConstants.ARE_DOCUMENTS_INDEXED + isInserterted,
					AppConstants.SUCCESS);
			captureLog.updateStatusAndRemarks(rrId, AppConstants.SUCCESS,
					AppConstants.IS_REQUEST_COMPLETED + isInserterted, uploadRequestModel);
            if (isInserterted) {
                try {
                    SolutionIdUpdate solutionIdUpdate = new SolutionIdUpdate();
                    solutionIdUpdate.setLawFirmId(uploadRequestModel.getLawFirmId());
                    solutionIdUpdate.setClientId(uploadRequestModel.getClientId());
                    solutionIdUpdate.setSolutionName(uploadRequestModel.getSolutionName());
                    solutionIdUpdate.setSourceDocumentId(uploadRequestModel.getFileData().getSourceDocumentId()); //
                    solutionIdUpdate.setServiceDocumentId(uploadRequestModel.getFileData().getDocumentId()); //
                    solutionIdUpdate.setRequestingService(uploadRequestModel.getRequestingService());
                    solutionIdUpdate.setSolutionProcessId(uploadRequestModel.getSolutionProcessId());
                    solutionIdUpdate.setStatus(AppConstants.SCG_PROCESS_COMPLETED);
                    Utility.validateUpdateSolutionIdModel(solutionIdUpdate);
                    Status updateStatus = llpDataManagerService.update(solutionIdUpdate);

                    if (updateStatus.getStatusCode() == 200) {
                        captureLog.captureProcessLogs(rrId, uploadRequestModel, AppConstants.UPDATED_SOLUTION_ID_SUCCESS,
                                AppConstants.SUCCESS);
                    } else {
                        captureLog.captureProcessLogs(rrId, uploadRequestModel, ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND,
                                ErrorConstants.FAILURE);
                        captureLog.updateStatusAndRemarks(rrId, AppConstants.SUCCESS,
                                AppConstants.UPDATED_SOLUTION_ID_FAILED, uploadRequestModel);
                    }
                } catch (AppException ex) {
                    captureLog.captureProcessLogs(rrId, uploadRequestModel, ex.getErrorMessage(),
                            ErrorConstants.FAILURE);
                    captureLog.updateStatusAndRemarks(rrId, AppConstants.SUCCESS,
                            AppConstants.UPDATED_SOLUTION_ID_FAILED, uploadRequestModel);
                }
            }
        } catch (Exception ex) {
            captureLog.captureProcessLogs(rrId, uploadRequestModel, ex.getMessage(),
                    ErrorConstants.FAILURE);
            captureLog.updateStatusAndRemarks(rrId, ErrorConstants.FAILURE, ex.getMessage(),
                    uploadRequestModel);
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
        }
    }
}


