package com.datafoundry.llpdatamanager.service.impl;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.SignedUrlRequest;
import com.datafoundry.llpdatamanager.models.SignedUrlResponse;
import com.datafoundry.llpdatamanager.models.UploadRequestModel;
import com.datafoundry.llpdatamanager.service.CaptureLogService;
import com.datafoundry.llpdatamanager.service.DocxService;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.RequestType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Dalee b
 * @version number Llpdatamanager v1.0
 * @created on 18/03/2021
 * @description Class for DocxService
 */
@Service
public class DocxServiceImpl implements DocxService {
    RRAdapter logger = new RRAdapterImpl(DocxServiceImpl.class.getName());

	@Autowired
    RestService restService;
    
    @Autowired
	CaptureLogService captureLog;
    
    @Value("${docx.url}")
    private String docxUrl;

    public void setDocxUrl(String docxUrl) {
        this.docxUrl = docxUrl;
    }

    public List<SignedUrlResponse> generateSignedURL(List<String> ids, String rrId, String clientID) {
        logger.info(rrId, AppConstants.LLP_DATAMANAGER, "calling docx for generating signed Url", null);
        StringBuilder signedUrl = new StringBuilder(docxUrl).append(AppConstants.GENERATE_SIGNED_URL_PATH).append("?" + AppConstants.CLIENT_Id + "=" + clientID).append("&" + AppConstants.USER_Id + "=" + AppConstants.USER_ID_VALUE);
        Map<String, List<SignedUrlRequest>> requetBody = generateSignedUrlRequest(ids);
        ResponseEntity<SignedUrlResponse[]> response =
                restService.getResponse(rrId, signedUrl.toString(), RequestType.REQUEST_WITH_BODY, requetBody, null, SignedUrlResponse[].class);
        logger.info(rrId, AppConstants.LLP_DATAMANAGER, "generating signed Url process completed successfully", null);
        return Arrays.asList(response.getBody());
    }

    private Map<String, List<SignedUrlRequest>> generateSignedUrlRequest(List<String> ids) {
        List<SignedUrlRequest> signedUrlRequestList = ids.stream().map(id -> new SignedUrlRequest(id, AppConstants.TTL_VALUE)).collect(Collectors.toList());
        Map<String, List<SignedUrlRequest>> requetBody = new HashMap<>();
        requetBody.put(AppConstants.DOCUMENT_IdS, signedUrlRequestList);
        return requetBody;
    }
    
	private String generateUrl(String url, UploadRequestModel uploadRequestModel) {
		return String.format("%s/document/internal/download?id=%s&clientId=%s&requestingService=%s", url,
				uploadRequestModel.getFileData().getDocumentId(), uploadRequestModel.getClientId(),
				uploadRequestModel.getRequestingService());
	}

	@Override
	public String downloadDocumentsFromDocx(String rrId, UploadRequestModel uploadRequestModel) {
		String docxResponse = null;
		try {
			logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Downloading Documents from Docx using docId", null);

			String url = generateUrl(docxUrl, uploadRequestModel);
			ResponseEntity<String> response = restService.getResponse(rrId, url, RequestType.GET_DOCX_IDS, null, null,
					String.class);
			docxResponse = response.getBody().toString();
			boolean isDocDownloaded = docxResponse != null;
			captureLog.captureProcessLogs(rrId, uploadRequestModel,
					AppConstants.ARE_DOCUMENTS_DOWNLOADING_FROM_DOCX + isDocDownloaded, AppConstants.SUCCESS);
		} catch (AppException ex) {
			captureLog.captureProcessLogs(rrId, uploadRequestModel, ex.getErrorMessage(),
					ErrorConstants.FAILURE);
			captureLog.updateStatusAndRemarks(rrId, ErrorConstants.FAILURE,  ex.getErrorMessage(),
					uploadRequestModel);
			logger.error(rrId, AppConstants.LLP_DATAMANAGER, ex.getErrorMessage(), null);
		}
		return docxResponse;
	}
}
