package com.datafoundry.llpdatamanager.service.impl;

import com.datafoundry.llpdatamanager.entity.ExtractedEntitiesModel;
import com.datafoundry.llpdatamanager.entity.LRMDocument;
import com.datafoundry.llpdatamanager.entity.LrmRequest;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.models.*;
import com.datafoundry.llpdatamanager.repositories.MongoRepository;
import com.datafoundry.llpdatamanager.service.IntegrationService;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.RequestType;
import com.datafoundry.llpdatamanager.utils.Utility;
import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

@Service
public class IntegrationServiceImpl implements IntegrationService {

    @Autowired
    MongoRepository<LrmRequest> lrmRequestRepository;

    @Autowired
    MongoRepository<LRMDocument> documentRepository;

    @Autowired
    RestService restService;

    @Autowired
    MongoRepository<ExtractedEntitiesModel> mongoRepoEntities;

    @Value("${llp.dpa.url}")
    private String llpDpaEndpoint;

    @Override
    public String processLRMRequest(LrmRequest lrmRequest) {
        try {
            Utility.logInfo(lrmRequest.getRrId(), "Request for extracting the entities from LRM");

            String solutionProcessId = Utility.getSolutionProcessId();
            lrmRequest.setSolutionProcessId(solutionProcessId);

            // insert the request body to db;
            lrmRequestRepository.save(lrmRequest, lrmRequest.getLawFirmId(), AppConstants.LRM_REQUEST,
                    LrmRequest.class);
            List<LRMDocument> documents = lrmRequest.getDocuments();

            // save all the documents in the database.
            documents.forEach(document -> {
                document.setSolutionProcessId(lrmRequest.getSolutionProcessId());
                document.setLawFirmId(lrmRequest.getLawFirmId());
                document.setClientId(lrmRequest.getClientId());
                documentRepository.save(document, lrmRequest.getLawFirmId(), AppConstants.LRM_DOCUMENTS,
                        LRMDocument.class);
                // start work flow process.
                callDpaService(lrmRequest, document, lrmRequest.getRrId());
            });

            return solutionProcessId;
        } catch (Exception ex) {
            Utility.logError(lrmRequest.getRrId(), ex);
            throw new AppException(ErrorConstants.INTERNAL_SERVER_ERROR, ErrorConstants.INTERNAL_SERVER_ERROR);
        }
    }

    @Async
    private void callDpaService(LrmRequest lrmRequest, LRMDocument document, String rrId) {

        try {

            // @formatter:off
            OCRDocumentsData data = OCRDocumentsData.builder()
                    .internalUrl(document.getUrl())
                    .caseNumber(lrmRequest.getCaseNumber())
                    .caseType(lrmRequest.getCaseType())
                    .resourceType(lrmRequest.getResourceType())
                    .sourceDocumentId(document.getDocumentId())
                    .sourceFileName(document.getFileName())
                    .documentType(document.getDocumentType())
                    .documentId(document.getDocumentId())
                    .fileName(document.getFileName())
                    .version(document.getVersion())
                    .status(document.getStatus())
                    .folderPath(String.format("%s/%s",document.getFolderPath(),document.getSolutionProcessId()))
                    .extraction(document.getExtraction())
                    .build();

            OCRRequestBody ocrRequestBody = OCRRequestBody.builder()
                    .clientId(lrmRequest.getClientId())
                    .lawFirmId(lrmRequest.getLawFirmId())
                    .solutionName(lrmRequest.getSolutionName())
                    .requestingService(lrmRequest.getRequestingService())
                    .solutionProcessId(lrmRequest.getSolutionProcessId())
                    .requestType("async")
                    .documentsData(Arrays.asList(data))
                    .build();

            // @formatter:on

            // triggering the workflow.
            restService.getResponse(rrId, llpDpaEndpoint, RequestType.REQUEST_WITH_BODY, ocrRequestBody, null,
                    String.class);
        } catch (Exception ex) {
            Utility.logError(lrmRequest.getRrId(), ex);
        }
    }

    @Override
    public ResponseEntity<Object> callback(CallbackModel model, String rrId) {
        try {
            Utility.logInfo(rrId, "Call back status update in side the work flow.");

            // Exists check on SOLUTION_PROCESS_ID.
            Bson whereFilter1 = Filters.eq(AppConstants.SOLUTION_PROCESS_ID, model.getSolutionProcessId());
            LrmRequest lrmRequest = lrmRequestRepository.findOne(whereFilter1, model.getLawFirmId(),
                    AppConstants.LRM_REQUEST, LrmRequest.class);
            if (lrmRequest != null) {
                Bson filterLrmRequest = Filters.and(
                        Filters.eq(AppConstants.SOLUTION_PROCESS_ID, lrmRequest.getSolutionProcessId()),
                        Filters.eq(AppConstants.DOCUMENT_ID, model.getSourceDocumentId()));
                LRMDocument doc = documentRepository.findOne(filterLrmRequest, model.getLawFirmId(),
                        AppConstants.LRM_DOCUMENTS, LRMDocument.class);
                if (doc != null) {
                    doc.setWorkflowMessage(model.getWorkflowMessage());
                    doc.setDocProgress(model.getDocProgress());
                    if (AppConstants.LLPDM_LOAD_ENTITIES.equalsIgnoreCase(model.getDocProgress()))
                        doc.setState(AppConstants.COMPLETED);
                    else
                        doc.setState(AppConstants.IN_PROGRESS);

                    List<Bson> updateObject = new ArrayList<>();
                    updateObject.add(set(AppConstants.DOC_PROGRESS, doc.getDocProgress()));
                    updateObject.add(set(AppConstants.STATE, doc.getState()));
                    updateObject.add(set(AppConstants.WORKFLOW_MESSAGE, doc.getWorkflowMessage()));
                    updateObject.add(set(model.getDocProgress() + "DocumentId", model.getServiceDocumentId()));

                    documentRepository.updateOne(filterLrmRequest, combine(updateObject), lrmRequest.getLawFirmId(),
                            AppConstants.LRM_DOCUMENTS, LRMDocument.class);

                    boolean requestStatus = false;
                    Bson whereFilter3 = Filters.and(
                            Filters.eq(AppConstants.SOLUTION_PROCESS_ID, lrmRequest.getSolutionProcessId()),
                            Filters.ne(AppConstants.STATE, AppConstants.COMPLETED));
                    requestStatus = documentRepository.findAllNoLimit(whereFilter3, lrmRequest.getLawFirmId(),
                            AppConstants.LRM_DOCUMENTS, LRMDocument.class).size() > 0 ? false : true;
                    String requestMessage = AppConstants.IN_PROGRESS;

                    if (requestStatus) {
                        requestMessage = AppConstants.COMPLETED;
                        Bson filterForDocumentStatus = Filters.and(
                                Filters.eq(AppConstants.SOLUTION_PROCESS_ID, lrmRequest.getSolutionProcessId()));

                        //update LRM document's requestStatus
                        documentRepository.update(filterForDocumentStatus, combine(set(AppConstants.REQUEST_STATUS, requestMessage)), lrmRequest.getLawFirmId(),
                                AppConstants.LRM_DOCUMENTS, LRMDocument.class);

                        //update LRM request's status
                        lrmRequestRepository.updateOne(whereFilter1, combine(set(AppConstants.STATUS, requestMessage)), model.getLawFirmId(),
                                AppConstants.LRM_REQUEST, LrmRequest.class);
                    }
                    // Call back to LRM
                    // @formatter:off
                    CallbackLrmModel callbackLrmModel = new CallbackLrmModel().builder()
                            .caseUniqueId(lrmRequest.getCaseUniqueId())
                            .documentId(model.getSourceDocumentId())
//                            .ocrDocumentId(serviceId)
//                            .scgDocumentId(serviceId)
                            .serviceDocumentId(model.getServiceDocumentId())
                            .message(doc.getFileName() + " completed " + doc.getDocProgress())
                            .status(doc.getState())
                            .indexed(doc.getExtraction())
                            .requestStatus(requestMessage)
                            .build();
                    // @formatter:on
                    restService.getResponse(rrId, lrmRequest.getCallbackUrl(), RequestType.REQUEST_WITH_BODY, callbackLrmModel, null,
                            Status.class);
                } else {
                    Utility.logWarn(rrId, ErrorConstants.NOT_FOUND);
                    return new ResponseEntity<>(ErrorConstants.NOT_FOUND, HttpStatus.CONFLICT);
                }
                return ResponseEntity.ok(AppConstants.DATA_RECEIVED);
            } else {
                Utility.logWarn(rrId, ErrorConstants.NO_REQUEST_FOUND + lrmRequest.getSolutionProcessId());
                return new ResponseEntity<>(ErrorConstants.NOT_FOUND, HttpStatus.CONFLICT);
            }

        } catch (Exception ex) {
            Utility.logError(rrId, ex);
            throw new AppException(ErrorConstants.SOMETHING_WENT_WRONG, ErrorConstants.SOMETHING_WENT_WRONG);
        }
    }

    @Override
    public List<ExtractedEntitiesModel> getExtractedEntities(String rrId, String solutionProcessId, String lawFirmId) {
        try {
            Utility.logInfo(rrId, "Get extracted entities for the given solutionProcessId");
            Bson whereFilter = Filters.eq(AppConstants.SOLUTION_PROCESS_ID, solutionProcessId);
            List<ExtractedEntitiesModel> response = mongoRepoEntities.findAllNoLimitWithProjection(whereFilter,
                    lawFirmId, AppConstants.EXTRACTED_ENTITY_COLLECTION, ExtractedEntitiesModel.class);
            if (response.isEmpty()) {
                throw new AppException(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND,
                        ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND);
            }
            return response;
        } catch (AppException ae) {
            Utility.logError(rrId, ae);
            throw new AppException(ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND,
                    ErrorConstants.SOLUTION_PROCESS_REF_ID_NOT_FOUND);
        } catch (Exception ex) {
            Utility.logError(rrId, ex);
            throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
        }
    }
}