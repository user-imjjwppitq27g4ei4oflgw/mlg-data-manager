package com.datafoundry.llpdatamanager.service.impl;

import com.datafoundry.RRAdapter;
import com.datafoundry.RRAdapterImpl;
import com.datafoundry.llpdatamanager.exception.AppException;
import com.datafoundry.llpdatamanager.utils.AppConstants;
import com.datafoundry.llpdatamanager.utils.ErrorConstants;
import com.datafoundry.llpdatamanager.utils.RequestType;
import com.datafoundry.llpdatamanager.utils.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Component
public class RestService {

    RRAdapter logger = new RRAdapterImpl(LLPDataManagerServiceImpl.class.getName());

    @Autowired
    ObjectMapper mapper;

    @Autowired
    RestTemplate restTemplate;

    public <T> ResponseEntity<T> getResponse(String rrId, String url, RequestType requestType, Object requestBody,
                                             Map<String, String> parameters, Class<T> returnType) {
        try {
            logger.info(rrId, AppConstants.LLP_DATAMANAGER, "Calling external service:", null);

            HttpEntity<?> entity = getEntity(requestType, requestBody, parameters);
            ResponseEntity<T> response = restTemplate.exchange(url, getMethod(requestType), entity, returnType);
            return response;
        } catch (AppException ex) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw ex;
        } catch (HttpClientErrorException | HttpServerErrorException heex) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, heex.getStatusCode().value() + heex.getResponseBodyAsString(), null);
            throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, heex.getStatusCode().value() + heex.getResponseBodyAsString());
        } catch (Exception ex) {
            logger.error(rrId, AppConstants.LLP_DATAMANAGER, Utility.getExceptionMessage(ex), null);
            throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ex.getMessage());
        }
    }

	private HttpEntity<?> getEntity(RequestType requestType, Object requestBody, Map<String, String> parameters) {

		HttpHeaders headers = createHeaders(requestType, parameters);
		HttpEntity<?> entity = null;
		try {
			switch (requestType) {
			case GET_DOCX_IDS:
				entity = new HttpEntity<String>(headers);
				break;
				case REQUEST_WITH_BODY:
				entity = new HttpEntity(requestBody, headers);
				break;
			default:
				throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
			}
		} catch (Exception Ex) {
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
		}
		return entity;
	}

	private HttpHeaders createHeaders(RequestType requestType, Map<String, String> parameters) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		switch (requestType) {
		case GET_DOCX_IDS:
			case REQUEST_WITH_BODY:
			headers.setContentType(MediaType.APPLICATION_JSON);
			break;
		default:
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);
		}
		return headers;

	}

	private HttpMethod getMethod(RequestType requestType) {
		HttpMethod method = HttpMethod.GET;
		switch (requestType) {
		case GET_DOCX_IDS:
			method = HttpMethod.GET;
			break;
			case REQUEST_WITH_BODY:
			method = HttpMethod.POST;
			break;
		default:
			throw new AppException(ErrorConstants.SERVICE_UNAVAILABLE, ErrorConstants.SOMETHING_WENT_WRONG);

		}
		return method;
	}
}
