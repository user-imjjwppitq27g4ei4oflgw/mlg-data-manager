package com.datafoundry.llpdatamanager.service;

public interface CaptureLogService {

	void captureRequestLogs(String rrId, Object inputRequest, String remarks, String status);
	void captureProcessLogs(String rrId, Object inputRequest, String remarks, String status);
	void updateStatusAndRemarks(String rrId, String status, String remarks,Object inputRequest);
}
