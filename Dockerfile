FROM azul/zulu-openjdk-alpine:11
VOLUME /tmp
EXPOSE 9091
COPY target/llp-data-manager-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT java -Djava.security.egd=file:/dev/./urandom -jar /app.jar
